from django.conf import settings
from django.core import signing
from django.core.exceptions import ImproperlyConfigured
from django.views.generic import TemplateView
# from users.views import UserMixin
from octonet.mixins import OctonetMixin


class Embed(OctonetMixin, TemplateView):
    template_name = "fussmanager/embed.html"

    def get(self, request, *args, **kwargs):
        print("Embed.get()", request, args, kwargs)
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        secret = getattr(settings, "FUSS_MANAGER_AUTH_FORWARD_KEY", None)
        if not secret:
            raise ImproperlyConfigured("FUSS_MANAGER_AUTH_FORWARD_KEY has not been set")

        fuss_manager_url = getattr(settings, "FUSS_MANAGER_URL", "http://localhost:1232") + "/auth-forward"

        username = self.request.user.username
        if username == "root":
            # Root user is not in octonet
            user_info = {
                "name": "root",
                "id": 0,
                "display_name": "root",
                "groups": [{
                    "name": "root",
                    "id": 0,
                }]
            }
        else:
            octonet_user = self.root_tree.get("/users/users/" + username)
            user_info = {
                "name": octonet_user["uid"],
                "id": octonet_user["uidNumber"],
                "display_name": octonet_user["gecos"],
                "groups": [],
            }
            for octonet_group in self.root_tree.get("/users/users/{}/groups".format(username)):
                user_info["groups"].append({
                    "name": octonet_group["cn"],
                    "id": octonet_group["gidNumber"],
                })

        # print(repr(user_info))

        # Create the authentication token
        token = signing.dumps({
            # This url is where fuss-manager redirects after accepting the auth
            # forwarding. Change it to any arbitrary url to choose which part
            # of fuss-manager is shown in the iframe
            "url": "/",
            "user": user_info,
        }, key=secret, salt="fuss_manager.auth")
        context['fussmanager_url'] = fuss_manager_url
        context['fussmanager_token'] = token
        # Visit "/auth-forward" + "?t=" + token
        return context
