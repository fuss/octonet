# coding: utf-8
from django.urls import re_path
from . import views


app_name = "fussmanager"
urlpatterns = [
    re_path(r"^embed$", views.Embed.as_view(), name="embed"),
]
