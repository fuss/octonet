# coding: utf-8
from django.apps import AppConfig
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from octonet.mixins import OctonetAppMixin
from django.conf import settings
import os


class FussManager(OctonetAppMixin, AppConfig):
    name = 'fussmanager'
    verbose_name = _("Machines")
    description = _("Fuss Manager")
    group = _("Fuss Manager")
    main_url = reverse_lazy("fussmanager:embed")
    font_awesome_class = "fa-cloud"
    weight = 1

    @classmethod
    def is_active(cls, tree):
        fuss_manager_path = getattr(settings, "FUSS_MANAGER_PATH", "/usr/bin/fuss-manager")
        return os.path.exists(fuss_manager_path)
