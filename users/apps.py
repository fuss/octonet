# coding: utf-8
from django.apps import AppConfig
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from octonet.mixins import OctonetAppMixin

class Users(OctonetAppMixin, AppConfig):
    weight = 10
    name = 'users'
    verbose_name = _("Users & Groups")
    description = _("Users & Groups")
    group = _("Users")
    main_url = reverse_lazy("users:list")
    font_awesome_class = "fa-user-circle"
    octofussd_url = "/users"
