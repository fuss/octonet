# coding: utf-8
from unittest import skipIf
from django.urls import reverse
from django.test import SimpleTestCase
import octofuss
from octonet import backend
from octonet.unittest import TestBase


# NB: MISSING TESTS, mainly related to group views (and many written tests are skipped because
#     of the problems with making a working MockTree for the tests themselves)


class LoginMixin():
    def setUp(self):
        # Login
        self.client.post(
            reverse("login"),
            data={
                "username": "root",
                "password": "root",
                "server_url": "http://localhost:13400/conf/",
            }
        )


class TestUsers(LoginMixin, TestBase, SimpleTestCase):
    """
    General views tests
    """
    def setUp(self):
        self.tree = octofuss.MockTree()
        super().setUp()
        session = self.client.session
        session['apps'] = ['users']
        session.save()

    def test_GET_user_list_gives_200(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("users:list"))
            self.assertEqual(response.status_code, 200)

    def test_POST_user_list_gives_405(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("users:list"))
            self.assertEqual(response.status_code, 405)

    def test_GET_user_list_json_gives_200(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("users:list_json"))
            self.assertEqual(response.status_code, 200)

    def test_POST_user_list_json_gives_405(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("users:list_json"))
            self.assertEqual(response.status_code, 405)

    def test_GET_user_create_gives_200(self):
        self.tree.lcreate(["users", "shells", "bash"])
        self.tree.lcreate(["users", "groups"], [{'members': [], 'gidNumber': 505, 'cn': 'group01'}])
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("users:user_create"))
            self.assertEqual(response.status_code, 200)

    def test_POST_user_create_gives_200(self):
        self.tree.lcreate(["users", "shells", "bash"])
        self.tree.lcreate(["users", "groups"], [{'members': [], 'gidNumber': 505, 'cn': 'group01'}])
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("users:user_create"))
            self.assertEqual(response.status_code, 200)

    def test_EMPTY_POST_user_create_gives_200(self):
        self.tree.lcreate(["users", "shells", "bash"])
        self.tree.lcreate(
            ["users", "groups"],
            [
                {'members': ['user01'], 'gidNumber': 500, 'cn': 'group01'},
                {'members': ['user01'], 'gidNumber': 505, 'cn': 'studenti'},
            ]
        )

        self.tree.lcreate(
            ["users", "users"],
            [{
                'uid': 'user01', 'gecos': 'User 01', 'controllerGroup': None,
                'gidNumber': '505', 'sambaPrimaryGroupSID': 'SIDPREFIX-2000', 'enabled': True,
                'uidNumber': '1054', 'homeDirectory': '/home/user01', 'sambaSID': 'SIDPREFIX-3108',
                'ou': 'studenti', 'loginShell': '/bin/bash',
            }]
        )
        self.tree.lcreate(
            ["users", "users", "user01", "groups"],
            [
                {'members': ['user01'], 'gidNumber': 500, 'cn': 'group01'},
                {'members': ['user01'], 'gidNumber': 505, 'cn': 'studenti'},
            ]
        )
        self.tree.lcreate(["netperms", "byuser", "user01"], [])
        # For this test is also needed those lines...
        self.tree.lcreate(["netperms", "bygroup", "group01"], [])
        self.tree.lcreate(["netperms", "bygroup", "studenti"], [])
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("users:user_create"))
            self.assertEqual(response.status_code, 200)

    def test_POST_user_create_gives_302(self):
        self.tree.lcreate(["users", "shells", "/bin/bash"])
        self.tree.lcreate(
            ["users", "users"],
            [{
                'uid': 'user01', 'gecos': 'User 01', 'controllerGroup': None,
                'gidNumber': '505', 'sambaPrimaryGroupSID': 'SIDPREFIX-2000', 'enabled': True,
                'uidNumber': '1054', 'homeDirectory': '/home/user01', 'sambaSID': 'SIDPREFIX-3108',
                'ou': 'studenti', 'loginShell': '/bin/bash',
            }]
        )
        self.tree.lcreate(
            ["users", "groups"],
            [
                {'members': ['user01'], 'gidNumber': 500, 'cn': 'group01'},
                {'members': ['user01'], 'gidNumber': 505, 'cn': 'studenti'},
            ]
        )
        self.tree.lcreate(
            ["users", "users", "user01", "groups"],
            [
                {'members': ['user01'], 'gidNumber': 500, 'cn': 'group01'},
                {'members': ['user01'], 'gidNumber': 505, 'cn': 'studenti'},
            ]
        )
        self.tree.lcreate(["netperms", "byuser", "user01"], [])
        # This test needs also these lines...
        self.tree.lcreate(["netperms", "bygroup", "group01"], [])
        self.tree.lcreate(["netperms", "bygroup", "studenti"], [])
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(
                reverse("users:user_create"),
                {
                    "name": "user02",
                    "uid": "user02",
                    "gidNumber": "505",
                    "gecos": "User 02",
                    "homeDirectory": "/home/user02",
                    "loginShell": "/bin/bash",
                    "password1": "",
                    "password2": "",
                    "controllerGroup": [""],
                },
            )
            self.assertEqual(response.status_code, 302)
            self.assertRedirectMatches(response, reverse("users:detail", kwargs={'uid': 'user02'}))

    def test_GET_user_detail_gives_200(self):
        self.tree.lcreate(["users", "shells", "bash"])
        self.tree.lcreate(
            ["users", "users"],
            [{
                'uid': 'user01', 'gecos': 'User 01', 'controllerGroup': None,
                'gidNumber': '505', 'sambaPrimaryGroupSID': 'SIDPREFIX-2000', 'enabled': True,
                'uidNumber': '1054', 'homeDirectory': '/home/user01', 'sambaSID': 'SIDPREFIX-3108',
                'ou': 'studenti', 'loginShell': '/bin/bash',
            }]
        )
        self.tree.lcreate(
            ["users", "groups"],
            [
                {'members': ['user01'], 'gidNumber': 500, 'cn': 'group01'},
                {'members': ['user01'], 'gidNumber': 505, 'cn': 'studenti'},
            ]
        )
        self.tree.lcreate(
            ["users", "users", "user01", "groups"],
            [
                {'members': ['user01'], 'gidNumber': 500, 'cn': 'group01'},
                {'members': ['user01'], 'gidNumber': 505, 'cn': 'studenti'},
            ]
        )
        self.tree.lcreate(["netperms", "byuser", "user01"], [])
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("users:detail", kwargs={'uid': 'user01'}))
            self.assertEqual(response.status_code, 200)

    def test_POST_user_detail_gives_405(self):
        self.tree.lcreate(["users", "shells", "bash"])
        self.tree.lcreate(
            ["users", "users"],
            [{
                'uid': 'user01', 'gecos': 'User 01', 'controllerGroup': None,
                'gidNumber': '505', 'sambaPrimaryGroupSID': 'SIDPREFIX-2000', 'enabled': True,
                'uidNumber': '1054', 'homeDirectory': '/home/user01', 'sambaSID': 'SIDPREFIX-3108',
                'ou': 'studenti', 'loginShell': '/bin/bash',
            }]
        )
        self.tree.lcreate(
            ["users", "groups"],
            [
                {'members': ['user01'], 'gidNumber': 500, 'cn': 'group01'},
                {'members': ['user01'], 'gidNumber': 505, 'cn': 'studenti'},
            ]
        )
        self.tree.lcreate(
            ["users", "users", "user01", "groups"],
            [
                {'members': ['user01'], 'gidNumber': 500, 'cn': 'group01'},
                {'members': ['user01'], 'gidNumber': 505, 'cn': 'studenti'},
            ]
        )
        self.tree.lcreate(["netperms", "byuser", "user01"], [])
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("users:detail", kwargs={'uid': 'user01'}))
            self.assertEqual(response.status_code, 405)

    def test_GET_user_edit_gives_200(self):
        # This test can be slimmed down, but for now it works (after many (re)tries)
        self.tree.lcreate(["users", "shells", "/bin/bash"])
        self.tree.lcreate(
            ["users", "users"],
            [{
                'uid': 'user01', 'gecos': 'User 01', 'controllerGroup': None,
                'gidNumber': '505', 'sambaPrimaryGroupSID': 'SIDPREFIX-2000', 'enabled': True,
                'uidNumber': '1054', 'homeDirectory': '/home/user01', 'sambaSID': 'SIDPREFIX-3108',
                'ou': 'studenti', 'loginShell': '/bin/bash',
            }]
        )
        self.tree.lcreate(
            ["users", "groups"],
            [
                {'members': ['user01'], 'gidNumber': 500, 'cn': 'group01'},
                {'members': ['user01'], 'gidNumber': 505, 'cn': 'studenti'},
            ]
            )

        self.tree.lcreate(
            ["users", "users", "user01", "groups"],
            [
                {'members': ['user01'], 'gidNumber': 500, 'cn': 'group01'},
                {'members': ['user01'], 'gidNumber': 505, 'cn': 'studenti'},
            ]
        )
        self.tree.lcreate(["netperms", "byuser", "user01"], [])
        self.tree.lcreate(["netperms", "bygroup", "group01"], [])
        self.tree.lcreate(["netperms", "bygroup", "studenti"], [])
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(
                reverse("users:user_create"),
                {
                    "name": "user02",
                    "uid": "user02",
                    "gidNumber": "505",
                    "gecos": "User 02",
                    "homeDirectory": "/home/user02",
                    "loginShell": "/bin/bash",
                    "password1": "",
                    "password2": "",
                    "controllerGroup": [""],
                },
            )
            self.tree.lcreate(
                ["users", "users", "user02", "groups"],
                [],
            )
            self.tree.lcreate(["netperms", "byuser", "user02"], [])
            response = self.client.get(reverse("users:user_edit", kwargs={'uid': 'user02'}))
            self.assertEqual(response.status_code, 200)

    def test_POST_user_edit_gives_200(self):
        # This test can be slimmed down, but for now it works (after many (re)tries)
        self.tree.lcreate(["users", "shells", "/bin/bash"])
        self.tree.lcreate(
            ["users", "groups"],
            [
                {'members': ['user01'], 'gidNumber': 500, 'cn': 'group01'},
                {'members': ['user01'], 'gidNumber': 505, 'cn': 'studenti'},
            ]
        )

        self.tree.lcreate(
            ["users", "users"],
            [{
                'uid': 'user01', 'gecos': 'User 01', 'controllerGroup': None,
                'gidNumber': '505', 'sambaPrimaryGroupSID': 'SIDPREFIX-2000', 'enabled': True,
                'uidNumber': '1054', 'homeDirectory': '/home/user01', 'sambaSID': 'SIDPREFIX-3108',
                'ou': 'studenti', 'loginShell': '/bin/bash',
            }]
        )
        self.tree.lcreate(
            ["users", "users", "user01", "groups"],
            [
                {'members': ['user01'], 'gidNumber': 500, 'cn': 'group01'},
                {'members': ['user01'], 'gidNumber': 505, 'cn': 'studenti'},
            ]
        )
        self.tree.lcreate(["netperms", "byuser", "user01"], [])
        self.tree.lcreate(["netperms", "bygroup", "group01"], [])
        self.tree.lcreate(["netperms", "bygroup", "studenti"], [])
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(
                reverse("users:user_create"),
                {
                    "name": "user02",
                    "gidNumber": "505",
                    "uid": "user02",
                    "gecos": "User 02",
                    "homeDirectory": "/home/user02",
                    "loginShell": "/bin/bash",
                    "password1": "",
                    "password2": "",
                    "controllerGroup": [""],
                },
            )
            self.tree.lcreate(
                ["users", "users", "user02", "groups"],
                [],
            )
            self.tree.lcreate(["netperms", "byuser", "user02"], [])
            response = self.client.post(reverse("users:user_edit", kwargs={'uid': 'user02'}))
            self.assertEqual(response.status_code, 200)

    def test_GET_user_set_enabled_gives_405(self):
        # This test can be slimmed down, but for now it works (after many (re)tries)
        self.tree.lcreate(["users", "shells", "/bin/bash"])
        self.tree.lcreate(
            ["users", "users"],
            [{
                'uid': 'user01', 'gecos': 'User 01', 'controllerGroup': None,
                'gidNumber': '505', 'sambaPrimaryGroupSID': 'SIDPREFIX-2000', 'enabled': True,
                'uidNumber': '1054', 'homeDirectory': '/home/user01', 'sambaSID': 'SIDPREFIX-3108',
                'ou': 'studenti', 'loginShell': '/bin/bash',
            }]
        )
        self.tree.lcreate(
            ["users", "groups"],
            [
                {'members': ['user01'], 'gidNumber': 500, 'cn': 'group01'},
                {'members': ['user01'], 'gidNumber': 505, 'cn': 'studenti'},
            ]
        )
        self.tree.lcreate(
            ["users", "users", "user01", "groups"],
            [
                {'members': ['user01'], 'gidNumber': 500, 'cn': 'group01'},
                {'members': ['user01'], 'gidNumber': 505, 'cn': 'studenti'},
            ]
        )
        self.tree.lcreate(["netperms", "byuser", "user01"], [])
        self.tree.lcreate(["netperms", "bygroup", "group01"], [])
        self.tree.lcreate(["netperms", "bygroup", "studenti"], [])
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(
                reverse("users:user_create"),
                {
                    "name": "user02",
                    "uid": "user02",
                    "gidNumber": "505",
                    "gecos": "User 02",
                    "homeDirectory": "/home/user02",
                    "loginShell": "/bin/bash",
                    "password1": "",
                    "password2": "",
                    "controllerGroup": [""],
                },
            )
            self.tree.lcreate(
                ["users", "users", "user02", "groups"],
                [],
            )
            self.tree.lcreate(["netperms", "byuser", "user02"], [])
            response = self.client.get(reverse("users:user_set_enabled", kwargs={'uid': 'user02'}))
            self.assertEqual(response.status_code, 405)

    def test_POST_user_set_enabled_gives_302(self):
        # This test can be slimmed down, but for now it works (after many (re)tries)
        self.tree.lcreate(["users", "shells", "/bin/bash"])
        self.tree.lcreate(
            ["users", "users"],
            [{
                'uid': 'user01', 'gecos': 'User 01', 'controllerGroup': None,
                'gidNumber': '505', 'sambaPrimaryGroupSID': 'SIDPREFIX-2000', 'enabled': True,
                'uidNumber': '1054', 'homeDirectory': '/home/user01', 'sambaSID': 'SIDPREFIX-3108',
                'ou': 'studenti', 'loginShell': '/bin/bash',
            }]
        )
        self.tree.lcreate(
            ["users", "groups"],
            [
                {'members': ['user01'], 'gidNumber': 500, 'cn': 'group01'},
                {'members': ['user01'], 'gidNumber': 505, 'cn': 'studenti'},
            ]
        )
        self.tree.lcreate(
            ["users", "users", "user01", "groups"],
            [
                {'members': ['user01'], 'gidNumber': 500, 'cn': 'group01'},
                {'members': ['user01'], 'gidNumber': 505, 'cn': 'studenti'},
            ]
        )
        self.tree.lcreate(["netperms", "byuser", "user01"], [])
        self.tree.lcreate(["netperms", "bygroup", "group01"], [])
        self.tree.lcreate(["netperms", "bygroup", "studenti"], [])
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(
                reverse("users:user_create"),
                {
                    "name": "user02",
                    "uid": "user02",
                    "gidNumber": "505",
                    "gecos": "User 02",
                    "homeDirectory": "/home/user02",
                    "loginShell": "/bin/bash",
                    "password1": "",
                    "password2": "",
                    "controllerGroup": [""],
                },
            )
            self.tree.lcreate(
                ["users", "users", "user02", "groups"],
                [],
            )
            self.tree.lcreate(
                ["users", "users", "user02", "enabled"],
                True,
            )
            self.tree.lcreate(["netperms", "byuser", "user02"], [])
            response = self.client.post(
                reverse("users:user_set_enabled", kwargs={'uid': 'user02'}),
                {
                    'value': False,
                },
            )
            self.assertEqual(response.status_code, 302)
            self.assertRedirectMatches(response, reverse("users:detail", kwargs={'uid': 'user02'}))

    def test_GET_user_add_group_gives_405(self):
        # This test can be slimmed down, but for now it works (after many (re)tries)
        self.tree.lcreate(["users", "shells", "/bin/bash"])
        self.tree.lcreate(
            ["users", "users"],
            [{
                'uid': 'user01', 'gecos': 'User 01', 'controllerGroup': None,
                'gidNumber': '505', 'sambaPrimaryGroupSID': 'SIDPREFIX-2000', 'enabled': True,
                'uidNumber': '1054', 'homeDirectory': '/home/user01', 'sambaSID': 'SIDPREFIX-3108',
                'ou': 'studenti', 'loginShell': '/bin/bash',
            }]
        )
        self.tree.lcreate(
            ["users", "groups"],
            [
                {'members': ['user01'], 'gidNumber': 500, 'cn': 'group01'},
                {'members': ['user01'], 'gidNumber': 505, 'cn': 'studenti'},
            ]
        )
        self.tree.lcreate(
            ["users", "users", "user01", "groups"],
            [
                {'members': ['user01'], 'gidNumber': 500, 'cn': 'group01'},
                {'members': ['user01'], 'gidNumber': 505, 'cn': 'studenti'},
            ]
        )
        self.tree.lcreate(["netperms", "byuser", "user01"], [])
        self.tree.lcreate(["netperms", "bygroup", "group01"], [])
        self.tree.lcreate(["netperms", "bygroup", "studenti"], [])
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(
                reverse("users:user_create"),
                {
                    "name": "user02",
                    "uid": "user02",
                    "gidNumber": "505",
                    "gecos": "User 02",
                    "homeDirectory": "/home/user02",
                    "loginShell": "/bin/bash",
                    "password1": "",
                    "password2": "",
                    "controllerGroup": [""],
                },
            )
            self.tree.lcreate(
                ["users", "users", "user02", "groups"],
                [],
            )
            self.tree.lcreate(["netperms", "byuser", "user02"], [])
            response = self.client.get(reverse("users:user_add_group", kwargs={'uid': 'user02'}))
            self.assertEqual(response.status_code, 405)

    def test_POST_user_add_group_gives_302(self):
        # This test can be slimmed down, but for now it works (after many (re)tries)
        self.tree.lcreate(["users", "shells", "/bin/bash"])
        self.tree.lcreate(
            ["users", "users"],
            [{
                'uid': 'user01', 'gecos': 'User 01', 'controllerGroup': None,
                'gidNumber': '505', 'sambaPrimaryGroupSID': 'SIDPREFIX-2000', 'enabled': True,
                'uidNumber': '1054', 'homeDirectory': '/home/user01', 'sambaSID': 'SIDPREFIX-3108',
                'ou': 'studenti', 'loginShell': '/bin/bash',
            }]
        )
        self.tree.lcreate(
            ["users", "groups"],
            [
                {'members': ['user01'], 'gidNumber': 500, 'cn': 'group01'},
                {'members': ['user01'], 'gidNumber': 505, 'cn': 'studenti'},
            ]
        )
        self.tree.lcreate(
            ["users", "users", "user01", "groups"],
            [
                {'members': ['user01'], 'gidNumber': 500, 'cn': 'group01'},
                {'members': ['user01'], 'gidNumber': 505, 'cn': 'studenti'},
            ]
        )
        self.tree.lcreate(["netperms", "byuser", "user01"], [])
        self.tree.lcreate(["netperms", "bygroup", "group01"], [])
        self.tree.lcreate(["netperms", "bygroup", "studenti"], [])
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(
                reverse("users:user_create"),
                {
                    "name": "user02",
                    "uid": "user02",
                    "gidNumber": "505",
                    "gecos": "User 02",
                    "homeDirectory": "/home/user02",
                    "loginShell": "/bin/bash",
                    "password1": "",
                    "password2": "",
                    "controllerGroup": [""],
                },
            )
            self.tree.lcreate(
                ["users", "users", "user02", "groups"],
                [],
            )
            self.tree.lcreate(
                ["users", "users", "user02", "enabled"],
                True,
            )
            self.tree.lcreate(["netperms", "byuser", "user02"], [])
            response = self.client.post(
                reverse("users:user_add_group", kwargs={'uid': 'user02'}),
                {
                    'group': "group01",
                },
            )
            self.assertEqual(response.status_code, 302)
            self.assertRedirectMatches(response, reverse("users:detail", kwargs={'uid': 'user02'}))

    def test_GET_user_delete_gives_405(self):
        # This test can be slimmed down, but for now it works (after many (re)tries)
        self.tree.lcreate(["users", "shells", "/bin/bash"])
        self.tree.lcreate(
            ["users", "users"],
            [{
                'uid': 'user01', 'gecos': 'User 01', 'controllerGroup': None,
                'gidNumber': '505', 'sambaPrimaryGroupSID': 'SIDPREFIX-2000', 'enabled': True,
                'uidNumber': '1054', 'homeDirectory': '/home/user01', 'sambaSID': 'SIDPREFIX-3108',
                'ou': 'studenti', 'loginShell': '/bin/bash',
            }]
        )
        self.tree.lcreate(
            ["users", "groups"],
            [
                {'members': ['user01'], 'gidNumber': 500, 'cn': 'group01'},
                {'members': ['user01'], 'gidNumber': 505, 'cn': 'studenti'},
            ]
        )
        self.tree.lcreate(
            ["users", "users", "user01", "groups"],
            [
                {'members': ['user01'], 'gidNumber': 500, 'cn': 'group01'},
                {'members': ['user01'], 'gidNumber': 505, 'cn': 'studenti'},
            ]
        )
        self.tree.lcreate(["netperms", "byuser", "user01"], [])
        self.tree.lcreate(["netperms", "bygroup", "group01"], [])
        self.tree.lcreate(["netperms", "bygroup", "studenti"], [])
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(
                reverse("users:user_create"),
                {
                    "name": "user02",
                    "uid": "user02",
                    "gidNumber": "505",
                    "gecos": "User 02",
                    "homeDirectory": "/home/user02",
                    "loginShell": "/bin/bash",
                    "password1": "",
                    "password2": "",
                    "controllerGroup": [""],
                },
            )
            self.tree.lcreate(
                ["users", "users", "user02", "groups"],
                [],
            )
            self.tree.lcreate(["netperms", "byuser", "user02"], [])
            response = self.client.get(reverse("users:user_delete", kwargs={'uid': 'user02'}))
            self.assertEqual(response.status_code, 405)

    def test_POST_user_delete_gives_302(self):
        # This test can be slimmed down, but for now it works (after many (re)tries)
        self.tree.lcreate(["users", "shells", "/bin/bash"])
        self.tree.lcreate(
            ["users", "users"],
            [{
                'uid': 'user01', 'gecos': 'User 01', 'controllerGroup': None,
                'gidNumber': '505', 'sambaPrimaryGroupSID': 'SIDPREFIX-2000', 'enabled': True,
                'uidNumber': '1054', 'homeDirectory': '/home/user01', 'sambaSID': 'SIDPREFIX-3108',
                'ou': 'studenti', 'loginShell': '/bin/bash',
            }]
        )
        self.tree.lcreate(
            ["users", "groups"],
            [
                {'members': ['user01'], 'gidNumber': 500, 'cn': 'group01'},
                {'members': ['user01'], 'gidNumber': 505, 'cn': 'studenti'},
            ]
        )
        self.tree.lcreate(
            ["users", "users", "user01", "groups"],
            [
                {'members': ['user01'], 'gidNumber': 500, 'cn': 'group01'},
                {'members': ['user01'], 'gidNumber': 505, 'cn': 'studenti'},
            ]
        )
        self.tree.lcreate(["netperms", "byuser", "user01"], [])
        self.tree.lcreate(["netperms", "bygroup", "group01"], [])
        self.tree.lcreate(["netperms", "bygroup", "studenti"], [])
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(
                reverse("users:user_create"),
                {
                    "name": "user02",
                    "uid": "user02",
                    "gidNumber": "505",
                    "gecos": "User 02",
                    "homeDirectory": "/home/user02",
                    "loginShell": "/bin/bash",
                    "password1": "",
                    "password2": "",
                    "controllerGroup": [""],
                },
            )
            self.tree.lcreate(
                ["users", "users", "user02", "groups"],
                [],
            )
            self.tree.lcreate(
                ["users", "users", "user02", "enabled"],
                True,
            )
            self.tree.lcreate(["netperms", "byuser", "user02"], [])
            response = self.client.post(reverse("users:user_delete", kwargs={'uid': 'user02'}))
            self.assertEqual(response.status_code, 302)
            self.assertRedirectMatches(response, reverse("users:list"))

    def test_GET_group_list_gives_200(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("users:group_list"))
            self.assertEqual(response.status_code, 200)

    def test_POST_group_list_gives_405(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("users:group_list"))
            self.assertEqual(response.status_code, 405)

    def test_GET_group_create_gives_200(self):
        self.tree.lcreate(["users", "groups"], [{'members': [], 'gidNumber': 505, 'cn': 'group01'}])
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("users:group_create"))
            self.assertEqual(response.status_code, 200)

    def test_EMPTY_POST_group_create_gives_200(self):
        self.tree.lcreate(["users", "groups"], [{'members': [], 'gidNumber': 505, 'cn': 'group01'}])
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("users:group_create"))
            self.assertEqual(response.status_code, 200)

    def test_POST_group_create_gives_302(self):
        self.tree.lcreate(["users", "groups"], [{'members': [], 'gidNumber': 505, 'cn': 'group01'}])
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("users:group_create"), {'name': "group02"})
            self.assertEqual(response.status_code, 302)
            self.assertRedirectMatches(
                response,
                reverse("users:group_detail", kwargs={'name': 'group02'}),
            )

    @skipIf(True, "Not working, because of MockTree")
    def test_GET_group_detail_gives_200(self):
        self.tree.lcreate(["users", "groups"], [{'members': [], 'gidNumber': 505, 'cn': 'group01'}])
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            self.client.post(reverse("users:group_create"), {'name': "group02"})
            self.tree.lcreate(["netperms", "bygroup", "group02"], [])
            response = self.client.get(reverse("users:group_detail", kwargs={'name': 'group02'}))
            self.assertEqual(response.status_code, 200)

    @skipIf(True, "Not working, because of MockTree")
    def test_POST_group_detail_gives_405(self):
        self.tree.lcreate(["users", "groups"], [{'members': [], 'gidNumber': 505, 'cn': 'group01'}])
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            self.client.post(reverse("users:group_create"), {'name': "group02"})
            self.tree.lcreate(["netperms", "bygroup", "group02"], [])
            response = self.client.post(reverse("users:group_detail", kwargs={'name': 'group02'}))
            self.assertEqual(response.status_code, 405)

    @skipIf(True, "Not working, because of MockTree")
    def test_GET_group_edit_gives_200(self):
        self.tree.lcreate(["users", "shells", "/bin/bash"])
        self.tree.lcreate(
            ["users", "users"],
            [{
                'uid': 'user01', 'gecos': 'User 01', 'controllerGroup': None,
                'gidNumber': '505', 'sambaPrimaryGroupSID': 'SIDPREFIX-2000', 'enabled': True,
                'uidNumber': '1054', 'homeDirectory': '/home/user01', 'sambaSID': 'SIDPREFIX-3108',
                'ou': 'studenti', 'loginShell': '/bin/bash',
            }]
        )
        self.tree.lcreate(["users", "groups"], [{'members': [], 'gidNumber': 505, 'cn': 'group01'}])
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            self.tree.lcreate(["netperms", "bygroup", "group01"], [])
            response = self.client.get(reverse("users:group_edit", kwargs={'name': 'group01'}))
            self.assertEqual(response.status_code, 200)

    @skipIf(True, "Not working, because of MockTree")
    def test_EMPTY_POST_group_edit_gives_200(self):
        self.tree.lcreate(["users", "shells", "/bin/bash"])
        self.tree.lcreate(
            ["users", "users"],
            [{
                'uid': 'user01', 'gecos': 'User 01', 'controllerGroup': None,
                'gidNumber': '505', 'sambaPrimaryGroupSID': 'SIDPREFIX-2000', 'enabled': True,
                'uidNumber': '1054', 'homeDirectory': '/home/user01', 'sambaSID': 'SIDPREFIX-3108',
                'ou': 'studenti', 'loginShell': '/bin/bash',
            }]
        )
        self.tree.lcreate(["users", "groups"], [{'members': [], 'gidNumber': 505, 'cn': 'group01'}])
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            self.tree.lcreate(["netperms", "bygroup", "group01"], [])
            response = self.client.post(reverse("users:group_edit", kwargs={'name': 'group01'}))
            self.assertEqual(response.status_code, 200)

    def test_GET_csvimport_gives_200(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("users:csvimport"))
            self.assertEqual(response.status_code, 200)

    def test_POST_csvimport_gives_405(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("users:csvimport"))
            self.assertEqual(response.status_code, 405)

    def test_GET_csvimport_ajax_gives_405(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("users:csvimport_ajax"))
            self.assertEqual(response.status_code, 405)

    def test_POST_csvimport_ajax_gives_200(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("users:csvimport_ajax"))
            self.assertEqual(response.status_code, 200)

    def test_GET_masscreate_gives_200(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("users:masscreate"))
            self.assertEqual(response.status_code, 200)

    def test_POST_masscreate_gives_200(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("users:masscreate"))
            self.assertEqual(response.status_code, 200)

    def test_GET_massedit_gives_200(self):
        self.tree.lcreate(["users", "groups"], [{'members': [], 'gidNumber': 505, 'cn': 'group01'}])
        self.tree.lcreate(["netperms", "bygroup", "group01"], [])
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("users:massedit"))
            self.assertEqual(response.status_code, 200)

    def test_POST_massedit_gives_302(self):
        self.tree.lcreate(["users", "groups"], [{'members': [], 'gidNumber': 505, 'cn': 'group01'}])
        self.tree.lcreate(["netperms", "bygroup", "group01"], [])
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("users:massedit"))
            self.assertEqual(response.status_code, 302)

    def test_GET_processldif_gives_200(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("users:processldif"))
            self.assertEqual(response.status_code, 200)

    def test_POST_processldif_gives_200(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("users:processldif"))
            self.assertEqual(response.status_code, 200)
