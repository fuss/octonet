from django.urls import re_path
from . import views


app_name="users"
urlpatterns = [
    re_path(r"^user/list$", views.UserList.as_view(), name="list"),
    re_path(r"^user/list_json", views.UserListJSON.as_view(), name="list_json"),
    re_path(r"^user/create", views.UserCreate.as_view(), name="user_create"),
    re_path(r"^user/(?P<uid>[^/]+)/detail$", views.UserDetail.as_view(), name="detail"),
    re_path(r"^user/(?P<uid>[^/]+)/edit$", views.UserEdit.as_view(), name="user_edit"),
    re_path(r"^user/(?P<uid>[^/]+)/set-enabled$", views.UserSetEnabled.as_view(), name="user_set_enabled"),
    # refs #12559
    # refs #14151: Hide the button that made POST to this view
    # re_path(r"^user/(?P<uid>[^/]+)/force-password-change$", views.UserForcePasswordChange.as_view(), name="user_force_password_change"),
    re_path(r"^user/(?P<uid>[^/]+)/add-group$", views.UserAddGroup.as_view(), name="user_add_group"),
    # refs #213
    re_path(r"^user/(?P<uid>[^/]+)/delete$", views.UserDelete.as_view(), name="user_delete"),
    re_path(r"^groups/list$", views.GroupList.as_view(), name="group_list"),
    # refs #212
    re_path(r"^group/create$", views.GroupCreate.as_view(), name="group_create"),
    re_path(r"^group/(?P<name>[^/]+)/detail$", views.GroupDetail.as_view(), name="group_detail"),
    re_path(r"^group/(?P<name>[^/]+)/edit$", views.GroupEdit.as_view(), name="group_edit"),
    re_path(r"^group/(?P<name>[^/]+)/add-group$", views.GroupAddGroup.as_view(), name="group_add_group"),
    re_path(r"^group/(?P<name>[^/]+)/remove-group$", views.GroupRemoveGroup.as_view(), name="group_remove_group"),
    re_path(r"^group/(?P<name>[^/]+)/drop-all-users$", views.GroupDropAllUsers.as_view(), name="group_drop_all_users"),
    re_path(r"^group/(?P<name>[^/]+)/add-permission$", views.GroupAddPermission.as_view(), name="group_add_perm"),
    re_path(r"^group/(?P<name>[^/]+)/remove-permission$", views.GroupRemovePermission.as_view(), name="group_remove_perm"),
    re_path(r"^group/(?P<name>[^/]+)/delete$", views.GroupDelete.as_view(), name="group_delete"),
    re_path(r"^csvimport$", views.CSVImport.as_view(), name="csvimport"),
    re_path(r"^csvimport_ajax$", views.CSVImportAJAX.as_view(), name="csvimport_ajax"),
    # refs #353
    re_path(r"^csvimport/check$", views.CSVImportCheck.as_view(), name="csvimport_check"),
    re_path(r"^masscreate$", views.MassCreate.as_view(), name="masscreate"),
    # refs #108
    re_path(r"^massedit$", views.MassEdit.as_view(), name="massedit"),
    re_path(r"^processldif", views.ProcessLDIF.as_view(), name="processldif"),
]
