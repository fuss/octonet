# coding: utf-8
import codecs
from collections import defaultdict
import csv
import datetime
from html import unescape
import io
import json
import os
import re
from django import http, forms
from django.contrib import messages
from django.urls import reverse, reverse_lazy
from django.utils import html
from django.utils.translation import gettext_lazy as _
from django.shortcuts import redirect, render
from django.views.generic import View, TemplateView
from django.views.generic.edit import FormView
from octofuss import exceptions as oexceptions
from octofuss import password as password_generator
from octonet.forms import FormControlClassMixin
from octonet.mixins import OctonetMixin
from octonet.utils import reorder_fields
from urllib.parse import quote_plus, unquote_plus


class UserList(OctonetMixin, TemplateView):
    template_name = "users/list.html"


class UserListJSON(OctonetMixin, View):
    def get(self, request, *args, **kw):
        tree = self.octofuss_tree("/users/users")
        users = tree.lget([])
        if users == None:
            users = []
        iTotalRecords = len(users)

        sEcho = request.GET.get('sEcho', None)
        sSearch = request.GET.get('sSearch', None)
        iDisplayStart = request.GET.get('iDisplayStart', None)
        iDisplayLength = request.GET.get('iDisplayLength', None)
        # refs #134
        iSortCol = request.GET.get('iSortCol_0', 1)
        sSortDir = request.GET.get('sSortDir_0', "asc")
        match = {
            0: "enabled",
            1: "uid",
            2: "gecos",
            3: "ou",
            4: "homeDirectory",
            5: "controllerGroup",
            6: "uidNumber",
        }

        if sSearch and users:
            users = [ u for u in users if sSearch in u.get("uid")]

        # refs #134, sort for real
        sort_key = match[int(iSortCol)] if iSortCol else "uid"
        users = sorted(
            users,
            key=lambda x: x.get(sort_key) if x.get(sort_key) is not None else "",
            reverse=(sSortDir == "desc"),
        )
        iTotalDisplayRecords = len(users)

        if iDisplayStart:
            start = int(iDisplayStart)
        else:
            start = 0
        if iDisplayLength:
            length = int(iDisplayLength)
        else:
            length = 30

        # refs #9972
        epoch = datetime.date(1970, 1, 1)
        today = datetime.date.today()

        aaData = []
        for u in users[start:start+length]:
            # refs #9972
            # Sometimes this get() will return None (as seen in test instances), so...
            shadowMax = int(u.get('shadowMax', 0) or 0)
            # This get didn't fail in test instances, but just to be on the safe side...
            shadowLastChange = int(u.get('shadowLastChange', 0) or 0)
            last_password_change = epoch + datetime.timedelta(days=shadowLastChange)
            next_password_change = last_password_change + datetime.timedelta(days=shadowMax)
            expired_password = shadowLastChange == 0 or next_password_change < today
            password_icon_color = expired_password and "text-danger" or "text-success"

            row = []
            row.append("<span class='glyphicon glyphicon-{}'></span>".format(
                u.get("enabled") and "ok text-success" or "remove text-danger"))
            row.append("<a href='{}'>{}</a>".format(
                html.escape(reverse("users:detail", kwargs=dict(uid=u.get("uid")))),
                html.escape(u.get("uid"))
            ))
            row.append(u.get("gecos"))
            # refs #9972
            row.append("<span class='fa fa-key {}'></span>".format(password_icon_color))
            row.append(u.get("ou"))
            row.append(u.get("homeDirectory"))
            controller = u.get("controllerGroup")
            if controller:
                row.append("<a href='{}'>{}</a>".format(
                    html.escape(reverse("users:group_detail", args=[controller])),
                    html.escape(controller))
                    )
            else:
                row.append("")
            row.append(u.get("uidNumber"))
            row.append("<a class='btn btn-primary btn-sm' href='{}'><span class='fa fa-pencil'></span> {}</a>".format(
                html.escape(reverse("users:user_edit", kwargs=dict(uid=u.get("uid")))),
                html.escape(_("Edit"))
                ))
            aaData.append(row)
        data = dict(aaData=aaData,
                    sEcho=sEcho,
                    iTotalDisplayRecords=iTotalDisplayRecords,
                    iTotalRecords=iTotalRecords
            )
        return http.HttpResponse(json.dumps(data), content_type="application/json")


class UserMixin(OctonetMixin):
    def load_objects(self):
        self.tree = self.octofuss_tree("/users/users/{}".format(self.kwargs["uid"]))
        self.user = self.tree.lget([])
        self.user["groups"] = self.tree.llist(["groups"])
        self.all_groups = self.root_tree.lget(["users", "groups"])
        self.user['get_gecos_display'] = unescape(self.user['gecos'])
        # refs #9972
        epoch = datetime.date(1970, 1, 1)
        today = datetime.date.today()
        # Sometimes this get() will return None (as seen in test instances), so...
        shadowMax = int(self.user.get('shadowMax', 0) or 0)
        # This get didn't fail in test instances, but just to be on the safe side...
        shadowLastChange = int(self.user.get('shadowLastChange', 0) or 0)
        last_password_change = epoch + datetime.timedelta(days=shadowLastChange)
        if shadowLastChange:
            self.user['last_password_change'] = last_password_change.strftime("%Y-%m-%d")
            self.user['last_password_change_days'] = _("{} day(s) ago").format((today - last_password_change).days)
        else:
            self.user['last_password_change'] = "-"
            self.user['last_password_change_days'] = _("0 day(s) ago")
        next_password_change = last_password_change + datetime.timedelta(days=shadowMax)
        self.user['next_password_change'] = next_password_change.strftime("%Y-%m-%d")
        if next_password_change < today:
            self.user['next_password_change_days'] = _("{} day(s) ago").format((today - next_password_change).days)
        elif next_password_change > today:
            self.user['next_password_change_days'] = _("in {} day(s)").format((next_password_change - today).days)
        else:
            self.user['next_password_change_days'] = _("Today!")

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        ctx["octofuss_user"] = self.user
        ctx["all_groups"] = self.all_groups
        return ctx


# refs #108
class MassEdit(OctonetMixin, FormView):
    template_name = "users/massedit.html"

    def load_objects(self):
        # refs #287
        self.has_quota = self.root_tree.lhas(["quota"])

    def dispatch(self, request, *args, **kwargs):
        self.uids = request.GET.get('uids', "")
        self.users = self.uids.split(",") or []
        return super().dispatch(request, *args, **kwargs)

    def get_initial(self):
        # refs #287
        if self.has_quota:
            return {
                "quota": [{"fs": fs} for fs in self.root_tree.llist(["quota"])]
            }
        else:
            return {}

    def get_form_class(self):
        all_groups = self.root_tree.llist(["users", "groups"])
        all_netperms = self.root_tree.llist(['netperms', 'bygroup'])

        class AddToGroupForm(FormControlClassMixin, forms.Form):
            add_to_group = forms.MultipleChoiceField(
                choices=[(x, unquote_plus(x)) for x in all_groups],
                label=_("Add to group"), required=False,
            )

        class RemoveFromGroupForm(FormControlClassMixin, forms.Form):
            remove_from_group = forms.MultipleChoiceField(
                choices=[(x, unquote_plus(x)) for x in all_groups],
                label=_("Remove from group"), required=False,
            )

        class NetPermForm(FormControlClassMixin, forms.Form):
            def __init__(self, *args, **kw):
                super().__init__(*args, **kw)
                self.fields["netperm"].choices = \
                    [("", _("Please choose..."))] +\
                    [(x, unquote_plus(x)) for x in all_netperms]

        class AddNetPermForm(FormControlClassMixin, forms.Form):
            add_netperm = forms.MultipleChoiceField(
                choices=[(x, unquote_plus(x)) for x in all_netperms],
                label=_("Add netperm"), required=False,
            )

        class RemoveNetPermForm(FormControlClassMixin, forms.Form):
            remove_netperm = forms.MultipleChoiceField(
                choices=[(x, unquote_plus(x)) for x in all_netperms],
                label=_("Remove netperm"), required=False,
            )

        # refs #287
        if self.has_quota:
            return make_multiform(**{
                'add_to_group': AddToGroupForm,
                'remove_from_group': RemoveFromGroupForm,
                'add_netperm': AddNetPermForm,
                'remove_netperm': RemoveNetPermForm,
                'quota': forms.formset_factory(OptionalQuotaForm, extra=0),
            })
        else:
            return make_multiform(**{
                'add_to_group': AddToGroupForm,
                'remove_from_group': RemoveFromGroupForm,
                'add_netperm': AddNetPermForm,
                'remove_netperm': RemoveNetPermForm,
            })

    def form_valid(self, form):
        uids = self.request.POST.get('uids', "").split(",")
        action = self.request.POST.get("action", "")
        if not action:
            messages.error(self.request, _("No action specified"))
            return redirect("users:list")
        if not uids:
            messages.error(self.request, _("No users specified"))
            return redirect("users:list")
        if action == "activate":
            for uid in uids:
                self.root_tree.lset(
                    ["users", "users", uid, "enabled"],
                    "True",
                )
        elif action == "deactivate":
            for uid in uids:
                self.root_tree.lset(
                    ["users", "users", uid, "enabled"],
                    "False",
                )
        # refs #12559
        elif action == "force_password_change":
            for uid in uids:
                self.root_tree.lset(
                    ["users", "users", uid, "shadowLastChange"],
                    0,
                )
        # refs #12389 #571
        elif action == "delete":
            for uid in uids:
                self.root_tree.ldelete(
                    ["users", "users", uid],
                )
        elif action == "edit":
            # Group management
            add_to_group_list = form["add_to_group"].cleaned_data["add_to_group"]
            remove_from_group_list = form["remove_from_group"].cleaned_data["remove_from_group"]
            for add_to_group in add_to_group_list:
                if self.root_tree.get("users/groups/%s" % add_to_group):
                    for uid in uids:
                        # What happens if the user is already in the group?
                        self.root_tree.lcreate(["users", "users", uid, "groups", add_to_group])
                else:
                    messages.error(self.request, _("Group to add users to does not exist"))
            for remove_from_group in remove_from_group_list:
                for uid in uids:
                    # If the user is not in the group this seems to be ok
                    self.root_tree.ldelete(["users", "users", uid, "groups", remove_from_group])
            # Netperms management
            add_netperm_list = form["add_netperm"].cleaned_data["add_netperm"]
            remove_netperm_list = form["remove_netperm"].cleaned_data["remove_netperm"]
            for add_netperm in add_netperm_list:
                # TODO: Add check for netperm existence
                for uid in uids:
                    # After manual testing, adding a netperm to a user that
                    # already has it does nothing
                    self.root_tree.lcreate(["netperms", "byuser", uid, add_netperm])
            for remove_netperm in remove_netperm_list:
                for uid in uids:
                    # After manual testing, removing a netperm to a user that
                    # does not have it does nothing
                    self.root_tree.ldelete(["netperms", "byuser", uid, remove_netperm])
            # refs #287
            if self.has_quota:
                # Quota management
                for qform in form["quota"]:
                    disk_soft = qform.cleaned_data["disk_soft"]
                    disk_hard = qform.cleaned_data["disk_hard"]
                    file_soft = qform.cleaned_data["file_soft"]
                    file_hard = qform.cleaned_data["file_hard"]
                    if disk_soft and disk_hard:
                        for uid in uids:
                            path = ["quota", qform.initial['fs'], "user", uid]
                            if self.root_tree.lhas(path):
                                self.root_tree.lset(path, {'disk_soft': disk_soft, 'disk_hard': disk_hard})
                    if file_soft and file_hard:
                        for uid in uids:
                            path = ["quota", qform.initial['fs'], "user", uid]
                            if self.root_tree.lhas(path):
                                self.root_tree.lset(path, {'file_soft': file_soft, 'file_hard': file_hard})
        messages.success(self.request, _("Action successful"))
        return redirect("users:list")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # refs #108
        context['groups'] = self.root_tree.llist(["users", "groups"])
        context['uids'] = self.uids
        self.tree = self.octofuss_tree("/users/users")
        context['users'] = [self.tree.lget([x]) for x in self.users]
        for user in context['users']:
            user['get_gecos_display'] = unescape(user['gecos'])
        # refs #287
        context['has_quota'] = self.has_quota
        return context


class UserDetail(UserMixin, TemplateView):
    """
    Show users details
    """
    template_name = "users/user_detail.html"

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        ctx["groups"] = self.tree.lget(["groups"])
        # refs #145
        netperms = self.root_tree.lget(["netperms", "byuser", ctx['uid']])
        ctx['netperms'] = [
            {'permission': x[0], 'hostname': x[1]['hostname']} for x in netperms
        ]
        return ctx


class UserForm(FormControlClassMixin, forms.Form):
    gecos = forms.CharField(label=_("Full name"))
    loginShell = forms.ChoiceField(label=_("Login shell"))
    gidNumber = forms.ChoiceField(label=_("Primary group"), required=True)
    controllerGroup = forms.ChoiceField(label=_("Controller group"), required=False)
    homeDirectory = forms.CharField(label=_("Home directory"))
    password1 = forms.CharField(
        label=_("Password"),
        required=False,
        # refs 341
        widget=forms.PasswordInput(render_value=True, attrs={'autocomplete': 'off'}),
    )
    password2 = forms.CharField(
        label=_("Confirm"),
        required=False,
        # refs 341
        widget=forms.PasswordInput(render_value=True, attrs={'autocomplete': 'off'}),
    )
    groups = forms.MultipleChoiceField(label=_("Groups"), required=False)
    netperms = forms.MultipleChoiceField(label=_("Permissions"), required=False)
    # refs #14151
    force_password_change = forms.BooleanField(label=_("Force password change"), required=False)

    # refs #13094
    def clean_gecos(self):
        val = self.cleaned_data['gecos']
        encoded = val.encode("ascii", "xmlcharrefreplace")
        # https://stackoverflow.com/a/48047511
        return "".join(chr(x) for x in bytearray(encoded))

    def clean_homeDirectory(self):
        val = self.cleaned_data["homeDirectory"].strip()
        if not val.startswith("/home/"):
            raise forms.ValidationError(_("The home directory must be a subdirectory of /home/"), code="home_outside_homes")
        return val

    # refs #14152
    def is_ascii(self, password):
        # Only accept standard ASCII characters
        try:
            # https://stackoverflow.com/a/32357552
            password.encode("ascii")
            return True
        except UnicodeEncodeError:
            return False

    # refs #14152
    def clean_password1(self):
        password = self.cleaned_data['password1']
        if not self.is_ascii(password):
            raise forms.ValidationError(_("Password can contain ASCII characters only"), code="non_ascii_password")
        return password

    # refs #14152
    def clean_password2(self):
        password = self.cleaned_data['password2']
        if not self.is_ascii(password):
            raise forms.ValidationError(_("Password can contain ASCII characters only"), code="non_ascii_password")
        return password

    def clean(self):
        password1 = self.cleaned_data.get("password1", None)
        password2 = self.cleaned_data.get("password2", None)

        # refs #14152
        if (password1 and password2) and (password1 != password2):
            raise forms.ValidationError(_("Passwords don't match"), code="password_mismatch")

        return self.cleaned_data


class QuotaForm(FormControlClassMixin, forms.Form):
    disk_soft = forms.IntegerField(label=_("Disk usage soft limit"))
    disk_hard = forms.IntegerField(label=_("Disk usage hard limit"))
    file_soft = forms.IntegerField(label=_("File count soft limit"))
    file_hard = forms.IntegerField(label=_("File count hard limit"))

    def clean(self):
        for domain in "disk", "file":
            soft = self.cleaned_data[domain + "_soft"]
            hard = self.cleaned_data[domain + "_hard"]
            if soft > hard:
                self.add_error(
                        domain + "_soft",
                        forms.ValidationError(
                            _("%(domain)s soft quota cannot be bigger than hard quota"),
                            params={"domain": domain}, code="soft_gt_hard")
                )


class OptionalQuotaForm(FormControlClassMixin, forms.Form):
    disk_soft = forms.IntegerField(label=_("Disk usage soft limit"), required=False)
    disk_hard = forms.IntegerField(label=_("Disk usage hard limit"), required=False)
    file_soft = forms.IntegerField(label=_("File count soft limit"), required=False)
    file_hard = forms.IntegerField(label=_("File count hard limit"), required=False)

    def __init__(self, *args, **kw):
        super().__init__(*args, **kw)
        for f in self.fields.values():
            f.widget.attrs["placeholder"] = _("Unchanged")

    def clean(self):
        for domain in "disk", "file":
            soft = self.cleaned_data.get(domain + "_soft", None)
            hard = self.cleaned_data.get(domain + "_hard", None)
            if soft is None and hard is not None:
                self.add_error(
                        domain + "_soft",
                        forms.ValidationError(
                            _("%(domain)s soft quota needs to specified together with hard quota"),
                            params={"domain": domain}, code="soft_missing")
                )
            if soft is not None and hard is None:
                self.add_error(
                        domain + "_soft",
                        forms.ValidationError(
                            _("%(domain)s hard quota needs to specified together with soft quota"),
                            params={"domain": domain}, code="hard_missing")
                )
            if soft is not None and hard is not None and soft > hard:
                self.add_error(
                        domain + "_soft",
                        forms.ValidationError(
                            _("%(domain)s soft quota cannot be bigger than hard quota"),
                            params={"domain": domain}, code="soft_gt_hard")
                )


class MultiForm:
    """
    Form that contains multiple subforms, each with its name
    """
    # Classes for the subforms
    form_classes = {}

    def __init__(self, *args, **kw):
        self.forms = {}
        for name, cls in self.form_classes.items():
            kwargs = dict(kw)
            # Prefix defaults to the form name
            kwargs.setdefault("prefix", name)
            # If initial[name] exists, use that as initial, else just use
            # initial
            initial = kw.get("initial", None)
            if initial is not None:
                kwargs["initial"] = initial.get(name, initial)
            self.forms[name] = cls(*args, **kwargs)

    def is_valid(self):
        return all(x.is_valid() for x in self.forms.values())

    def __getitem__(self, name):
        return self.forms[name]


def make_multiform(**kw):
    return type("Multiform", (MultiForm,), { "form_classes": kw })


class UserFormMixin:
    def load_objects(self):
        super().load_objects()
        self.has_net_perms = self.root_tree.lhas(["netperms"])
        self.has_quota = self.root_tree.lhas(["quota"])
        self.all_groups = self.root_tree.lget(["users", "groups"])
        self.all_groups = sorted(self.all_groups, key=lambda x: x.get("cn"))

    def _get_custom_user_form(self):
        shells = self.root_tree.llist(["users", "shells"])

        groups = [(quote_plus(x["cn"]), x["cn"]) for x in getattr(self, "all_groups", [])]
        groups_with_gid = [(x['gidNumber'], x['cn']) for x in getattr(self, "all_groups", [])]
        # refs #12214 #352 Add the variable also here, to be reachable in CreateUser view
        self.groups_with_gid = groups_with_gid

        if self.has_net_perms:
            netperms = [(x, x) for x in self.root_tree.llist(["netperms", "bygroup"])]
        else:
            netperms = []

        class Form(self.form_class):
            def __init__(self, *args, **kw):
                super().__init__(*args, **kw)
                self.fields["loginShell"].choices = [(x, x) for x in shells]
                self.fields["gidNumber"].choices = [("", "")] + groups_with_gid
                self.fields["controllerGroup"].choices = [("", _("(disabled)"))] + groups

                self.fields["groups"].choices = groups
                if not netperms:
                    del self.fields["netperms"]
                else:
                    self.fields["netperms"].choices = netperms

        return Form

    def get_form_class(self):
        res = { "user": self._get_custom_user_form() }
        if self.has_quota and hasattr(self, "user"):
            res["quota"] = forms.formset_factory(QuotaForm, extra=0)
        return make_multiform(**res)


class UserEdit(UserFormMixin, UserMixin, FormView):
    template_name = "users/user_edit.html"
    form_class = UserForm

    def get_initial(self):
        res = {
            "user": self.user
        }
        res["user"].update(
            netperms=self.root_tree.llist(["netperms", "byuser", self.user["uid"]]),
            # refs #13094
            gecos=self.user['get_gecos_display'],
        )
        if self.has_quota:
            quota = []
            for fs in self.root_tree.llist(["quota"]):
                data = self.root_tree.lget(["quota", fs, "user", self.user["uid"]])
                data["fs"] = fs
                quota.append(data)
            res["quota"] = quota
        return res

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        ctx["has_net_perms"] = self.has_net_perms
        ctx["has_quota"] = self.has_quota
        return ctx

    def get_success_url(self):
        return reverse("users:detail", args=[self.user["uid"]])

    def form_valid(self, form):
        user_form = form["user"]
        for field in ["loginShell", "gecos", "homeDirectory", "controllerGroup"]:
            if not self.user[field] and not user_form.cleaned_data[field]: continue
            if self.user[field] != user_form.cleaned_data[field]:
                self.tree.lset([field], user_form.cleaned_data[field])

        if user_form.cleaned_data["password1"]:
            self.tree.lset(["password"], user_form.cleaned_data["password1"])

        cur_groups = frozenset(self.tree.llist(["groups"]))
        new_groups = frozenset(user_form.cleaned_data["groups"])
        if cur_groups != new_groups:
            for group in cur_groups - new_groups:
                self.tree.ldelete(["groups", group])
            for group in new_groups - cur_groups:
                self.tree.lcreate(["groups", group])

        self.tree.lset(["gidNumber"], user_form.cleaned_data["gidNumber"])

        netperms = user_form.cleaned_data.get("netperms", None)
        if netperms is not None:
            cur_perms = frozenset(self.root_tree.llist(["netperms", "byuser", self.user["uid"]]))
            new_perms = frozenset(netperms)
            for perm in cur_perms - new_perms:
                self.root_tree.ldelete(["netperms", "byuser", self.user["uid"], perm])
            for perm in new_perms - cur_perms:
                self.root_tree.lcreate(["netperms", "byuser", self.user["uid"], perm])

        if self.has_quota:
            quota_form = form["quota"]
            for f in quota_form:
                path = ["quota", f.initial["fs"], "user", self.user["uid"]]
                if self.root_tree.lhas(path):
                    self.root_tree.lset(path, f.cleaned_data)

        # refs #14151
        if user_form.cleaned_data.get('force_password_change', False):
            self.root_tree.lset(["users", "users", self.user["uid"], "shadowLastChange"], 0)
            messages.info(self.request, _("Password change forced for user {}").format(self.user['uid']))
        return super().form_valid(form)


class UserCreateForm(UserForm):
    name = forms.CharField(label=_("User name"))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # refs #230 Move username to the top of the form
        key_order = [
            'name',
            'gidNumber',
            'gecos',
            'loginShell',
            'controllerGroup',
            'homeDirectory',
            'password1',
            'password2',
            # Add the new field to the key_order list of keys
            'force_password_change',
            'groups',
            'netperms',
        ]
        self.fields = reorder_fields(self.fields, key_order)

    # refs #13930
    def clean_name(self):
        value = self.cleaned_data['name']
        # Reuse the CSVImporter regex, already in place since much time
        regex = re.compile("^[a-z.0-9_]+$")
        if not regex.match(value):
            raise forms.ValidationError(_("The username can contain only lowercase letters, numbers, dot, underscore"))
        return value


# refs #286
class NetpermsMixin():
    def load_objects(self):
        super().load_objects()
        self.all_netperms = self.root_tree.llist(['netperms', 'bygroup'])
        self.default_netperms = [
            x for x in self.all_netperms if x not in ['lpadmin', 'wifi']
        ]


class UserCreate(NetpermsMixin, UserFormMixin, OctonetMixin, FormView):
    template_name = "users/user_edit.html"
    form_class = UserCreateForm

    # refs #286
    def get_initial(self):
        initial = super().get_initial()
        initial['netperms'] = self.default_netperms
        return initial

    def get_success_url(self):
        return reverse("users:detail", args=[self.user["uid"]])

    def form_valid(self, form):
        user_form = form["user"]

        user_data = dict(user_form.cleaned_data)
        name = user_data.pop("name")
        # do not remove gidNumber info, its needed for primary group
        #gidNumber = user_data.pop("gidNumber")
        gidNumber = user_data["gidNumber"]
        user_data['uid'] = name
        password = user_data.pop("password1")
        user_data.pop("password2")
        groups = user_data.pop("groups")
        netperms = user_data.pop("netperms", [])
        # refs #12214 #352
        gid = ""
        if gidNumber:
            for group in self.groups_with_gid:
                if int(group[0]) == int(gidNumber):
                    gid = group[1]
        # refs #172
        # TODO: Maybe patch octofussd to ignore an empty string in controllerGroup instead of
        # raising an exception?
        if "controllerGroup" in user_data and (not user_data.get("controllerGroup", "")):
            del user_data['controllerGroup']
        # refs #192
        try:
            self.user = self.root_tree.lcreate(["users", "users", name], user_data)

            # Set password
            # refs #12214 #352
            if password:
                if gidNumber and gid and gid.startswith("docent"):
                    self.root_tree.lset(["users", "users", name, "password"], {'password': password, 'shadowLastChange': 0})
                else:
                    self.root_tree.lset(["users", "users", name, "password"], password)

            # Set groups
            for group in groups:
                self.root_tree.lcreate(["users", "users", name, "groups", group])

            # Set netperms
            for perm in netperms:
                self.root_tree.lcreate(["netperms", "byuser", name, perm])

            return super().form_valid(form)
        # refs #192 I wasn't able to make octofussd xmlrpc to throw a FileExistsError or a
        # ValueError or a custom exception (something like 'AlreadyExistingHomeException') *properly*.
        # So here I catch everything that octofussd thinks to throw at my face, indistinctly, and
        # show it in the messages area (I know, it will be better in the form fields' errors, but
        # here in the view I have at least access to octofussd (which I don't have in the form class),
        # and it's not easy (or possible?) to manipulate form errors from here.
        # TODO: Side effects?
        # TODO: What happens if (for some reason) not the first lcreate() fails?
        # FIXME: Problem: if the user fills all fields (the password ones too) and the home directory
        #        already exists, after the template rendering the password fields can be empty, unlike
        #        other fields.
        # Catch only the two possible exceptions
        except (oexceptions.HomeDirectoryExistsError, oexceptions.HomeDirectoryOutsideHomeError) as e:
            form['user'].add_error('homeDirectory', forms.ValidationError(str(e)))
            return super().form_invalid(form)
        except oexceptions.UserAlreadyExistsError as e:
            form['user'].add_error('name', forms.ValidationError(str(e)))
            return super().form_invalid(form)
        except ValueError as e:
            messages.error(
                self.request,
                "{name}: {message}".format(name=e.__class__.__name__, message=str(e)),
            )
            return super().form_invalid(form)


class UserSetEnabled(OctonetMixin, FormView):
    http_method_names = ['post']

    def post(self, request, *args, **kwargs):
        self.root_tree.lset(["users", "users", self.kwargs["uid"], "enabled"], self.request.POST["value"])
        if self.request.POST['value'] == '0':
            messages.info(request, _("User {} disabled").format(self.kwargs['uid']))
        else:
            messages.info(request, _("User {} enabled").format(self.kwargs['uid']))
        return redirect("users:detail", uid=self.kwargs["uid"])


# refs #12559
# refs #14151: Hide the button that made POST to this view
# class UserForcePasswordChange(OctonetMixin, FormView):
    # http_method_names = ['post']

    # def post(self, request, *args, **kwargs):
        # self.root_tree.lset(["users", "users", self.kwargs["uid"], "shadowLastChange"], 0)
        # messages.info(request, _("Password change forced for user {}").format(self.kwargs['uid']))
        # return redirect("users:detail", uid=self.kwargs["uid"])


class UserAddGroup(OctonetMixin, FormView):
    http_method_names = ['post']

    def post(self, request, *args, **kwargs):
        self.root_tree.lcreate(["users", "users", self.kwargs["uid"], "groups", self.request.POST["group"]])
        return redirect("users:detail", uid=self.kwargs["uid"])


# refs #213
class UserDelete(UserMixin, View):
    http_method_names = ["post"]

    def post(self, request, *args, **kw):
        self.root_tree.ldelete(["users", "users", self.kwargs["uid"]])
        messages.info(request, _("User %s deleted") % self.kwargs['uid'])
        return redirect("users:list")



class GroupMixin(OctonetMixin):
    def load_objects(self):
        super().load_objects()
        self.tree = self.octofuss_tree("/users/groups/{}".format(self.kwargs["name"]))
        self.group = self.tree.lget([])
        self.members = self.tree.lget(["members"])
        self.has_quota = self.root_tree.lhas(["quota"])
        self.all_groups = self.root_tree.lget(["users", "groups"])
        self.all_perms = self.root_tree.llist(["netperms", "bygroup"])

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        ctx["group"] = self.group
        ctx["members"] = self.members
        for user in ctx['members']:
            user['get_gecos_display'] = unescape(user['gecos'])
        ctx["has_quota"] = self.has_quota
        ctx["all_groups"] = self.all_groups
        ctx["all_perms"] = self.all_perms
        return ctx


class GroupList(OctonetMixin, TemplateView):
    template_name = "users/group_list.html"
    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        tree = self.octofuss_tree("/users/groups")
        ctx['groups'] = tree.lget([])
        return ctx


# refs #212
class GroupCreateForm(FormControlClassMixin, forms.Form):
    name = forms.CharField(label=_("Group name"))

    def __init__(self, *args, **kwargs):
        self.existing_groups = kwargs.pop('existing_groups')
        super().__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super().clean()
        name = cleaned_data.get('name')
        if name in self.existing_groups:
            raise forms.ValidationError(_("This group already exists"))
        return cleaned_data

    # refs #13930
    def clean_name(self):
        value = self.cleaned_data['name']
        # EXPLICIT regex to avoid problems with a-z and different locales
        # https://unicode.org/reports/tr18/tr18-5.1.html#Locale%20Dependent%20Ranges
        # Added "-" to acceptes char for group names: refs #15001
        regex = re.compile(r'^[qwertyuiopasdfghjklzxcvbnm0-9-]+$')
        if not regex.match(value):
            raise forms.ValidationError(_("The group name can contain only lowercase letters and numbers and hyphen"))
        return value


# refs #212
class GroupCreate(OctonetMixin, FormView):
    template_name = "users/group_create.html"
    form_class = GroupCreateForm

    def get_success_url(self):
        return reverse("users:group_detail", args=[self.group_name])

    def form_valid(self, form):
        self.group_name = form.cleaned_data.get('name')
        self.root_tree.lcreate(['users', 'groups', self.group_name])
        messages.info(self.request, _("Group %s created") % self.group_name)
        return super().form_valid(form)

    def get_form_kwargs(self):
        kw = super().get_form_kwargs()
        kw['existing_groups'] = [x['cn'] for x in self.root_tree.lget(['users', 'groups'])]
        return kw


class GroupDetail(GroupMixin, TemplateView):
    template_name = "users/group_detail.html"


class GroupEditForm(FormControlClassMixin, forms.Form):
    members = forms.MultipleChoiceField(label=_("Members"), required=False)


class GroupEdit(GroupMixin, FormView):
    template_name = "users/group_edit.html"
    form_class = GroupEditForm

    def get_success_url(self):
        return reverse("users:group_detail", kwargs=dict(name=self.group.get("cn")))

    def _get_custom_group_form(self):
        users = [(x, x) for x in self.root_tree.llist(["users", "users"])]
        class Form(self.form_class):
            def __init__(self, *args, **kw):
                super().__init__(*args, **kw)
                self.fields["members"].choices = users
        return Form

    def get_form_class(self):
        res = { "group": self._get_custom_group_form() }
        if self.has_quota and hasattr(self, "group"):
            res["quota"] = forms.formset_factory(QuotaForm, extra=0)
        return make_multiform(**res)

    def get_initial(self):
        res = {
            "members": self.tree.llist(["members"]),
        }
        if self.has_quota:
            quota = []
            for fs in self.root_tree.llist(["quota"]):
                data = self.root_tree.lget(["quota", fs, "group", self.group["cn"]])
                data["fs"] = fs
                quota.append(data)
            res["quota"] = quota
        return res

    def form_valid(self, form):
        user_form = form["group"]

        cur_members = frozenset(self.tree.llist(["members"]))
        new_members = frozenset(user_form.cleaned_data["members"])
        if cur_members != new_members:
            for member in cur_members - new_members:
                self.tree.ldelete(["members", member])
            for member in new_members - cur_members:
                self.tree.lcreate(["members", member])

        if self.has_quota:
            quota_form = form["quota"]
            for f in quota_form:
                path = ["quota", f.initial["fs"], "group", self.group["cn"]]
                if self.root_tree.lhas(path):
                    self.root_tree.lset(path, f.cleaned_data)

        return super().form_valid(form)

# refs #10018
class GroupDelete(GroupMixin, View):
    http_method_names = ["post"]

    def post(self, request, *args, **kw):
        group_name = self.kwargs.get("name")
        try:
            self.root_tree.ldelete(["users", "groups", group_name])
        except oexceptions.DeleteSystemGroupError as e:
            messages.error(request, str(e))
            return redirect("users:group_list")
        
        messages.info(request, _("Group %s deleted") % group_name)
        return redirect("users:group_list")

class GroupAddGroup(OctonetMixin, FormView):
    def post(self, request, *args, **kwargs):
        for user in self.root_tree.llist(["users", "groups", self.kwargs["name"], "members"]):
            self.root_tree.lcreate(["users", "users", user, "groups", self.request.POST["group"]])
        return redirect("users:group_detail", name=self.kwargs["name"])


class GroupRemoveGroup(OctonetMixin, FormView):
    def post(self, request, *args, **kwargs):
        for user in self.root_tree.llist(["users", "groups", self.kwargs["name"], "members"]):
            self.root_tree.ldelete(["users", "users", user, "groups", self.request.POST["group"]])
        return redirect("users:group_detail", name=self.kwargs["name"])


class GroupDropAllUsers(OctonetMixin, View):
    def do_drop_users(self):
        n_to_delete = len(self.to_delete)
        act = 0
        for user in self.to_delete:
            self.root_tree.ldelete(["users", "users", user])
            act += 1
            state = {
                "lastDeleted": str(user),
                "percentage": float(act)/n_to_delete*100,
                "totalUsers": n_to_delete,
                "finish": act == n_to_delete
            }
            yield json.dumps(state) + "\n"

    def post(self, request, *args, **kwargs):
        self.to_delete = self.root_tree.llist(["users", "groups", self.kwargs["name"], "members"])
        if not self.to_delete:
            state = {
                "lastDeleted": "",
                "percentage": 100,
                "totalUsers": 0,
                "finish": True
            }
            return http.HttpResponse(json.dumps(state) + "\n")
        response = http.StreamingHttpResponse(self.do_drop_users())
        return response


class GroupAddPermission(OctonetMixin, FormView):
    def post(self, request, *args, **kwargs):
        for user in self.root_tree.llist(["users", "groups", self.kwargs["name"], "members"]):
            self.root_tree.lcreate(["netperms", "byuser", user, self.request.POST["perm"]])
        return redirect("users:group_detail", name=self.kwargs["name"])


class GroupRemovePermission(OctonetMixin, FormView):
    def post(self, request, *args, **kwargs):
        for user in self.root_tree.llist(["users", "groups", self.kwargs["name"], "members"]):
            self.root_tree.ldelete(["netperms", "byuser", user, self.request.POST["perm"]])
        return redirect("users:group_detail", name=self.kwargs["name"])


class CSVImportForm(FormControlClassMixin, forms.Form):
    csv = forms.FileField(label=_("User file (.csv)"))
    delimiter = forms.CharField(max_length=1, widget=forms.HiddenInput())
    first_row = forms.IntegerField(widget=forms.HiddenInput())

    # All column fields are all integerfields, because they contain the column
    # index to be used from the CSV
    fullname = forms.IntegerField(widget=forms.HiddenInput(), required=False)
    name = forms.IntegerField(widget=forms.HiddenInput(), required=False)
    surname = forms.IntegerField(widget=forms.HiddenInput(), required=False)
    username = forms.IntegerField(widget=forms.HiddenInput())
    password = forms.IntegerField(widget=forms.HiddenInput(), required=False)
    homebase = forms.IntegerField(widget=forms.HiddenInput(), required=False)
    primarygroup = forms.IntegerField(widget=forms.HiddenInput(), required=False)
    controllergroup = forms.IntegerField(widget=forms.HiddenInput(), required=False)
    # refs #333
    secondarygroups = forms.IntegerField(widget=forms.HiddenInput(), required=False)

    def clean(self):
        cleaned_data = super().clean()

        fieldset = set()
        fields_to_check = [
            "fullname", "name", "surname", "username", "password",
            "homebase", "primarygroup", "controllergroup", "secondarygroups",
        ]
        for col in fields_to_check:
            # FIXME: IS IT GOOD?
            if col in cleaned_data and cleaned_data[col] is not None:
                fieldset.add(col)

        errors = []

        # Validate column selection
        if "fullname" in fieldset and "name" in fieldset:
            errors.append(forms.ValidationError(_("Full Name and Name cannot both be assigned"), code="fullname_name_conflict"))

        if "fullname" in fieldset and "surname" in fieldset:
            errors.append(forms.ValidationError(_("Full Name and Surname cannot both be assigned"), code="fullname_surname_conflict"))

        if "name" in fieldset and "surname" not in fieldset:
            errors.append(forms.ValidationError(_("Name is assigned but Surname is not"), code="name_but_not_surname"))

        if "name" not in fieldset and "surname" in fieldset:
            errors.append(forms.ValidationError(_("Surname is assigned but Name is not"), code="surname_but_not_name"))

        if errors:
            raise forms.ValidationError(errors)


class CSVImporter:
    re_username = re.compile("^[a-z.0-9_]+$")

    def __init__(self, tree, fields):
        self.tree = tree
        self.fields = fields
        self.imported_ok = 0
        self.errors = {}
        self.valid_groups = set(tree.llist(["users", "groups"]))
        self.users = []
        self.stop = False

    def add_error(self, rowidx, field, message, stop=True):
        # It seems that django.http.JsonResponse cannot serialize defaultdict...
        message = str(message)
        if rowidx in self.errors:
            if field in self.errors[rowidx]:
                self.errors[rowidx][field].append(message)
            else:
                self.errors[rowidx][field] = [message]
        else:
            self.errors[rowidx] = {field: [message]}
        if not self.stop:
            self.stop = stop

    def import_row(self, rowidx, row):
        # Read columns from the CSV
        vals = { name: row[col] for name, col in self.fields if col is not None }

        # Validate username
        username = vals["username"]
        if not self.re_username.match(username):
            self.add_error(rowidx, "username", _("Not a valid user name"))
        if self.tree.lhas(["users", "users", username]):
            self.add_error(rowidx, "username", _("User already exists"), False)
            messages.warning(self.request, _("%s will be ignored as it already exists" % username))
            return

        user = {
            "uid": username
        }

        if "homebase" in vals:
            homebase = vals["homebase"]
            if homebase and homebase[0] != '/':
                self.add_error(rowidx, "homebase", _("Path must start with a slash"))
            # refs #231, the key is homeDirectory with a capital D
            user["homeDirectory"] = os.path.join(homebase, username)

        if "fullname" in vals:
            user["gecos"] = vals["fullname"]

        if "name" in vals:
            user["gecos"] = vals["name"] + " " + vals["surname"]

        # refs #330
        primarygroup = vals.get("primarygroup", "")
        if primarygroup:
            if primarygroup not in self.valid_groups:
                self.add_error(rowidx, "primarygroup", _("Group does not exist"))
            else:
                user['primaryGroup'] = primarygroup

        controllergroup = vals.get("controllergroup", "")
        if controllergroup:
            # Check that controller group exists, as primary group
            if controllergroup not in self.valid_groups:
                self.add_error(rowidx, "controllergroup", _("Group does not exist"))
            # refs #231, the key is controllerGroup with a capital G
            user["controllerGroup"] = controllergroup

        # refs #333
        secondarygroups = vals.get("secondarygroups", "").strip()
        if secondarygroups:
            secondarygroups = [x for x in secondarygroups.split(" ") if x]
            for sg in secondarygroups:
                if not (sg in self.valid_groups):
                    # Create the group and add it to valid_groups
                    self.tree.lcreate(['users', 'groups', sg])
                    self.valid_groups.add(sg)
            user['secondaryGroups'] = secondarygroups

        password = vals.get("password", None)
        if password is not None:
            user["password"] = password

        if rowidx not in self.errors:
            self.users.append(user)
            self.imported_ok += 1


# refs #231
# refs #286
class CSVImportAJAX(NetpermsMixin, OctonetMixin, View):
    imported_users = []

    def users_generator(self, importer, tree):
        created = 0
        try:
            for user in importer.users:
                password = user.pop("password", "")
                # refs #12214 #352
                primaryGroup = user.get("primaryGroup", "")
                self.tree.create(user["uid"], user)
                if password:
                    # refs #12214 #352
                    if primaryGroup and primaryGroup.startswith("docent"):
                        self.tree.lset([user["uid"], "password"], {'password': password, 'shadowLastChange': 0})
                    else:
                        self.tree.lset([user["uid"], "password"], password)
                # refs #286
                # Set netperms
                for perm in self.default_netperms:
                    self.root_tree.lcreate(["netperms", "byuser", user['uid'], perm])
                # refs #333
                secondary_groups = user.pop("secondaryGroups", [])
                if secondary_groups:
                    for sg in secondary_groups:
                        self.root_tree.lcreate(['users', 'groups', sg, 'members', user['uid']])
                created += 1
                self.imported_users.append(user)
                percentage = (float(created) / importer.imported_ok) * 100
                # if percentage >= 50:
                    # raise Exception("Ne hai già importati circa la metà")
                yield "%d\n" % int(percentage)
        # refs #231: Catch all the exceptions...
        except Exception as e:
            # ...and send the message back to the ajax/streaming frontend as a \n terminated string
            data = json.dumps({
                'status': 'error',
                'percentage': "%d" % int(percentage),
                'error': str(e),
                'imported_users': self.imported_users,
            }) + "\n"
            yield data

    def post(self, *args, **kwargs):
        form = CSVImportForm(self.request.POST, self.request.FILES)
        if form.is_valid():
            self.tree = self.octofuss_tree("/users/users")

            # Read field indices
            fields = []
            # refs #333
            cols = [
                "fullname", "name", "surname", "username", "password", "homebase",
                "primarygroup", "controllergroup", "secondarygroups",
            ]
            for col in cols:
                # FIXME: IS IT GOOD?
                fields.append((col, form.cleaned_data.get(col, None)))

            # https://docs.python.org/3/library/codecs.html#encodings-and-unicode
            # "On decoding utf-8-sig will skip those three bytes if they appear as the first three bytes in the file."
            reader = iter(enumerate(csv.reader((x.decode("utf-8-sig") for x in self.request.FILES["csv"]), delimiter=form.cleaned_data["delimiter"])))
            # Skip first_row rows
            for idx in range(form.cleaned_data["first_row"]):
                next(reader)

            importer = CSVImporter(self.root_tree, fields)
            importer.request = self.request

            # Note, this isn't processing data as it gets uploaded: django saves it to a temp file first
            for rowidx, row in reader:
                importer.import_row(rowidx, row)

            if importer.errors and importer.stop:
                errors = sorted((row, sorted(rowerrs.items())) for row, rowerrs in importer.errors.items())
                # Return errors and don't try to import anything
                response = http.JsonResponse({
                    'csv_errors': errors,
                })
                return response
            else:
                response = http.StreamingHttpResponse(
                    self.users_generator(importer, self.tree),
                )
                return response
        else:
            # Hopefully we never land here...
            response = http.JsonResponse({
                'form_errors': form.errors,
            })
            return response


class CSVImport(OctonetMixin, TemplateView):
    template_name = "users/csv_import.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = CSVImportForm()
        return context


# refs #353
class CSVImportCheckForm(FormControlClassMixin, forms.Form):
    csv = forms.FileField(label=_("CSV file to check"))
    username_field = forms.IntegerField(
        # I think starting from 1 is more intuitive
        label=_("Username column (start counting from 1)"),
        widget=forms.NumberInput(attrs={'min': 1}),
    )

# refs #353
class CSVImportCheck(OctonetMixin, FormView):
    template_name = "users/csv_import_check.html"
    form_class = CSVImportCheckForm
    success_url = reverse_lazy("users:csvimport")
    re_username = re.compile("^[a-z.0-9_]+$")

    def form_valid(self, form):
        has_errors = False
        has_BOM = False
        has_duplicates = False
        duplicates = {}
        has_not_valid_usernames = False
        not_valid_usernames = []
        different_cols = False
        csv_file = self.request.FILES['csv']
        username_field = form.cleaned_data['username_field']
        # Check for BOM
        # https://stackoverflow.com/a/13591421
        data = csv_file.read()
        csv_file.seek(0)
        if data.startswith(codecs.BOM) or data.startswith(codecs.BOM_UTF8):
            encoding = "utf-8-sig"
            has_BOM = True
            has_errors = True
        else:
            encoding = "utf-8"
        # Euristically infer delimiter (support for ',' and ';')
        line = data.decode(encoding).split('\n')[1]
        delimiter = (line.count(',') > line.count(';')) and ',' or ';'
        # Rewind the file
        csv_file.seek(0)
        # InMemoryUploadedFile is a bytes object, csv library wants strings
        # https://stackoverflow.com/a/41911251
        decoded_file = csv_file.read().decode(encoding)
        io_string = io.StringIO(decoded_file)
        # Loop the file and (for now) print the rows
        first_row = True
        usernames = set()
        # The user is starting to count from 1
        row_counter = 0
        for row in csv.reader(io_string, delimiter=delimiter):
            # The user is starting to count from 1
            row_counter += 1
            if first_row:
                cols = len(row)
                first_row = False
            this_row_cols = len(row)
            # The user is starting to count from 1
            if username_field <= len(row):
                username = row[username_field - 1]
                if not self.re_username.match(username):
                    has_not_valid_usernames = True
                    has_errors = True
                    not_valid_usernames.append((row_counter, username))
                if username in usernames:
                    has_duplicates = True
                    has_errors = True
                    if username in duplicates:
                        duplicates[username].append(row_counter)
                    else:
                        duplicates[username] = [row_counter]
                else:
                    usernames.add(username)
            if cols and (this_row_cols != cols):
                different_cols = True
                has_errors = True
                # If there is even only one row with a different number of columns, the duplicate
                # usernames check has no meaning at all, because it's based on the column number
                # Also the not valid usernames has no meaning.
                has_duplicates = False
                duplicates = {}
                has_not_valid_usernames = False
                not_valid_usernames = []
                break
        if has_errors:
            error_messages = []
            if has_BOM:
                error_messages.append(_("The file has BOM"))
            if different_cols:
                error_messages.append(_("Not all rows have the same number of columns"))
            if has_duplicates:
                error_messages.append(_("There are duplicate usernames."))
            if has_not_valid_usernames:
                error_messages.append(_("There are not valid usernames"))
            messages.error(self.request, _("There are errors in the CSV file"))
            context = self.get_context_data()
            context.update({
                'filename': self.request.FILES['csv'].name,
                'csv_errors': error_messages,
                'duplicates': duplicates,
                'not_valid': not_valid_usernames,
                'form': self.form_class(),
            })
            return render(self.request, "users/csv_import_check.html", context)
        messages.success(self.request, _("The CSV file has no known errors, you can import it"))
        return redirect(self.get_success_url())


class MassCreateForm(FormControlClassMixin, forms.Form):
    user_prefix = forms.CharField(label=_("Username prefix"))
    amount = forms.IntegerField(min_value=2, max_value=50,
                                label=_("Number of users"))
    # refs #13224
    home_prefix = forms.CharField(label=_("Home prefix"))
    gidNumber = forms.ChoiceField(label=_("Primary group"), required=False)

    def clean_home_prefix(self):
        # Match letters, numbers, dash, underscore, slash
        regex = re.compile(r'^[a-zA-z0-9_/-]+$')
        value = self.cleaned_data['home_prefix']
        if not value.startswith('/home/'):
            raise forms.ValidationError(_("The prefix must start with /home/"))
        if not regex.match(value):
            raise forms.ValidationError(_("You can only use these characters: a-z A-Z 0-9 - _ /"))
        return value


# From https://docs.djangoproject.com/en/1.9/howto/outputting-csv/#streaming-large-csv-files
class Echo(object):
    """An object that implements just the write method of the file-like
    interface.
    """
    def write(self, value):
        """Write the value by returning it, instead of storing in a buffer."""
        return value


class MassCreate(NetpermsMixin, OctonetMixin, FormView):
    template_name = "users/masscreate.html"
    form_class = MassCreateForm

    def get_form_class(self):
        self.all_groups = self.root_tree.lget(["users", "groups"])
        self.all_groups = sorted(self.all_groups, key=lambda x: x.get("cn"))
        groups = [(quote_plus(x["cn"]), x["cn"]) for x in getattr(self, "all_groups", [])]
        groups_with_gid = [(x['gidNumber'], x['cn']) for x in getattr(self, "all_groups", [])]
        self.groups_with_gid = groups_with_gid

        class Form(self.form_class):
            def __init__(self, *args, **kw):
                super().__init__(*args, **kw)
                self.fields["gidNumber"].choices = [("", "")] + groups_with_gid

        return Form

    # refs #13224
    def get_initial(self):
        initial = super().get_initial()
        initial['home_prefix'] = "/home"
        return initial

    def get_success_url(self):
        return reverse("users:list")

    def form_valid(self, form):
        user_num = form.cleaned_data.get("amount")
        user_prefix = form.cleaned_data.get("user_prefix")
        # refs #13224
        home_prefix = form.cleaned_data.get("home_prefix", "/home")
        gidNumber = form.cleaned_data.get("gidNumber")
        created_rows = []
        # refs #13224
        header = ["username", "fullname", "home_directory", "password"]
        created_rows.append(header)

        for x in range(user_num):
            username = "{}{}".format(user_prefix, x+1)
            userpath = ["users", "users", username]
            password = password_generator.generate_simple()
            fullname = "{} n. {}".format(user_prefix.upper(),
                                         x+1)
            # refs #13224
            home_directory = os.path.join(home_prefix, username)
            try:
                # refs #13224
                self.root_tree.lcreate(userpath, {"gecos": fullname, 'homeDirectory': home_directory})
                self.root_tree.lset(userpath + ["password"], password)
                if gidNumber:
                    p = ["users", "users", username, "gidNumber"]
                    self.root_tree.lset(p, int(gidNumber))
                # refs #286
                # Set netperms
                for perm in self.default_netperms:
                    self.root_tree.lcreate(["netperms", "byuser", username, perm])
                # refs #13224
                row = [username, fullname, home_directory, password]
            except Exception as e:
                # refs #13224
                row = [username, "", "", "ALREADY EXISTS"]

            created_rows.append(row)


        pseudo_buffer = Echo()
        writer = csv.writer(pseudo_buffer)

        response = http.StreamingHttpResponse((writer.writerow(row) for row in created_rows),
                                              content_type="text/csv")
        response['Content-Disposition'] = 'attachment; filename="mass-created-users-{}.csv"'.format(datetime.date.today())
        response['Location'] = reverse("users:list")
        response.set_cookie("fileDownload", value="true")
        return response


class LDIFImportForm(FormControlClassMixin, forms.Form):
    ldif_file = forms.FileField(label=_("LDAP LDIF File (.ldif)"),
                                widget=forms.FileInput(attrs={'accept':'text/x-ldif'}))

class ProcessLDIF(OctonetMixin, FormView):
    template_name = "users/process_ldif.html"
    form_class = LDIFImportForm

    def get_success_url(self):
        return reverse("users:csvimport")

    def form_valid(self, form):
        f = form.cleaned_data.get("ldif_file")
        dest_name = f.name.replace(".ldif", "")
        data = f.read().decode("utf-8")
        splitted = data.split("\n\n")
        created_rows = []
        header = [
            "username", "fullname",
            "password", "home_prefix",
            "primary_group", "secondary_groups"
            ]

        created_rows.append(header)

        def parse_entry(entry):
            r = dict()
            for line in entry.split("\n"):
                s = line.split(":",1)
                if len(s) == 2:
                    attr = s[0]
                    val = s[1]
                else:
                    continue
                if attr not in r:
                    r[attr] = val.strip()
                else:
                    if not isinstance(r[attr], list):
                        t = r[attr]
                        r[attr] = []
                        r[attr].append(t)
                    r[attr].append(val.strip())
            return r

        # handle groups
        secondary_groups_map = defaultdict(list)
        group_gid_mapping = dict()
        groups = [x for x in splitted if "ou=Groups" in x]
        for g in groups:
            parsed = parse_entry(g)
            group_gid_mapping[parsed.get("gidNumber")] = parsed.get("cn")
            for member in parsed.get("memberUid", []):
                secondary_groups_map[member].append(parsed.get("cn"))

        users = [x for x in splitted if "ou=Users" in x]

        for u in users:
            parsed = parse_entry(u)
            if parsed.get("uid") in ['nobody','admin']:
                continue

            if 'posixAccount' not in parsed.get('objectClass'):
                continue

            username = parsed.get("uid")
            home_prefix = "/".join(parsed.get("homeDirectory").split("/")[:-1])
            password = password_generator.generate_simple()
            primary_group = group_gid_mapping.get(parsed.get("gidNumber"))
            fullname = ""
            if 'gecos' in parsed:
                fullname = parsed.get('gecos')
            else:
                fullname = username
            secondary_groups = " ".join(secondary_groups_map.get(username, []))

            row =  (username, fullname,
                    password, home_prefix,
                    primary_group, secondary_groups)

            created_rows.append(row)


        pseudo_buffer = Echo()
        writer = csv.writer(pseudo_buffer)

        response = http.StreamingHttpResponse((writer.writerow(row) for row in created_rows),
                                              content_type="text/csv")
        response["Content-Disposition"] = 'attachment; filename="{}.csv"'.format(dest_name)
        response['Location'] = reverse("users:list")
        response.set_cookie("fileDownload", value="true")
        return response
