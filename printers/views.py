from django.contrib import messages
from django.urls import reverse_lazy
from django.shortcuts import redirect
from django.views.generic import View, TemplateView, FormView
from django.utils.translation import gettext_lazy as _
from octonet.mixins import OctonetMixin
import printers.forms as pforms


class List(OctonetMixin, TemplateView):
    template_name = "printers/list.html"

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        ctx["queues"] = self.root_tree.llist(["printers", "byqueue"])
        return ctx


class QueueMixin(OctonetMixin):
    def load_objects(self):
        super().load_objects()
        # FIXME: What to do if a queue does not exist?
        self.queue = self.kwargs["queue"]

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        ctx["queue"] = self.queue
        return ctx


class Edit(QueueMixin, TemplateView):
    template_name = "printers/edit.html"

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        configured_hosts = self.root_tree.lget(["printers", "byqueue", self.queue])
        # Avoid exception on sort() method if there are no configured_hosts
        if configured_hosts:
            configured_hosts.sort()
        all_hosts = self.root_tree.llist(["computers"])
        ctx["queue_hosts"] = configured_hosts
        ctx["other_hosts"] = [h for h in all_hosts if h not in configured_hosts and ":actions" not in h]
        ctx["clusters"] = self.root_tree.llist(["cluster"])
        return ctx


class RemoveAllFromQueue(QueueMixin, View):
    def get(self, request, *args, **kwargs):
        configured_hosts = self.root_tree.lget(["printers", "byqueue", self.queue])
        for i in configured_hosts:
            self.root_tree.ldelete(["printers", "byqueue", self.queue, i])
        return redirect("printers:edit", queue=self.queue)


class Remove(QueueMixin, View):
    def post(self, request, *args, **kw):
        host = kw.get("host")
        # FIXME: What to do if a host does not exist?
        self.root_tree.ldelete(["printers", "byqueue", self.queue, host])
        messages.info(request, _("Host {} remove from queue {}").format(host, self.queue))
        return redirect("printers:edit", queue=self.queue)


class Add(QueueMixin, View):
    def post(self, request, *args, **kw):
        host = request.POST.get("host")
        self.root_tree.lcreate(["printers", "byqueue", self.queue, host])
        messages.info(request, _("Host {} added to queue {}").format(host, self.queue))
        return redirect("printers:edit", queue=self.queue)


class AddCluster(QueueMixin, View):
    def post(self, request, *args, **kw):
        cluster = request.POST.get("cluster")
        hosts = self.root_tree.llist(["cluster", cluster])
        for host in hosts:
            if ":actions:" in host: continue
            self.root_tree.lcreate(["printers", "byqueue", self.queue, host])
        messages.info(request, _("Cluster {} added to queue {}").format(cluster, self.queue))
        return redirect("printers:edit", queue=self.queue)


# refs #12376 #566
class Cleanup(OctonetMixin, FormView):
    template_name = "printers/cleanup.html"
    form_class = pforms.CleanupForm
    success_url = reverse_lazy("printers:list")

    def get_printers(self):
        printers_byqueue = self.root_tree.llist(["printers", "byqueue"])
        printers_byhost = []
        printers_to_keep = []
        printers_to_remove = []
        for host in self.root_tree.llist(["printers", "byhost"]):
            for printer in self.root_tree.llist(["printers", "byhost", host]):
                printers_byhost.append(printer)
                if printer in printers_byqueue:
                    printers_to_keep.append({'host': host, 'printer': printer})
                else:
                    printers_to_remove.append({'host': host, 'printer': printer})
        printers = {
            'byqueue': printers_byqueue,
            'byhost': printers_byhost,
            'to_keep': printers_to_keep,
            'to_remove': printers_to_remove,
        }
        return printers

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        printers = self.get_printers()
        context['printers_byqueue'] = printers['byqueue']
        context['printers_to_keep'] = printers['to_keep']
        context['printers_to_remove'] = printers['to_remove']
        return context

    def form_valid(self, form):
        printers = self.get_printers()
        print(printers['to_remove'])
        for printer in printers['to_remove']:
            print("Removing printer:", printer)
            self.root_tree.ldelete(["printers", "byhost", printer['host'], printer['printer']])
        messages.info(self.request, _("{} association(s) between host and printer removed").format(
            len(printers['to_remove'])))
        return super().form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, form.errors)
        return super().form_invalid(form)
