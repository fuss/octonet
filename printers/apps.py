# coding: utf-8
from django.apps import AppConfig
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from octonet.mixins import OctonetAppMixin

class Printers(OctonetAppMixin, AppConfig):
    name = 'printers'
    verbose_name = _("Network Printers")
    description = _("Manage networked printers")
    group = _("Network")
    main_url = reverse_lazy("printers:list")
    font_awesome_class = "fa-print"
    octofussd_url = "/printers"

