# coding: utf-8
from unittest import skipIf
from django.urls import reverse
from django.test import SimpleTestCase
import octofuss
from octonet import backend
from octonet.unittest import TestBase


class LoginMixin():
    def setUp(self):
        # Login
        self.client.post(
            reverse("login"),
            data={
                "username": "root",
                "password": "root",
                "server_url": "http://localhost:13400/conf/",
            }
        )


class TestPrinters(LoginMixin, TestBase, SimpleTestCase):
    """
    General views tests
    """
    def setUp(self):
        self.tree = octofuss.MockTree()
        # For printers:list
        self.tree.lcreate(
            ["printers", "byqueue", "test_queue"]
        )
        super().setUp()
        # Make it work after b0f11897be396e03fbf515e787ebdba4cddb9c7e (found thanks to git bisect)
        session = self.client.session
        session['apps'] = ['printers']
        session.save()

    def test_GET_list_gives_200(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("printers:list"))
            self.assertEqual(response.status_code, 200)

    def test_POST_list_gives_405(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("printers:list"))
            self.assertEqual(response.status_code, 405)

    # FIXME the code and then the test
    def test_GET_edit_non_existing_queue_gives_EXCEPTION(self):
        # FIXME: Has this to give an exception of some sort?
        self.tree.lcreate(["computers"], "test_computer")
        self.tree.lcreate(["cluster"], "test_cluster")
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("printers:edit", kwargs={'queue': 'non_existing'}))
            # For now test for HTTP 200, as this is the current backend behaviour
            self.assertEqual(response.status_code, 200)

    def test_GET_edit_existing_queue_gives_200(self):
        self.tree.lcreate(["computers"], "test_computer")
        self.tree.lcreate(["cluster"], "test_cluster")
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("printers:edit", kwargs={'queue': 'test_queue'}))
            self.assertEqual(response.status_code, 200)

    def test_POST_edit_non_existing_queue_gives_405(self):
        self.tree.lcreate(["computers"], "test_computer")
        self.tree.lcreate(["cluster"], "test_cluster")
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("printers:edit", kwargs={'queue': 'non_existing'}))
            self.assertEqual(response.status_code, 405)

    def test_POST_edit_existing_queue_gives_405(self):
        self.tree.lcreate(["computers"], "test_computer")
        self.tree.lcreate(["cluster"], "test_cluster")
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("printers:edit", kwargs={'queue': 'test_queue'}))
            self.assertEqual(response.status_code, 405)

    def test_GET_remove_non_existing_host_gives_405(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("printers:remove", kwargs={'queue': 'queue', 'host': 'host'}))
            self.assertEqual(response.status_code, 405)

    def test_GET_remove_existing_host_gives_405(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("printers:remove", kwargs={'queue': 'queue', 'host': 'host'}))
            self.assertEqual(response.status_code, 405)

    @skipIf(True, "")
    def test_POST_remove_non_existing_host_gives_UNMANAGED_EXCEPTION(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("printers:remove", kwargs={'queue': 'queue', 'host': 'host'}))
            self.fail("Finish the test when it's clear how the code must behave")
