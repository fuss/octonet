from django.urls import re_path
from . import views


app_name = "printers"
urlpatterns = [
    re_path(r"^$", views.List.as_view(), name="list"),
    re_path(r"^edit/(?P<queue>[^/]+)$", views.Edit.as_view(), name="edit"),
    re_path(r"^remove_all/(?P<queue>[^/]+)/$", views.RemoveAllFromQueue.as_view(), name="remove-all"),
    re_path(r"^remove/(?P<queue>[^/]+)/(?P<host>[^/]+)$", views.Remove.as_view(), name="remove"),
    re_path(r"^add/(?P<queue>[^/]+)$", views.Add.as_view(), name="add"),
    re_path(r"^add_cluster/(?P<queue>[^/]+)$", views.AddCluster.as_view(), name="add_cluster"),
    # refs #12376 #566
    re_path(r"^cleanup/$", views.Cleanup.as_view(), name="cleanup"),
]
