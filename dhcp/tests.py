from django.urls import reverse
from django.test import SimpleTestCase
import octofuss
from octonet import backend
from octonet.unittest import TestBase


class LoginMixin():
    def setUp(self):
        # Login
        self.client.post(
            reverse("login"),
            data={
                "username": "root",
                "password": "root",
                "server_url": "http://localhost:13400/conf/",
            }
        )


class TestDhcp(LoginMixin, TestBase, SimpleTestCase):
    def setUp(self):
        self.tree = octofuss.MockTree()
        super().setUp()
        # Make it work after b0f11897be396e03fbf515e787ebdba4cddb9c7e (found thanks to git bisect)
        session = self.client.session
        session['apps'] = ['dhcp']
        session.save()

    def test_list_GET(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("dhcp:list"))
            self.assertEquals(response.status_code, 200)

    def test_list_all_methods_give_403_if_app_not_active_in_session(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            session = self.client.session
            session['apps'] = []
            session.save()
            response = self.client.get(reverse("dhcp:list"))
            self.assertEqual(response.status_code, 403)
            response = self.client.post(reverse("dhcp:list"))
            self.assertEqual(response.status_code, 403)

    def test_static_entry_list_GET(self):
        self.tree.lcreate(["dhcp", "static"])
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("dhcp:static_entry_list"))
            self.assertEquals(response.status_code, 200)

    def test_static_entry_list_POST(self):
        self.tree.lcreate(["dhcp", "static"])
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("dhcp:static_entry_list"))
            self.assertEquals(response.status_code, 405)

    def test_static_entry_create_GET(self):
        self.tree.lcreate(["dhcp", "static"], {})
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("dhcp:static_entry_create"))
            self.assertEquals(response.status_code, 200)

    def test_static_entry_create_EMPTY_POST(self):
        self.tree.lcreate(["dhcp", "static"], {})
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("dhcp:static_entry_create"))
            self.assertEquals(response.status_code, 200)

    def test_static_entry_update_GET(self):
        self.tree.lcreate(
            ["dhcp", "static", "test"],
            {'mac': "11:11:11:11:11", 'address': "192.168.1.1", 'hostname': "test"},
        )
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("dhcp:static_entry_update", kwargs={'hostname': 'test'}))
            self.assertEquals(response.status_code, 200)

    def test_static_entry_update_EMPTY_POST(self):
        self.tree.lcreate(
            ["dhcp", "static", "test"],
            {'mac': "11:11:11:11:11", 'address': "192.168.1.1", 'hostname': "test"},
        )
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("dhcp:static_entry_update", kwargs={'hostname': 'test'}))
            self.assertEquals(response.status_code, 200)

    def test_static_entry_delete_GET(self):
        self.tree.lcreate(
            ["dhcp", "static", "test"],
            {'mac': "11:11:11:11:11", 'address': "192.168.1.1", 'hostname': "test"},
        )
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("dhcp:static_entry_delete", kwargs={'hostname': 'test'}))
            self.assertEquals(response.status_code, 405)

    def test_static_entry_delete_POST(self):
        self.tree.lcreate(
            ["dhcp", "static", "test"],
            {'mac': "11:11:11:11:11", 'address': "192.168.1.1", 'hostname': "test"},
        )
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("dhcp:static_entry_delete", kwargs={'hostname': 'test'}))
            self.assertEquals(response.status_code, 302)
            self.assertRedirectMatches(response, reverse("dhcp:static_entry_list"))
