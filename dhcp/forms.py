from django import forms
from django.core.validators import RegexValidator
from django.utils.translation import gettext_lazy as _
from octonet.forms import FormControlClassMixin


# refs #244
class StaticEntryUpdateForm(FormControlClassMixin, forms.Form):
    # https://django.readthedocs.io/en/1.8.x/ref/validators.html#regexvalidator
    mac_address = forms.CharField(
        label=_("MAC Address"),
        validators=[RegexValidator(
            # https://github.com/wtforms/wtforms/blob/2.1/wtforms/validators.py#L372
            regex=r'^(?:[0-9a-fA-F]{2}:){5}[0-9a-fA-F]{2}$',
            message=_("Please insert a valid MAC address"),
        )],
    )
    # https://django.readthedocs.io/en/1.8.x/ref/forms/fields.html#django.forms.GenericIPAddressField
    ip_address = forms.GenericIPAddressField(protocol="IPV4", label=_("IP Address"))

    def __init__(self, *args, **kwargs):
        self.ip_addresses = kwargs.pop('ip_addresses')
        self.mac_addresses = kwargs.pop('mac_addresses')
        super().__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super().clean()
        mac_address = cleaned_data.get("mac_address")
        ip_address = cleaned_data.get("ip_address")
        if mac_address and mac_address in self.mac_addresses:
            self.add_error('mac_address', forms.ValidationError(_("This MAC address is duplicated")))
        if ip_address and ip_address in self.ip_addresses:
            self.add_error('ip_address', forms.ValidationError(_("This IP address is duplicated")))
        return cleaned_data


# refs #244
class StaticEntryCreateForm(StaticEntryUpdateForm):
    hostname = forms.CharField(label=_("Hostname"))

    def __init__(self, *args, **kwargs):
        self.hostnames = kwargs.pop('hostnames')
        super().__init__(*args, **kwargs)
        # self.fields used to be an OrderedDict, but it seems it is no
        # more
        # https://docs.djangoproject.com/en/4.1/ref/forms/api/#django.forms.Form.field_order
        self.order_fields(["hostname"])

    def clean_hostname(self):
        value = self.cleaned_data.get('hostname', "")
        if "." in value:
            raise forms.ValidationError(_('Please do not use dot characters (".") in hostname'))
        return value

    def clean(self):
        # Make the superclass cleaning, so mac and ip address checkimg is already taken care of.
        cleaned_data = super().clean()
        # Check also hostname for duplicates
        hostname = cleaned_data.get('hostname')
        if hostname and hostname in self.hostnames:
            self.add_error('hostname', forms.ValidationError(_("This hostname is duplicated")))
        return cleaned_data
