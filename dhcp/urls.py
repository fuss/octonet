from django.urls import re_path
from . import views

app_name = "dhcp"
urlpatterns = [
    re_path(r"^$", views.List.as_view(), name="list"),
    # refs #244
    re_path(r"^static/list$", views.StaticEntryList.as_view(), name="static_entry_list"),
    re_path(
        r"^static/create$",
        views.StaticEntryCreate.as_view(),
        name="static_entry_create",
    ),
    # Is this the right regexp for a hostname?
    re_path(
        r"^static/(?P<hostname>[\w_\-]+)/update$",
        views.StaticEntryUpdate.as_view(),
        name="static_entry_update",
    ),
    # Is this the right regexp for a hostname?
    re_path(
        r"^static/(?P<hostname>[\w_\-]+)/delete$",
        views.StaticEntryDelete.as_view(),
        name="static_entry_delete",
    ),
]
