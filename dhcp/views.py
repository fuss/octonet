# coding: utf-8
from django.contrib import messages
from django.urls import reverse_lazy
from django.shortcuts import redirect
from django.utils.translation import gettext_lazy as _
from django.views.generic import TemplateView, FormView, View
from octonet.mixins import OctonetMixin
from . import forms as dforms


class List(OctonetMixin, TemplateView):
    """
    List dhcp leases
    """
    template_name = "dhcp/list.html"

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        tree = self.octofuss_tree("/dhcp")
        ctx['leases'] = tree.get("leases")
        return ctx


class StaticEntryList(OctonetMixin, TemplateView):
    """
    List static entries
    """
    template_name = "dhcp/static_entry_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['entries'] = [self.root_tree.lget(["dhcp", "static", x]) for x in self.root_tree.llist(["dhcp", "static"])]
        return context


# refs #244
class GetStaticEntryDataMixin():
    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        entries = self.root_tree.lget(['dhcp', 'static'])
        # If self.hostname exists, the user is editing an existing entry, so
        if getattr(self, 'hostname', None):
            # Skip this entry and do not consider existing hostnames,
            # because the user cannot change it
            del entries[self.hostname]
        # Otherwise the user is creating a new entry
        else:
            # So consider existing hostnames to avoid duplicate entries
            hostnames = [x['hostname'] for k,x in entries.items()]
            kwargs['hostnames'] = hostnames
        mac_addresses = [x['mac'] for k,x in entries.items()]
        ip_addresses = [x['address'] for k,x in entries.items()]
        kwargs['mac_addresses'] = mac_addresses
        kwargs['ip_addresses'] = ip_addresses
        return kwargs


class StaticEntryCreate(OctonetMixin, GetStaticEntryDataMixin, FormView):
    """
    Create a static entry
    """
    template_name = "dhcp/static_entry_form.html"
    form_class = dforms.StaticEntryCreateForm
    fields = ['hostname', 'mac_address', 'ip_address']
    success_url = reverse_lazy('dhcp:static_entry_list')

    def form_valid(self, form):
        # Save the data
        hostname = form.cleaned_data['hostname']
        mac_address = form.cleaned_data['mac_address']
        ip_address = form.cleaned_data['ip_address']
        self.root_tree.lcreate(
            ['dhcp', 'static', hostname],
            {'mac': mac_address, 'address': ip_address, 'hostname': hostname},
        )
        messages.success(self.request, _("DHCP static entry saved"))
        return super().form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, _("DHCP static entry not saved"))
        return super().form_invalid(form)


class StaticEntryUpdate(OctonetMixin, GetStaticEntryDataMixin, FormView):
    """
    Update a static entry
    """
    template_name = "dhcp/static_entry_form.html"
    form_class = dforms.StaticEntryUpdateForm
    success_url = reverse_lazy('dhcp:static_entry_list')

    def dispatch(self, request, *args, **kwargs):
        self.hostname = kwargs.get('hostname', '')
        response = super().dispatch(request, *args, **kwargs)
        return response

    def get_initial(self):
        initial = super().get_initial()
        data = self.root_tree.lget(['dhcp', 'static', self.hostname])
        initial['mac_address'] = data['mac']
        initial['ip_address'] = data['address']
        return initial

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['hostname'] = self.hostname
        return context

    def form_valid(self, form):
        # Save the data
        mac_address = form.cleaned_data['mac_address']
        ip_address = form.cleaned_data['ip_address']
        self.root_tree.lset(['dhcp', 'static', self.hostname, 'mac'], mac_address)
        self.root_tree.lset(['dhcp', 'static', self.hostname, 'address'], ip_address)
        messages.success(self.request, _("DHCP static entry saved"))
        return super().form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, _("DHCP static entry not saved"))
        return super().form_invalid(form)


class StaticEntryDelete(OctonetMixin, View):
    """
    Delete a static entry
    """
    http_method_names = ['post']

    def get_success_url(self):
        return redirect('dhcp:static_entry_list')

    # For error redirects, which incidentally is the same as success redirects, but it's overridable
    def get_redirect(self):
        return self.get_success_url()

    def post(self, request, *args, **kwargs):
        hostname = kwargs.get("hostname", "")
        if not hostname:
            messages.error(request, _("No hostname specified"))
            return self.get_redirect()
        if not self.root_tree.lhas(['dhcp', 'static', hostname]):
            messages.error(request, _("Hostname does not exist"))
            return self.get_redirect()
        # Delete
        self.root_tree.ldelete(['dhcp', 'static', hostname])
        messages.success(request, _("DHCP static entry deleted"))
        return self.get_success_url()
