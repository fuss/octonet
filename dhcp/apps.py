# coding: utf-8
from django.apps import AppConfig
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from octonet.mixins import OctonetAppMixin

class Dhcp(OctonetAppMixin, AppConfig):
    name = 'dhcp'
    verbose_name = _("DHCP leases")
    description = _("Browse issued dhcp leases")
    group = _("Hosts")
    main_url = reverse_lazy("dhcp:list")
    font_awesome_class = "fa-handshake-o"
    octofussd_url = "/dhcp"
