from django.urls import re_path
from . import views


app_name = "auth"
urlpatterns = [
    re_path(r"^$", views.List.as_view(), name="list"),
    re_path(r"^edit/(?P<user>[^/]+)$", views.Edit.as_view(), name="edit"),
]
