# coding: utf-8
from django.apps import AppConfig
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from octonet.mixins import OctonetAppMixin

class Auth(OctonetAppMixin, AppConfig):
    weight = 11
    name = 'auth'
    verbose_name = _("User authorization")
    description = _("Manage user authorizations")
    group = _("Users")
    main_url = reverse_lazy("auth:list")
    font_awesome_class = "fa-pie-chart"
    octofussd_url = "/auth"

    @classmethod
    def is_active(cls, tree):
        # refs #249
        # For this plugin to be active, also the users plugin must be active
        return tree.has(cls.octofussd_url) and tree.has("/users")
