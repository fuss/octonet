from django import forms
from django.views.generic import TemplateView
from django.views.generic.edit import FormView
from django.urls import reverse
from octonet.mixins import OctonetMixin
from octonet.forms import FormControlClassMixin

class List(OctonetMixin, TemplateView):
    template_name = "auth/list.html"

    def load_objects(self):
        super().load_objects()
        self.users = self.root_tree.llist(["users", "users"])

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        #ctx["apps"] = self.octofuss_apps()
        ctx["users"] = self.users
        return ctx


class Edit(OctonetMixin, FormView):
    template_name = "auth/edit.html"

    # Avoid AttributeError: 'FussManager' object has no attribute 'octofussd_url'
    def octofuss_apps(self):
        res = super().octofuss_apps()
        return [x for x in res if hasattr(x, "octofussd_url")]

    def load_objects(self):
        super().load_objects()
        self.users = self.root_tree.llist(["users", "users"])

    def get_form_class(self):
        fields = {}
        for app in self.octofuss_apps():
            fields[app.octofussd_url[1:]] = forms.BooleanField(required=False,
                                                  label=app.verbose_name)
        fields["serverinfo"] = forms.BooleanField(required=False)
        return type("Form", (FormControlClassMixin, forms.Form), fields)

    def get_initial(self):
        auths = self.root_tree.llist(["auth", "access", self.kwargs["user"]])
        if not auths: return {}
        return { a: True for a in auths }

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        ctx["apps"] = self.octofuss_apps()
        ctx["users"] = self.users
        ctx["username"] = self.kwargs["user"]
        return ctx

    def get_success_url(self):
        return reverse("auth:edit", args=[self.kwargs["user"]])

    def form_valid(self, form):
        all_apps = [x.octofussd_url[1:] for x in self.octofuss_apps()]
        all_apps += ['serverinfo']
        for app in all_apps:
            path = ["auth", "access", self.kwargs["user"], app]
            cur = self.root_tree.lhas(path)
            new = form.cleaned_data.get(app, False)
            if cur == new: continue
            if new:
                # FIXME: this seems an inconsistency in octofussd, which wants
                #   create /auth/access/username app
                # instead of
                #   create /auth/access/username/app
                self.root_tree.lcreate(path[:-1], path[-1])
                if app == "users":
                    path = ["auth", "access", self.kwargs["user"], "netperms"]
                    self.root_tree.lcreate(path[:-1], path[-1])
                # refs #12375 #564
                # To make the user see /computers and /cluster, add manually also /cluster
                # here, after that /computers was added automagically by the first .lcreate() with
                # the name of the app (that is, obviously, 'computers') in the path.
                if app == "computers":
                    path = ["auth", "access", self.kwargs["user"], "cluster"]
                    self.root_tree.lcreate(path[:-1], path[-1])
            else:
                self.root_tree.ldelete(path)
        return super().form_valid(form)
