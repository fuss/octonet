# coding: utf-8
from django.urls import reverse
from django.test import SimpleTestCase
import octofuss
from octonet import backend
from octonet.unittest import TestBase


class LoginMixin():
    def setUp(self):
        # Login
        self.client.post(
            reverse("login"),
            data={
                "username": "root",
                "password": "root",
                "server_url": "http://localhost:13400/conf/",
            }
        )


class TestAuth(LoginMixin, TestBase, SimpleTestCase):
    """
    General views tests
    """
    def setUp(self):
        self.tree = octofuss.MockTree()
        # For auth:list
        self.tree.lcreate(
            ["users", "users"],
            {
                'sambaPrimaryGroupSID': 'SIDPREFIX-2000', 'loginShell': '/bin/false', 'gidNumber': '500',
                'homeDirectory': '/home/testuser', 'gecos': 'Test User', 'enabled': True,
                'sambaSID': 'SIDPREFIX-4896', 'ou': 'studenti', 'uid': 'testuser',
                'controllerGroup': 'prototype', 'uidNumber': '666',
            },
        )
        # For auth:edit
        self.tree.lcreate(["auth", "access", "testuser"], [])
        super().setUp()
        # Make it work after b0f11897be396e03fbf515e787ebdba4cddb9c7e (found thanks to git bisect)
        session = self.client.session
        session['apps'] = ['auth']
        session.save()

    def test_GET_list_gives_200(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("auth:list"))
            self.assertEqual(response.status_code, 200)

    def test_POST_list_gives_405(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("auth:list"))
            self.assertEqual(response.status_code, 405)

    def test_GET_edit_gives_200(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("auth:edit", kwargs={'user': 'testuser'}))
            self.assertEqual(response.status_code, 200)

    def test_POST_edit_gives_302(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("auth:edit", kwargs={'user': 'testuser'}))
            self.assertEqual(response.status_code, 302)
            self.assertRedirectMatches(response, reverse("auth:edit", kwargs={'user': 'testuser'}))

    def test_all_methods_give_403_if_app_not_active_in_session(self):
        session = self.client.session
        session['apps'] = []
        session.save()
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("auth:list"))
            self.assertEqual(response.status_code, 403)
            response = self.client.post(reverse("auth:list"))
            self.assertEqual(response.status_code, 403)
            response = self.client.get(reverse("auth:edit", kwargs={'user': 'testuser'}))
            self.assertEqual(response.status_code, 403)
            response = self.client.post(reverse("auth:edit", kwargs={'user': 'testuser'}))
            self.assertEqual(response.status_code, 403)
