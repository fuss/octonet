# coding: utf-8
from urllib.parse import urlparse, urlunparse

from django.core.exceptions import PermissionDenied
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django import http, apps
from django.contrib import messages
from . import backend
import octofuss

REDIRECT_FIELD_NAME = "next"


def redirect_to_login(next, redirect_field_name=REDIRECT_FIELD_NAME, octofussd_disconnected=False):
    """
    Redirects the user to the login page, passing the given 'next' page
    """
    login_url = reverse("login")
    login_url_parts = list(urlparse(login_url))
    if redirect_field_name:
        querystring = http.QueryDict(login_url_parts[4], mutable=True)
        querystring[redirect_field_name] = next
        if octofussd_disconnected:
            querystring["reconnect"] = "true"
        login_url_parts[4] = querystring.urlencode(safe='/')
    return http.HttpResponseRedirect(urlunparse(login_url_parts))


def redirect_to_octofussd_error(next, redirect_field_name=REDIRECT_FIELD_NAME):
    """
    Redirects the user to the login page, passing the given 'next' page
    """
    dest_url = reverse("octofussd_error")
    dest_url_parts = list(urlparse(dest_url))
    if redirect_field_name:
        querystring = http.QueryDict(dest_url_parts[4], mutable=True)
        querystring[redirect_field_name] = next
        dest_url_parts[4] = querystring.urlencode(safe='/')
    return http.HttpResponseRedirect(urlunparse(dest_url_parts))


class OctonetMixin:
    def octofuss_tree(self, root=None):
        if root is None or root == "/":
            return self.root_tree
        return octofuss.Subtree(self.root_tree, root)

    def octofuss_apps(self):
        from django.apps import apps

        app_names = self.request.session.get("apps", None)
        if app_names is not None:
            return [apps.get_app_config(name) for name in app_names]
        else:
            # TODO: To speed up requests especially when working on remote
            # octofussd servers, we can cache the list of detected app names in the
            # session, and reuse them on further queries
            # https://work.fuss.bz.it/issues/65
            res = []
            for app in apps.get_app_configs():
                if not getattr(app, "octonet_plugin", False):
                    continue
                if not app.is_active(self.octofuss_tree()):
                    continue
                res.append(app)
            res.sort(key=lambda app: (getattr(app, "weight", 100), app.group, app.name))
            self.request.session["apps"] = [a.name for a in res]
            return res

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        ctx["user"] = self.request.user
        current_app = getattr(self.request, "current_app", None)
        if current_app:
            ctx["app"] = apps.apps.get_app_config(current_app)
        return ctx

    def load_objects(self):
        pass

    def dispatch(self, request, *args, **kw):
        if not request.user.is_authenticated():
            return redirect_to_login(request.build_absolute_uri())

        # refs #255
        resolver_match = self.request.resolver_match
        apps = self.request.session.get("apps", [])
        # Allow to visit home, at least :)
        if resolver_match.url_name not in ["home", "newsfeed"]:
            if resolver_match.app_name not in apps:
                raise PermissionDenied

        if not getattr(request, "current_app", None):
            request.current_app = resolver_match.app_name

        self.root_tree = self.request.user.tree
        # Perform one query to validate the api key
        try:
            # refs #249
            self.root_tree.lhas(["status"])
        except octofuss.xmlrpc.APIKeyException as e:
            disconnected = e.args[0] == "Invalid API key"
            if not disconnected:
                messages.error(request, _("Octofussd error: {}").format(str(e)))
            backend.logout(request)
            return redirect_to_login(request.build_absolute_uri(), octofussd_disconnected=disconnected)
        except ConnectionRefusedError:
            messages.error(request, _("Connection to {} was refused").format(request.user.server_url))
            return redirect_to_octofussd_error(request.build_absolute_uri())

        self.load_objects()

        return super().dispatch(request, *args, **kw)


class OctonetAppMixin(object):
    octonet_plugin = True

    @classmethod
    def is_active(cls, tree):
        return tree.has(cls.octofussd_url)
