# coding: utf-8
# Copyright (C) 2021-2022 The FUSS Project <info@fuss.bz.it>
# Author: Marco Marinello <contact-nohuman@marinello.bz.it>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program or from the site that you downloaded it
# from; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA  02111-1307   USA

import re
import mimetypes
from urllib.parse import urlparse
from urllib.request import urlopen
from django import forms, http
from django.conf import settings
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.views.generic import View, TemplateView
from django.views.generic.edit import FormView
import json
from . import backend
from .forms import FormControlClassMixin
from .mixins import OctonetMixin, REDIRECT_FIELD_NAME


def get_server_list():
    re_localhost = re.compile(r"^localhost(?::\d+)?$")
    res = []
    for url, name in settings.OCTONET_MANAGED_SERVERS:
        parts = urlparse(url)
        # FIXME: remote hosts should be allowed only with https
        #if parts.scheme == "http" and not re_localhost.match(parts.netloc):
        #    continue
        res.append((url, name))
    return res


class LoginForm(FormControlClassMixin, forms.Form):
    server_url = forms.ChoiceField(label=_("Server"),
                                   required=True,
                                   choices=get_server_list)
    username = forms.CharField(required=True)
    password = forms.CharField(required=True,
                               widget=forms.PasswordInput())

    def __init__(self, *args, **kw):
        super().__init__(*args, **kw)
        server_url_field = self.fields["server_url"]
        server_url_field.choices = get_server_list()
        if len(server_url_field.choices) == 1:
            server_url_field.widget = forms.TextInput(attrs={"class": "form-control", "readonly": "readonly"})

    def clean(self):
        cleaned_data = super().clean()

        if "server_url" not in cleaned_data: return cleaned_data
        if "username" not in cleaned_data: return cleaned_data
        if "password" not in cleaned_data: return cleaned_data

        # Validate password with octofussd
        try:
            api_key = backend.authenticate(cleaned_data["server_url"], cleaned_data["username"], cleaned_data["password"])
        except backend.AuthenticationError as e:
            raise forms.ValidationError(_("Login failed: {}").format(e))
        cleaned_data["api_key"] = api_key

        return cleaned_data


class IntermediatePageMixin:
    redirect_field_name = REDIRECT_FIELD_NAME

    def get_redirect_to(self):
        redirect_to = self.request.POST.get(self.redirect_field_name,
                self.request.GET.get(self.redirect_field_name, ''))
        if not redirect_to:
            redirect_to = reverse("home")
        return redirect_to

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        ctx["redirect_field_name"] = self.redirect_field_name
        ctx["redirect_field_value"] = self.get_redirect_to()
        return ctx


class Login(IntermediatePageMixin, FormView):
    template_name = "octonet/login.html"
    form_class = LoginForm
    initial = {
        "server_url": "http://localhost:13400/conf/",
    }

    def dispatch(self, *args, **kwargs):
        # refs #92
        if self.request.user.is_authenticated():
            return http.HttpResponseRedirect(self.get_redirect_to())
        response = super().dispatch(*args, **kwargs)
        return response

    def form_valid(self, form):
        backend.login(self.request, form.cleaned_data["server_url"], form.cleaned_data["username"], form.cleaned_data["api_key"])
        return http.HttpResponseRedirect(self.get_redirect_to())

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        ctx["reconnect"] = self.request.GET.get("reconnect", "false").lower() == "true"
        return ctx


class Logout(View):
    redirect_field_name = REDIRECT_FIELD_NAME

    def get(self, request, *args, **kw):
        redirect_to = self.request.POST.get(self.redirect_field_name,
                self.request.GET.get(self.redirect_field_name, ''))
        if not redirect_to:
            redirect_to = reverse("home")

        backend.logout(request)

        return http.HttpResponseRedirect(redirect_to)


class NewsFeed(OctonetMixin, View):
    def get(self, request, *args, **kw):
        url = settings.NEWS_FEED_URL
        print("OPENING", url)
        try:
            r = urlopen(url)
            status_code = r.getcode()
            mimetype = r.headers.get_content_type() or mimetypes.guess_type(url)
            content = r.read()
        except Exception as e:
            return http.HttpResponse(e.msg, status=status_code, content_type='text/plain')
        else:
            return http.HttpResponse(content, status=status_code, content_type=mimetype)

class Home(OctonetMixin, TemplateView):
    template_name = "octonet/home.html"

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        root = self.octofuss_tree("/serverinfo")
        ctx['serverinfo'] = []
        if root.has("/"):
            serverinfo = root.get("/")
            ctx['filesystems'] = serverinfo.pop("filesystems", [])
            ctx['load_avg'] = serverinfo.pop("load_avg", [])
            ctx['last_seen_clients'] = serverinfo.pop("last_seen_clients", [])
            sorted_keys = ['organization', 'kernel_version', 'ram_mbyte']
            if serverinfo:
                for key in sorted_keys:
                    if key in serverinfo:
                        ctx['serverinfo'].append((root.doc(key), serverinfo[key]))
        return ctx


class OctofussdError(IntermediatePageMixin, TemplateView):
    template_name = "octonet/octofussd_error.html"


class VeyonClientConfiguration(View):
    def read_clusters(self):
        with open("/etc/clusters") as a:
            content = a.read()
        clusters = {}
        for line in content.split("\n"):
            if len(line.split(" ")) > 1:
                line = line.split(" ")
                clusters[line[0]] = line[1:]
        return clusters

    def get_host_clusters(self, hostname, clusters):
        out = []
        for cluster in clusters:
            if hostname in clusters[cluster]:
                out.append(cluster)
        return out

    def get(self, request):
        if "hostname" not in self.request.GET:
            return http.HttpResponse("Bad request", status=400)
        clusters = self.read_clusters()
        # TODO: Find an efficient way to also provide MAC addresses
        host_clusters = [
            {"name": a, "computers": clusters[a]}
            for a in self.get_host_clusters(self.request.GET["hostname"], clusters)
        ]
        return http.HttpResponse(
            json.dumps(host_clusters), content_type="application/json"
        )
