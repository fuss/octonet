from django.conf.urls import include
from django.urls import re_path
from . import views

urlpatterns = [
    re_path(r"^$", views.Home.as_view(), name="home"),
    re_path(r"^login/$", views.Login.as_view(), name="login"),
    re_path(r"^logout/$", views.Logout.as_view(), name="logout"),
    re_path(r"^newsfeed/$", views.NewsFeed.as_view(), name="newsfeed"),
    re_path(r"^veyon/client_conf$", views.VeyonClientConfiguration.as_view(), name="veyon-client-conf"),
    re_path(r"^octofussd-error/$", views.OctofussdError.as_view(), name="octofussd_error"),
    re_path(r'^i18n/', include('django.conf.urls.i18n')),
    re_path(r"^hostqueue/", include("hostqueue.urls", namespace="hostqueue")),
    re_path(r"^dhcp/", include("dhcp.urls", namespace="dhcp")),
    re_path(r"^polygen/", include("polygen.urls", namespace="polygen")),
    re_path(r"^firewall/", include("firewall.urls", namespace="firewall")),
    re_path(r"^dansguardian/", include("dansguardian.urls", namespace="dansguardian")),
    re_path(r"^host/", include("host.urls", namespace="host")),
    re_path(r"^upgrade/", include("upgrade.urls", namespace="upgrade")),
    re_path(r"^asterisk/", include("asterisk.urls", namespace="asterisk")),
    re_path(r"^samba/", include("samba.urls", namespace="samba")),
    re_path(r"^users/", include("users.urls", namespace="users")),
    re_path(r"^script/", include("script.urls", namespace="script")),
    re_path(r"^quota/", include("quota.urls", namespace="quota")),
    re_path(r"^printers/", include("printers.urls", namespace="printers")),
    re_path(r"^services/", include("services.urls", namespace="services")),
    re_path(r"^auth/", include("auth.urls", namespace="auth")),
    re_path(r"^fussmanager/", include("fussmanager.urls", namespace="fussmanager")),
]
