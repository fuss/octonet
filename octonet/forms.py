from django.forms.widgets import CheckboxInput, FileInput

class FormControlClassMixin:
    """
    Add class=form-control to all widgets in the form
    """
    def __init__(self, *args, **kw):
        super().__init__(*args, **kw)
        for f in self.fields.values():
            if isinstance(f.widget,  CheckboxInput):
                continue
            if isinstance(f.widget, FileInput):
                continue
            cur_class = f.widget.attrs.get("class", "")
            if cur_class:
                f.widget.attrs["class"] = " ".join(cur_class, "form-control")
            else:
                f.widget.attrs["class"] = "form-control"
