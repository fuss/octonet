from octonet.unittest import TestBase
from django.urls import reverse
from django.test import SimpleTestCase, Client

class TestLogin(TestBase, SimpleTestCase):
    def test_login(self):
        good_server = "http://localhost:13400/conf/"
        bad_server = "http://example.org/conf/"

        with self.settings(OCTONET_MANAGED_SERVERS=[(good_server, good_server), (bad_server, bad_server)]):
            response = self.client.get(reverse("login"))
            self.assertEquals(response.status_code, 200)

            response = self.client.post(reverse("login"), data={"username": "root", "password": "incorrect", "server_url": good_server})
            self.assertEquals(response.status_code, 200)
            self.assertEquals(response.context["form"].errors["__all__"], ['Login failed: Invalid credentials'])

            response = self.client.post(reverse("login"), data={"username": "root", "password": "root", "server_url": bad_server})
            self.assertEquals(response.status_code, 200)
            self.assertEquals(response.context["form"].errors["__all__"], ['Login failed: %s: Not Found' % bad_server])
            response = self.client.post(reverse("login"), data={"username": "root", "password": "root", "server_url": good_server})
            self.assertRedirectMatches(response, reverse("home"))

            response = self.client.post(reverse("login") + "?next=/foo", data={"username": "root", "password": "root", "server_url": good_server})
            self.assertRedirectMatches(response, "/foo")

    def test_login_twice(self):
        good_server = "http://localhost:13400/conf/"
        bad_server = "http://example.org/conf/"

        with self.settings(OCTONET_MANAGED_SERVERS=[(good_server, good_server), (bad_server, bad_server)]):
            response = self.client.post(reverse("login") + "?next=/foo", data={"username": "root", "password": "root", "server_url": good_server})
            self.assertRedirectMatches(response, "/foo")

            response = self.client.get(reverse("login") + "?next=/foo")
            self.assertRedirectMatches(response, "/foo")

    def test_login_choices(self):
        with self.settings(OCTONET_MANAGED_SERVERS=[("foo", "Foo")]):
            response = self.client.post(reverse("login"), data={"username": "root", "password": "root", "server_url": "bar"})
            self.assertEquals(response.status_code, 200)
            self.assertIn("Select a valid choice", response.context["form"].errors["server_url"][0])

    def test_logout(self):
        response = self.client.get(reverse("home"))
        self.assertRedirectMatches(response, reverse("login"))

        response = self.client.post(reverse("login"), data={"username": "root", "password": "root", "server_url": "http://localhost:13400/conf/"})
        self.assertRedirectMatches(response, reverse("home"))

        response = self.client.get(reverse("home"))
        self.assertEquals(response.status_code, 200)

        response = self.client.get(reverse("logout"))
        self.assertRedirectMatches(response, reverse("home"))

        response = self.client.get(reverse("home"))
        self.assertRedirectMatches(response, reverse("login"))
