from . import backend
from django.conf import settings
from django.utils.deprecation import MiddlewareMixin
from datetime import datetime
import logging


requests_logger = logging.getLogger('django.request')


class AuthenticationMiddleware(MiddlewareMixin):
    """
    Add request.user to request
    """
    def process_request(self, request):
        assert hasattr(request, 'session'), (
            "The Octonet authentication middleware requires session middleware "
            "to be installed. Edit your MIDDLEWARE_CLASSES setting to insert "
            "'django.contrib.sessions.middleware.SessionMiddleware' before "
            "'octonet.middleware.AuthenticationMiddleware'."
        )
        test_user = getattr(settings, "TEST_USER", None)
        if test_user is not None:
            request.user = test_user
        elif request.session.get(backend.SESSION_APIKEY_KEY, None):
            request.user = backend.ValidUser(
                    server_url=request.session.get(backend.SESSION_SERVER_KEY, None),
                    username=request.session.get(backend.SESSION_USERNAME_KEY, None),
                    api_key=request.session.get(backend.SESSION_APIKEY_KEY, None),
            )
        else:
            request.user = backend.AnonymousUser()
        # Check for forwarded for header
        ip = f"{request.META['REMOTE_ADDR']}"
        if any(["forward" in i.lower() for i in request.META]):
            for i in request.META:
                if "forward" in i.lower():
                    ip = f"{request.META['REMOTE_ADDR']} f{request.META[i]}"
        # Log request
        requests_logger.info(
            f"{ip} {getattr(request.user, 'username', '-')} {request.method} {request.get_full_path()}"
        )
