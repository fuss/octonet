from django.core import exceptions
from django.core.management.base import BaseCommand, CommandError
from django.utils.crypto import get_random_string
import os, sys

PLACEHOLDER = "{GENERATED_SECRET}"

def get_lines_to_changes(pathname):
    import ast

    with open(pathname, "rt") as fd:
        code = compile(fd.read(), pathname, "exec", ast.PyCF_ONLY_AST)

    replace_lines = set()
    for node in ast.walk(code):
        if not isinstance(node, ast.Str): continue
        if node.s != PLACEHOLDER: continue
        replace_lines.add(node.lineno)
    return replace_lines


class Command(BaseCommand):
    help = 'Update the secret key in the default config.'

    def add_arguments(self, parser):
        parser.add_argument('pathname',
            help='Configuration file to edit.')

    def handle(self, *args, **options):
        configfile = options["pathname"]

        chars = 'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)'
        secret_key = get_random_string(50, chars)

        if not os.path.exists(configfile):
            print("{} does not exist".format(configfile), file=sys.stderr)
            sys.exit(1)

        replace_lines = get_lines_to_changes(configfile)
        if not replace_lines: return

        new_content = []
        with open(configfile, "rt") as fd:
            for lineno, line in enumerate(fd, start=1):
                if lineno in replace_lines:
                    line = line.replace(PLACEHOLDER, secret_key)
                new_content.append(line)

        with open(configfile, "wt") as fd:
            for line in new_content:
                if line.startswith("#makesecret:"): continue
                fd.write(line)
