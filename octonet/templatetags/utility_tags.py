# coding: utf-8
import os
from urllib.parse import unquote_plus as urllib_unquote_plus
from urllib.parse import quote_plus as urllib_quote_plus
from django import template


register = template.Library()


# http://stackoverflow.com/a/23783666
@register.filter
def addstr(arg1, arg2):
    return str(arg1) + str(arg2)


@register.filter
def os_path_join(arg1, arg2):
    return os.path.join(arg1, arg2)


@register.filter
def quote_plus(value):
    return urllib_quote_plus(value)


@register.filter
def unquote_plus(value):
    return urllib_unquote_plus(value)
