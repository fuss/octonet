from urllib import parse
from django.utils.translation import gettext_lazy as _
from xmlrpc.client import ServerProxy, ProtocolError
import octofuss

SESSION_SERVER_KEY = '_octonet_server_url'
SESSION_USERNAME_KEY = '_octonet_username'
SESSION_APIKEY_KEY = '_octonet_api_key'


class ValidUser:
    def __init__(self, server_url, username, api_key, tree=None):
        self.server_url = server_url
        self.server_hostname = parse.urlparse(self.server_url).hostname
        self.username = username
        self.api_key = api_key
        self._tree = tree
        if not self._tree:
            self._tree = octofuss.xmlrpc.Client(self.server_url, self.api_key)
        serverinfo = octofuss.Subtree(self._tree, "/serverinfo")
        try:
            domain = serverinfo.get("organization")
            if " " in domain:
                self.domain = domain.split(" ")[0]
            else:
                self.domain = domain
            if self.domain.startswith('"'):
                self.domain = self.domain[1:]
            if self.domain[-1] == '"':
                self.domain = self.domain[:-1]
        except:
            try:
                self.domain = serverinfo.get("organization")
            except:
                self.domain = self.server_hostname

    def is_anonymous(self):
        return False

    def is_authenticated(self):
        return True

    @property
    def tree(self):
        if self._tree is None:
            self._tree = octofuss.xmlrpc.Client(self.server_url, self.api_key)
        return self._tree

    def __str__(self):
        return self.username


class TestUser(ValidUser):
    def __init__(self, tree):
        super().__init__("testserver", "testuser", "testapikey", tree)


class AnonymousUser:
    def __init__(self):
        self.server_url = None
        self.server_hostname = self.server_url
        self.username = None
        self.api_key = None

    def is_anonymous(self):
        return True

    def is_authenticated(self):
        return False


class AuthenticationError(RuntimeError):
    pass


def authenticate(server_url, username, password):
    """
    Authenticate user credentials against the given octofuss server.

    Return the api key that can be used to access the server with these
    credentials, or None if authentication failed.
    """
    loginServer = ServerProxy(server_url)
    try:
        api_key = loginServer.login(username, password)
    except ProtocolError as e:
        raise AuthenticationError(server_url + ": " + e.errmsg)
    except ConnectionRefusedError as e:
        raise AuthenticationError(server_url + ": connection refused")
    if not api_key:
        raise AuthenticationError(_("Invalid credentials"))
    return api_key


def login(request, server_url, username, api_key):
    """
    Persist user and server informations in the request. This way a user
    doesn't have to reauthenticate on every request. Note that data set during
    the anonymous session is retained when the user logs in.
    """
    # TODO: reimplement https://docs.djangoproject.com/en/1.10/topics/auth/default/#session-invalidation-on-password-change
    #       if we want it on octonet
    if SESSION_APIKEY_KEY in request.session:
        if request.session.get(SESSION_APIKEY_KEY) != api_key:
            # To avoid reusing another user's session, create a new, empty
            # session if the existing session corresponds to a different
            # authenticated user.
            request.session.flush()
    else:
        request.session.cycle_key()

    request.session[SESSION_SERVER_KEY] = server_url
    request.session[SESSION_USERNAME_KEY] = username
    request.session[SESSION_APIKEY_KEY] = api_key

    # Changes the CSRF token in use for a request - should be done on login
    # for security purposes.  (from rotate_token's docstring)
    from django.middleware.csrf import rotate_token
    rotate_token(request)


def logout(request):
    """
    Removes the authenticated user's ID from the request and flushes their
    session data.
    """
    # this used to save the language stored in the session, but since
    # Django 2.1 the language is stored in the LANGUAGE_COOKIE_NAME
    # cookie instead.
    # https://docs.djangoproject.com/en/5.0/releases/3.0/#id3

    request.session.flush()

    request.user = AnonymousUser()
