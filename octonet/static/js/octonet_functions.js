// Inject messages via javascript
function inject_message(container_selector, message_level, message_text) {
  $(container_selector).empty().append(
    '<div class="alert alert-dismissable alert-' + message_level + '"><a class="close" data-dismiss="alert" href="#">&times;</a>' + message_text + '</div>'
  );
}
