(function($) {
"use strict";

// See http://stackoverflow.com/questions/8034918/jquery-switch-elements-in-dom
function swapElements(elm1, elm2)
{
    var parent1, next1,
        parent2, next2;

    parent1 = elm1.parentNode;
    next1   = elm1.nextSibling;
    parent2 = elm2.parentNode;
    next2   = elm2.nextSibling;

    parent1.insertBefore(elm2, next1);
    parent2.insertBefore(elm1, next2);
}

/**
 * Convert CSV data to an array of JSON-able elements via a field definitions.
 *
 * The widget is applied to a <table>. The table's <thead> contains a row of
 * headers with the field definition:
 *
 *    <thead>
 *      <tr>
 *        <th data-name="trange" data-type="integer">{% trans "Time Range" %}</th>
 *        <th data-name="p1"     data-type="integer">{% trans "P1" %}</th>
 *      </tr>
 *    </thead>
 *
 * There is a field for each <th> element. data-name is the field name,
 * data-type is the field type, and the content of the <th> element is the
 * field description.
 *
 * The field name is used as key in the resulting JSON.
 *
 * The field type is used to parse the CSV column values and is a name picked
 * from $.truelite.csvpreview.field_types
 */
$.widget("truelite.csvpreview", {
    options: {
        /**
         * If defined, hook into the change() event of this file upload field
         */
        file_field: undefined,

        /// Title for columns not associated to fields
        make_extra_th: function() { return $("<th>"); },

        /// Number of bytes to read from the beginning of the file as a preview
        preview_bytes: 1000000,

        /// Number of CSV rows used in the preview
        preview_rows: 8,

        /// Prefix used for <td> classes in the preview table
        cell_class_prefix: "field-",

        /// Class applied to preview table rows that are ignored from import
        ignored_row_class: "csvpreview-ignored",

        /// Class added to a table header while it is being dragged
        drag_source_class: "dragged",

        /**
         * Column assigned to each field
         *
         * Example:
         *
         *   // Map the 5th field to the 1st column, and the 4rd field to the 3rd column
         *   $("#preview").csvpreview("option", "columns", [5, undefined, 4])
         */
        columns: [],

        /**
         * First row considered for import
         *
         * Example:
         *
         *   // Skip the first 2 rows of data
         *   // (note that row indices are 0-based)
         *   $("#preview").csvpreview("option", "first_row", 2)
         */
        first_row: 0,
    },

    _create: function() {
        var self = this;

        // PapaParse parsing result, see http://papaparse.com/docs#results
        self.parsed = undefined;
        // Number of columns in the parsed CSV
        self.parsed_cols = 0;

        // Connect to file field if requested
        var $file_field = self.options.file_field ? $(self.options.file_field) : undefined;
        if ($file_field)
            $file_field.change(function() {
                self.preview_file(this.files[0]);
            });

        // Cache lookups of significant elements
        self.$thead_tr = self.element.find("thead tr");
        self.$tfoot_tr = self.element.find("tfoot tr");

        // List of fields, in the order they have been found in the initial DOM
        self.fields = [];
        // Same fields as in self.fields, but indexed by name
        self.fields_byname = {};
        self._load_fields();

        self._dd_init();
    },

    /// Load field definition from table headers
    _load_fields: function() {
        var self = this;
        self.$thead_tr.find("th").each(function() {
            var def = {
                // field name
                name: this.getAttribute("data-fieldname"),
                // field data type
                type: this.getAttribute("data-type"),
                // th DOM element for the column header
                th: this,
                // index of this field's allocated column in CSV rows
                // (undefined when not allocated)
                column: undefined,
            };
            if (!def.name)
            {
                console.error("Missing data-name for", this);
                return;
            }
            if (!def.type)
            {
                console.error("Missing data-type for", this);
                return;
            }
            def.parser = $.truelite.csvpreview.field_types[def.type];
            if (!def.parser)
            {
                console.error("Invalid data-type for", this);
                console.error("Please use one of", $.truelite.csvpreview.field_types);
                return;
            }
            self.fields.push(def);
            self.fields_byname[def.name] = def;
        });
    },

    // Set up drag and drop
    _dd_init: function() {
        var self = this;

        // Element currently being dragged
        self.dragged = undefined;
        // Element under the element being dragged
        self.dropped = undefined;

        self.$thead_tr.find("th").attr("draggable", true).on("dragstart", function(evt) {
            // Make the current field name available also during dragover events
            self.dragged = this.getAttribute("data-fieldname");
            self.dropped = undefined;
            evt.originalEvent.dataTransfer.dropEffect = "move";
            evt.originalEvent.dataTransfer.setData("text/plain", self.dragged);

            $(this).addClass(self.options.drag_source_class);

            var field_alloc = {};
            $.each(self.fields, function() { field_alloc[this.column] = this.name; });
            delete field_alloc[self.dragged];
            self._color_tds(field_alloc);
        }).on("dragend", function(evt) {
            evt.preventDefault();
            $(evt.target).removeClass(self.options.drag_source_class);
            if (self.dropped === undefined)
            {
                // Unassign the field
                self.fields_byname[self.dragged].column = undefined;
                self._trigger("changed", null, { state: self.state() });
            }
            self.update_titles();
            self._color_tds();
            self.dragged = undefined;
            self.dropped = undefined;
        });
        self.element.on("dragover", function(evt) {
            evt.preventDefault();
            evt.originalEvent.dataTransfer.effectAllowed = "move";

            var src_field = self.fields_byname[self.dragged];
            var dest_pos = $(evt.target).data("column");
            var field_alloc = {};
            $.each(self.fields, function() {
                if (this == src_field) return;
                field_alloc[this.column] = this.name;
            });
            field_alloc[dest_pos] = src_field.name;
            self._color_tds(field_alloc);
        });
        self.element.on("drop", function(evt) {
            evt.preventDefault();
            var src_field = self.fields_byname[self.dragged];
            self.dropped = $(evt.target).data("column");
            var dst_field = self._get_field_for_column(self.dropped);
            if (dst_field !== undefined)
                dst_field.column = src_field.column;
            src_field.column = self.dropped;
            self._trigger("changed", null, { state: self.state() });
        });
    },

    /// Get the field record for a column position, or undefined if no field is
    /// assigned to that colum
    _get_field_for_column: function(pos) {
        var self = this;
        var found = $.grep(self.fields, function(field) { return field.column == pos; });
        if (found.length == 0) return undefined;
        return found[0];
    },

    // Set up the preview for a new file
    preview_file: function(file) {
        var self = this;

        var head = file.slice(0, self.options.preview_bytes);

        Papa.parse(head, {
            complete: function(results) {
                self.preview_results(results);
            },
            // skipEmptyLines: true,
            preview: self.options.preview_rows,
        });
    },

    _count_undefined_in_column: function(col, field) {
        var self = this;
        var res = 0;
        for (var i = 0; i < self.parsed.data.length; ++i)
            if (field.parser(self.parsed.data[i][col]) === undefined)
                ++res;
        return res;
    },

    _select_best_column: function(startcol, field) {
        var self = this;
        var selected = undefined;
        var undefineds = self.options.preview_rows;
        for (var i = 0; i != self.parsed_cols; ++i)
        {
            var col = (i + startcol) % self.parsed_cols;

            // Skip columns already used
            var exists = $.grep(self.fields, function(field) { return field.column == col; });
            if (exists.length)
                continue;

            var count = self._count_undefined_in_column(col, field);
            if (count < undefineds)
            {
                selected = col;
                undefineds = count;
            }
        }
        field.column = selected;
        return selected;
    },

    // Assign all unassigned fields, guessing where they fit best
    _autodetect_fields: function() {
        var self = this;
        var last_selected = 0;
        $.each(self.fields, function(idx, field) {
            last_selected = self._select_best_column(last_selected === undefined ? 0 : last_selected, field);
        });
    },

    // Set up the preview for parsed CSV data
    preview_results: function(results) {
        var self = this;
        self.parsed = results;

        // Compute the number of columns in the parsed CSV
        self.parsed_cols = 0;
        $.each(self.parsed.data, function(ri, row) {
            if (row.length > self.parsed_cols)
                self.parsed_cols = row.length;
        });

        // Autodetect initial field assignments
        self._autodetect_fields();

        self.update_titles();
        self._update_tbody_data();
        self._color_tds();

        self._trigger("have_preview", null, { data: results });
        self._trigger("changed", null, { state: self.state() });
    },

    /// Regenerate the <th> elements inside the title row, reusing the existing
    /// th elements and adding extra empty ones for unallocated columns
    update_titles: function(field_alloc) {
        var self = this;

        self.options.columns = []

        // Default to using the currently allocated field positions
        if (field_alloc === undefined)
        {
            field_alloc = {};
            $.each(self.fields, function(idx, field) {
                field_alloc[field.column] = field.name;
                self.options.columns.push(field.column);
            });
        }

        // Update the header with extra fields to accomodate all CSV columns
        self.$thead_tr.find("th.extra").remove();

        // Remove the header fields to be reinserted later at the right place
        self.$thead_tr.find("th").detach();
        self.$tfoot_tr.find("th").detach();

        // Recreate the sequence of th objects for the header
        var headers = [];
        for (var i = 0; i < self.parsed_cols; ++i)
        {
            var $th;
            var name = field_alloc[i];
            if (name === undefined) {
                $th = self.options.make_extra_th();
                $th.addClass("extra");
            } else {
                $th = $(self.fields_byname[name].th);
            }
            $th.data("column", i);
            headers.push($th);
        }
        self.$thead_tr.append(headers);

        // Add all unassigned fields to the tfoot
        $.each(self.fields, function(idx, field) {
            if (field.column !== undefined) return;
            self.$tfoot_tr.append(field.th);
        });
    },

    /// Populate the <tbody> elements with data cells with the CSV values
    _update_tbody_data: function() {
        var self = this;
        var $tbody = self.element.find("tbody").empty();
        for (var ri = 0; ri < self.options.preview_rows; ++ri)
        {
            if (ri > self.parsed.data.length) break;
            var row = self.parsed.data[ri];
            var $tr = $("<tr>").data("index", ri);
            $.each(row, function(ci, col) {
                $tr.append($("<td>").text(col).data("column", ci));
            });
            $tr.click(function(evt) { self._toggle_first_row(evt, this); });
            $tbody.append($tr);
        }
    },

    _toggle_first_row: function(evt, tr) {
        var self = this;
        var $tr = $(tr);
        var ri = $tr.data("index");
        if (ri == self.options.first_row - 1)
        {
            --self.options.first_row;
            $tr.removeClass(self.options.ignored_row_class);
        } else if (ri == self.options.first_row) {
            ++self.options.first_row;
            $tr.addClass(self.options.ignored_row_class);
        }
        self._trigger("changed", null, { state: self.state() });
    },

    /// Update attributes of tbody column according to field assignments
    _color_tds: function(field_alloc) {
        var self = this;

        // Default to using the currently allocated field positions
        if (field_alloc === undefined)
        {
            field_alloc = {};
            $.each(self.fields, function() { field_alloc[this.column] = this.name; });
        }

        var $tbody = self.element.find("tbody");

        var classes = $.map(self.fields, function(field) { return "field-" + field.name }).join(" ");
        $tbody.find("td").removeClass(classes);

        $tbody.find("tr").each(function() {
            var $tr = $(this);
            $.each(field_alloc, function(idx, name) {
                $($tr.find("td")[idx]).addClass("field-" + name);
            });
        });
    },

    state: function() {
        var self = this;
        return {
            fields: self.fields_byname,
            first_row: self.options.first_row,
            csv_preview: self.parsed
        }
    },

    _setOption: function(key, value) {
        var self = this;

        self._super(key, value);

        if (key === "columns")
        {
            for (var i = 0; i < self.fields.length; ++i)
                self.fields[i].column = value[i]

            self.update_titles();
            self._update_tbody_data();
            self._color_tds();
            self._trigger("changed", null, { state: self.state() });
        } else if (key === "first_row") {
            var $tbody = self.element.find("tbody");

            $tbody.find("tr").each(function(idx, tr) {
                var $tr = $(tr);
                if (idx < self.options.first_row)
                    $tr.addClass(self.options.ignored_row_class);
                else
                    $tr.removeClass(self.options.ignored_row_class);
            });

            self._trigger("changed", null, { state: self.state() });
        }
    },

    /*
    generate: function(consumer) {
        var self = this;
        $.each(self.parsed.data, function(idx, row) {
            if (idx < self.options.first_row) return;
            var record = {};
            $.each(self.fields_byname, function(name, field) {
                record[name] = field.parser(row[field.column]);
            });
            consumer(record);
        });
    },
    */
});


/**
 * Parser functions for the various column types.
 *
 * They return undefined if the value was not parsable.
 *
 * They return a JSON-able value of the parsing was successful.
 */
$.truelite.csvpreview.field_types = {
    string: function(str) {
        return str;
    },
    integer: function(str) {
        if (!/^\d+$/.test(str)) return;
        var res = parseInt(str);
        if (!isNaN(res)) return res;
    },
};

})(jQuery);
