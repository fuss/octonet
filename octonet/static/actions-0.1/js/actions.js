(function($) {
"use strict";

/// Common implementation for all action widgets
$.widget("truelite.action", {
    options: {
        /// The action description record
        action: undefined,
        /// URL where data is posted when the action is activated
        url: undefined,
        action_completion_url: undefined,
    },

    _create: function() {
        var self = this;
    },

    /// Compute the POST data to be sent when the action is activated
    _get_post_data: function() {
        var self = this;
        return {};
    },

    /// Called on action activation, by default posts to self.options.url
    _activate: function() {
        var self = this;
        var post_data = self._get_post_data();
        $.ajax({
          url: self.options.url,
          method: "POST",
          data: post_data,
          dataType: "json",
          success: function(data, textStatus, jqXHR) {
              console.log(self.options.action, "POST success", data);
              // refs #176 do not show the message. Maybe it's possible to show it, and after
              // some delay reload the page...
              // inject_message("#id_messages", data.message_class, data.message);
              self._trigger("success", null, { post_data: post_data });
              // refs #176 reload the page after successful action
              // refs #174 get the redirect url from the response, defaulting to window.location
              var redirect_url = data.redirect_url || window.location;
              $(location).attr('href', redirect_url);
          },
          error: function(jqXHR, textStatus, errorThrown) {
              console.log(jqXHR, textStatus, errorThrown);
              var data = jqXHR.responseJSON;
              console.log(self.options.action, "POST fail", data);
              inject_message("#id_messages", data.message_class, data.message);
              self._trigger("failure", null, { post_data: post_data });
          },
        });
    },
});

/// Nudge action: send the server an event with no data
$.widget("truelite.action_nudge", $.truelite.action, {
    _create: function() {
        var self = this;
        self._super();
        self.button = $('<span class="btn btn-primary pull-right">').text(self.options.action.label);
        self.button.click(function() {
          if(self.button.attr("disabled") !== "disabled")
            self._activate();
        });
        self.element.append(self.button);
    },
    _get_post_data: function() {
        var self = this;
        var res = self._super();
        res.value = self.options.action.value;
        return res;
    },

});

/// Freeform action: send the server an event with data from a text input
$.widget("truelite.action_freeform", $.truelite.action, {
    _create: function() {
        var self = this;
        self._super();

        // refs #153
        var id_for_input = "id_" + self.options.action.action;
        var id_for_button = id_for_input + "_button";
        self.form = $('<form class="form-inline"></form>');
        self.label = $('<label>' + self.options.action.text + ':&nbsp;</label>');
        self.input = $('<input id="' + id_for_input + '" type="text" class="form-control freeform_actions"/>');
        self.button = $('<span id="' + id_for_button + '" class="btn btn-primary pull-right">').text(self.options.action.label);
        self.button.click(function() {
          if(self.button.attr("disabled") !== "disabled")
            self._activate();
        });
        // refs #176 Submit form on 'enter' key in the input field
        self.input.keypress(function(e) {
          if(e.which == 13) {
            self.button.trigger("click");
            return false;
          }
        });

        self.form.append(self.label);
        self.form.append(self.input);
        self.form.append(self.button);
        self.element.append(self.form);
    },

    _get_post_data: function() {
        var self = this;
        var res = self._super();
        res.value = self.input.val();
        return res;
    },
});

/// Choice action: send the server an event with data from a <select> field
$.widget("truelite.action_choice", $.truelite.action, {
    _create: function() {
        var self = this;
        self._super();

        // refs #153 #157
        var id_for_select = "id_" + self.options.action.action;
        var id_for_button = id_for_select + "_button";
        // Add a select and a button
        self.form = $('<form class="form-inline"></form>');

        self.label = $('<label>' + self.options.action.label + ':&nbsp;</label>');

        self.select = $('<select id="' + id_for_select + '" class="form-control select_actions"></select>');
        $.each(self.options.action.options, function(idx, option) {
            self.select.append($('<option>').attr("value", option).text(decodeURIComponent(option).replace(/\+/g, " ")));
        });
        self.button = $('<span id="' + id_for_button + '" class="btn btn-primary pull-right">').text(self.options.action.label);
        self.button.click(function() {
          if(self.button.attr("disabled") !== "disabled")
            self._activate();
        });
        self.form.append(self.label);
        self.form.append(self.select);
        self.form.append(self.button);
        self.element.append(self.form);
    },

    _get_post_data: function() {
        var self = this;
        var res = self._super();
        res.value = self.select.val();
        return res;
    },
});

/// Completion action: send the server an event with data from a <select> field
$.widget("truelite.action_completion", $.truelite.action, {
    _create: function() {
        var self = this;
        self._super();

        // refs #153 #157
        var id_for_select = "id_" + self.options.action.action;
        var id_for_button = id_for_select + "_button";
        // Add a select and a button
        self.form = $('<form class="form-inline"></form>');

        self.label = $('<label>' + self.options.action.label + ':&nbsp;</label>');

        self.select = $('<select id="' + id_for_select + '" class="form-control select_actions"></select>');
        // Make the AJAX call to get options from /candidate
        $.ajax({
          url: self.options.action_completion_url,
          method: "GET",
          data: {
            'base_path': self.options.action.base_path,
            'action': self.options.action.action,
            'path': self.options.action.path,
          },
          dataType: "json",
          success: function(data, textStatus, jqXHR) {
              console.log("action_completion_ajax SUCCESS", data, textStatus, jqXHR);
              if(data.status === "ok") {
                  self.select.append($('<option>').attr("value", null).text("-----"));
                  $.each(data.options, function(idx, option) {
                      self.select.append($('<option>').attr("value", option).text(decodeURIComponent(option).replace(/\+/g, " ")));
                  });
              }
          },
          error: function(jqXHR, textStatus, errorThrown) {
              console.log("action_completion_ajax ERROR", jqXHR, textStatus, errorThrown);
              var data = jqXHR.responseJSON;
          },
        });

        self.button = $('<span id="' + id_for_button + '" class="btn btn-primary pull-right">').text(self.options.action.label);
        self.button.click(function() {
          if(self.button.attr("disabled") !== "disabled")
            self._activate();
        });
        self.form.append(self.label);
        self.form.append(self.select);
        self.form.append(self.button);
        self.element.append(self.form);
    },

    _get_post_data: function() {
        var self = this;
        var res = self._super();
        res.value = self.select.val();
        return res;
    },
});


/// Action panel, updated dynamically as directed server-side
$.widget("truelite.actions", {
    options: {
        url: undefined,
    },

    _create: function() {
      var self = this;
      //console.log("_create()", self.options.string);
      //console.log("self.url", self.options.url);
      if (self.options.url) {
          self.refresh();
      }
      //console.log("action_url:", self.options.action_url);
    },

    refresh: function() {
        var self = this;
        $.ajax({
          url: self.options.url,
          data: {},
          dataType: "json",
          success: function(data) {
            console.log("have action list", data);
            self.render(data.actions);
          },
        });
    },

    render: function(actions) {
        var self = this;
        self.element.empty();
        $.each(actions, function(idx, action) {
            // refs #14432
            // https://stackoverflow.com/a/481611
            // "Temporarily" disable lock and unlock desktop in the actions widget by hiding the buttons
            if (action.action === "lockdesktop" || action.action === "unlockdesktop")
                return true;
            var row_id = "id_" + action.action;
            var selector = "#" + row_id;
            var el = ($('<div>').addClass("row")).append($('<div id="' + row_id + '"></div>').addClass("col-md-12"));
            var action_url = self.options.action_url.replace("_action", action.action);
            var action_completion_url = self.options.action_completion_url;
            var setup = el["action_" + action.type];
            if (!setup)
                console.error("Unsupported action type", action)
            else
                setup.call(el, {
                    action: action,
                    url: action_url,
                    action_completion_url: action_completion_url,
                    success: function() {
                        self.refresh();
                    },
                });
            self.element.append(el);
        });
        $("select").select2({'width': '50%'});

        // refs #153
        var freeform_actions = $(".freeform_actions");
        $.each(freeform_actions, function(idx, el) {
          var input = $(el);
          var button = $("#" + input.attr('id') + "_button");
          input.on("input", function() {
            if(input.val())
              button.removeAttr("disabled");
            else
              button.attr("disabled", "disabled");
          });
          // Trigger input to make it work also on load
          input.trigger("input");
        });

        // refs #153 #157
        var select_actions = $(".select_actions");
        $.each(select_actions, function(idx, el) {
          var select = $(el);
          var button = $("#" + select.attr('id') + "_button");
          select.on("change", function() {
            var value = select.val();
            if((value !== "-----") && (value !== "") && (value !== null) && (value !== undefined) && (value !== "None"))
              button.removeAttr("disabled");
            else
              button.attr("disabled", "disabled");
          });
          // Trigger change to make it work also on load
          select.trigger("change");
        });
    },
});

})(jQuery);
