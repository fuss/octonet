# coding: utf-8
from collections import OrderedDict


# http://stackoverflow.com/a/27493844
def reorder_fields(fields, order):
    """Reorder form fields by order, removing items not in order.

    >>> reorder_fields(
    ...     OrderedDict([('a', 1), ('b', 2), ('c', 3)]),
    ...     ['b', 'c', 'a'])
    OrderedDict([('b', 2), ('c', 3), ('a', 1)])
    """
    # https://stackoverflow.com/a/52549728
    old_fields = fields.copy()
    # Iterate through old_fields, but remove fields from fields, which is the OrderedDict that
    # gets returned, so to avoid 'RuntimeError: OrderedDict mutated during iteration'
    for key, v in old_fields.items():
        if key not in order:
            del fields[key]
    return OrderedDict(sorted(fields.items(), key=lambda k: order.index(k[0])))
