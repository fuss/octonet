octonet (13.0.0-1) unstable; urgency=medium

  * Compatibility with Django 4.2

 -- Elena Grandi <valhalla@debian.org>  Tue, 07 May 2024 14:28:04 +0200

octonet (12.0.3-1) bookworm; urgency=medium

  * Version bump

 -- Paolo Dongilli <dongilli@fuss.bz.it>  Fri, 08 Mar 2024 09:18:05 +0100

octonet (12.0.2-1) bookworm; urgency=medium

  * Added button to empty a cluster
  * Added button to empty a printer domain

 -- Marco Marinello <mmarinello@fuss.bz.it>  Wed, 06 Mar 2024 18:46:27 +0100

octonet (12.0.1-1) bookworm; urgency=medium

  * Fix ordering of fields in the DHCP leases with modern django.

 -- Elena Grandi <valhalla@debian.org>  Tue, 17 Jan 2023 12:12:39 +0100

octonet (10.0.10-1) buster; urgency=medium

  * Use load static instead of load staticfiles (deprecated in django 2.1).

 -- Elena Grandi <elena@truelite.it>  Wed, 04 Jan 2023 12:07:20 +0100

octonet (10.0.9-1) buster; urgency=medium

  * Remove the use of six: python2 has not been supported for a while.

 -- Elena Grandi <elena@truelite.it>  Tue, 03 Jan 2023 13:07:42 +0100

octonet (10.0.8-1) buster; urgency=medium

  * Added support for Veyon auto-configuration

 -- Marco Marinello <mmarinello@fuss.bz.it>  Wed, 16 Mar 2022 23:21:19 +0100

octonet (10.0.7-1) buster; urgency=medium

  * Display number of successfull and unsuccessfull hosts in a run

 -- Marco Marinello <mmarinello@fuss.bz.it>  Wed, 09 Feb 2022 11:59:07 +0100

octonet (10.0.6-1) buster; urgency=critical

  * Configure logging per standard
  * Restart octonet service when upgraded

 -- Marco Marinello <mmarinello@fuss.bz.it>  Tue, 18 Jan 2022 15:12:05 +0100

octonet (10.0.5-2) buster; urgency=medium

  * Rebuild for buster

 -- Elena Grandi <elena@truelite.it>  Tue, 31 Aug 2021 11:50:48 +0200

octonet (10.0.5-1) buster; urgency=medium

  * Support for django 2.

 -- Elena Grandi <elena@truelite.it>  Fri, 27 Aug 2021 13:17:00 +0200

octonet (10.0.4-1) buster; urgency=medium

  * Upload to buster

 -- Elena Grandi <elena@truelite.it>  Thu, 27 Aug 2020 12:14:09 +0200

octonet (10.0.3-1) unstable; urgency=medium

  * Readding '-' to allowed chars in group names

 -- Simone Piccardi <piccardi@truelite.it>  Tue, 25 Aug 2020 10:39:20 +0200

octonet (10.0.2-1) testing; urgency=medium

  * Changed the interface for the SAMBA share create and edit pages.
  * When creating, the name is editable, and the path is forced to be
    /home/SAMBA/<name_of_the_share>, filled automatically
  * When editing, the name *and* the path are not editable
  * Both pages will pass down the data to octofussd, which will call
    the needed script
  * Italian translation of the new strings

 -- Mark Caglienzi <mark@truelite.it>  Tue, 24 Mar 2020 14:35:08 +0100

octonet (10.0.1-1) testing; urgency=medium

  * Add "last password change" and "next password change" data in the
    user detail page
  * Add a column in the user list page showing a green key for users
    that have a non-expired password and a red key for users that have
    an expired password
  * If the performances of the user list page becomes too sluggish after
    this change, the modification is easily removed by reverting the unique
    commit that introduces it
  * Fix a bug in the create user page: now Octonet does not crash anymore
    there

 -- Mark Caglienzi <mark@truelite.it>  Mon, 23 Mar 2020 15:52:36 +0100

octonet (10.0.0-1) testing; urgency=medium

  * Version bump to 10.x.x
  * The 'Force password change' button is removed and the function is
    done via a boolean field in the form.

 -- Mark Caglienzi <mark@truelite.it>  Thu, 12 Mar 2020 16:27:42 +0100

octonet (0.2.62-1) testing; urgency=medium

  * Temporarily hide the lock and unlock desktop buttons in the cluster
    and host edit pages.

 -- Mark Caglienzi <mark@truelite.it>  Tue, 10 Mar 2020 12:58:53 +0100

octonet (0.2.61-1) testing; urgency=medium

  * Run a script to propagate network permissions

 -- Mark Caglienzi <mark@truelite.it>  Tue, 03 Mar 2020 16:29:22 +0100

octonet (0.2.60-1) testing; urgency=medium

  * Make the home field follow the changes in the username field when
    creating or editing a user, also in the form when there is some
    error(s) during POST.

 -- Mark Caglienzi <mark@truelite.it>  Tue, 25 Feb 2020 16:24:42 +0100

octonet (0.2.59-1) testing; urgency=medium

  * Bugfix: If the user chooses a password containing non-ascii chars
    octonet will show an error instead of giving a HTTP 500 error.

 -- Mark Caglienzi <mark@truelite.it>  Mon, 24 Feb 2020 16:52:48 +0100

octonet (0.2.58-1) testing; urgency=medium

  [ Enrico Zini ]
  * Open fuss manager in a new window.

 -- Elena Grandi <elena@truelite.it>  Thu, 16 Jan 2020 10:37:41 +0100

octonet (0.2.57-1) testing; urgency=medium

  * Disallow passwords that contain non-ASCII characters when creating or
    editing users, because non-ASCII passwords cause problems to http
    basic auth, that is used (for example) by squid.

 -- Mark Caglienzi <mark@truelite.it>  Wed, 11 Dec 2019 12:09:28 +0100

octonet (0.2.56-1) testing; urgency=medium

  * Fix logging configuration for buster.
  * Revert generation of FUSS_MANAGER_SECRET_KEY when installing

 -- Elena Grandi <elena@truelite.it>  Wed, 04 Dec 2019 11:26:23 +0100

octonet (0.2.55-1) testing; urgency=medium

  * Generate FUSS_MANAGER_SECRET_KEY when installing

 -- Elena Grandi <elena@truelite.it>  Fri, 22 Nov 2019 13:06:18 +0100

octonet (0.2.54-1) testing; urgency=medium

  [ Mark Caglienzi ]
  * Disallow uppercase characters in the name when creating Samba shares.
  * Allow only lowercase characters, numbers, dot, underscore for usernames
    (this for coherence with the checks done when importing users from a CSV
    file).
  * Allow only lowercase characters and numbers for group names.
  * Fix the strings in two buttons in the group edit page . refs #905
  * Fix the members field in the group edit page, making it automatically
    page width aware. refs #905
  * Avoid HTTP Error 500 when editing a user's authorization(s).

  [ Elena Grandi ]
  * Provide a (commented out) configuration to setup logging.

  [ Marco Marinello ]
  * Avoid using relative path for locales

 -- Elena Grandi <elena@truelite.it>  Fri, 22 Nov 2019 10:18:54 +0100

octonet (0.2.53-1) testing; urgency=medium

  * Do not fail whole import on already existing users. refs #892

 -- Marco Marinello <me@marcomarinello.it>  Sun, 15 Sep 2019 11:46:39 +0200

octonet (0.2.52-1) unstable; urgency=medium

  * Update packaging for Buster
  * Compatibility with django 1.11

 -- Elena Grandi <elena@truelite.it>  Mon, 19 Aug 2019 11:40:02 +0200

octonet (0.2.51-1) unstable; urgency=medium

  * Set default port for fuss-manager to be 1232. refs: #826

 -- Elena Grandi <elena@truelite.it>  Tue, 18 Jun 2019 12:09:56 +0200

octonet (0.2.50-1) unstable; urgency=medium

  * Embed fuss-manager if it is available. refs: #789

 -- Elena Grandi <elena@truelite.it>  Wed, 15 May 2019 10:42:14 +0200

octonet (0.2.49-1) unstable; urgency=medium

  * Now there are error checks for the home prefix in the users mass creation
    page: only a-z A-Z 0-9 - _ / are allowed (no ".")
  * The italian translation is updated

 -- Mark Caglienzi <mark@truelite.it>  Thu, 21 Mar 2019 10:37:33 +0100

octonet (0.2.48-1) unstable; urgency=medium

  * Allow to start and stop the access points in a school (a very specific
    model of access point)
  * For this function to work, the fuss-server package version must be >= 8.0.40,
    because it contains the needed scripts (the configuration file must be created
    in a proper manner, though).
  * There are 2 buttons in the Network/Services page, one to start and one to
    stop them.
  * This function needs the script and the configuration file containing the
    right password to work properly.
  * There is a 60 seconds timeout, after that the process that launched the
    script will be killed, and the underlying job can be incomplete.
  * There are messages: when the script finishes with success, when the
    timeout occurs, when there are missing files (and in this case the buttons
    won't do anything)

 -- Mark Caglienzi <mark@truelite.it>  Wed, 13 Mar 2019 12:27:39 +0100

octonet (0.2.47-1) unstable; urgency=medium

  * Allow to specify a home prefix when mass creating users

 -- Mark Caglienzi <mark@truelite.it>  Mon, 11 Mar 2019 12:42:12 +0100

octonet (0.2.46-1) unstable; urgency=medium

  * Now it's possible to force the password change on next login for users via Octonet.
  * There is a button in the single user edit page.
  * There is a button in the users mass edit page.
  * Both buttons have an alert asking confirmation for the action.
  * The action is setting shadowLastChange = 0 in the LDAP entry for the
    user(s).
  * The action gives then a message to the user

 -- Mark Caglienzi <mark@truelite.it>  Fri, 01 Mar 2019 12:16:23 +0100

octonet (0.2.45-1) unstable; urgency=medium

  * Fix the full name field in the user edit page: if it contains non-ascii
    values, it will be shown as intended, and it will be saved correctly.

 -- Mark Caglienzi <mark@truelite.it>  Thu, 21 Feb 2019 14:34:38 +0100

octonet (0.2.44-1) unstable; urgency=medium

  * Fix the output in the user detail, mass edit, group edit pages: if the
    full name of the user contains non-ascii values, it will be shown as
    intended.

 -- Mark Caglienzi <mark@truelite.it>  Fri, 01 Feb 2019 11:59:43 +0000

octonet (0.2.43-1) unstable; urgency=medium

  [ Simone Piccardi ]
  * Modify the regex that checks for username correctness to disallow capital
    letters and allow the dot (".") character.

  [ Mark Caglienzi ]
  * Fix an off-by-one error in the CSV import check: test the usernames when
    they are in the last column of the CSV.
  * Fix the italian translation of a string that was wrong.
    "There are not valid usernames" is "Ci sono nomi utente non validi"
    instead of "Non ci sono nomi utente validi".

 -- Mark Caglienzi <mark@truelite.it>  Mon, 28 Jan 2019 14:55:17 +0100

octonet (0.2.42-1) testing; urgency=medium

  * Port services interface as in octofuss-gtk was

 -- Marco Marinello <mmarinello@fuss.bz.it>  Fri, 09 Nov 2018 23:13:56 +0100

octonet (0.2.41-1) testing; urgency=medium

  * Allow removing all LDAP users belonging to the same group
    (from group edit view)
  * Show domain name in the navbar
  * Correct strings & various errors

 -- Marco Marinello <mmarinello@fuss.bz.it>  Sat, 20 Oct 2018 15:27:42 +0200

octonet (0.2.40-1) unstable; urgency=medium

  * Solve typo on quota editing

 -- Simone Piccardi <piccardi@truelite.it>  Fri, 07 Sep 2018 16:52:30 +0200

octonet (0.2.39-1) unstable; urgency=medium

  * Sort alphabetically the clusters in the 'Managed Hosts' navbar menu, keeping
    'All computers' and 'New computers' on top. The sorting is case insensitive.
    refs #455

 -- Mark Caglienzi <mark@truelite.it>  Wed, 01 Aug 2018 14:50:35 +0000

octonet (0.2.38-1) unstable; urgency=medium

  * Allow a non-root user with 'Managed Hosts' permission to see also clusters
    in Octonet. refs #564

 -- Mark Caglienzi <mark@truelite.it>  Wed, 01 Aug 2018 13:59:04 +0000

octonet (0.2.37-1) unstable; urgency=medium

  * Bugfix: Set shadowLastChange to 0 for 'docenti' also when mass importing
    users via CSV file. refs #352

 -- Mark Caglienzi <mark@truelite.it>  Fri, 27 Jul 2018 14:18:50 +0000

octonet (0.2.36-1) unstable; urgency=medium

  * Add 'Cleanup printers' new feature, to remove non-existent printers from hosts. refs #566

 -- Mark Caglienzi <mark@truelite.it>  Fri, 27 Jul 2018 13:07:49 +0000

octonet (0.2.35-1) unstable; urgency=medium

  * Add a button to delete users in the mass edit page. refs #571

 -- Mark Caglienzi <mark@truelite.it>  Wed, 25 Jul 2018 08:03:46 +0000

octonet (0.2.34-1) unstable; urgency=medium

  * Set shadowLastChange to 0 when creating a 'docenti' user. refs #12214

 -- Mark Caglienzi <mark@truelite.it>  Wed, 18 Jul 2018 07:31:33 +0000

octonet (0.2.33-1) unstable; urgency=medium

  * Fixed loading for subpages for dansguardian

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Tue, 28 Nov 2017 19:10:16 +0100

octonet (0.2.32-1) unstable; urgency=medium

  * refs #403 - fixed app authorization process

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Mon, 11 Sep 2017 18:25:40 +0200

octonet (0.2.31-1) unstable; urgency=medium

  * refs #408 - allow fields with empty description.

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Mon, 11 Sep 2017 16:27:47 +0200

octonet (0.2.30-1) unstable; urgency=medium

  *  refs #10147 - primary group in user creation

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Wed, 30 Aug 2017 12:06:30 +0200

octonet (0.2.29-1) unstable; urgency=medium

  * refs #10018 - delete of groups, if empty

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Wed, 23 Aug 2017 14:54:19 +0200

octonet (0.2.28-1) unstable; urgency=medium

  * refs #377 - confirm modal dialog text & translations

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Fri, 11 Aug 2017 17:08:04 +0200

octonet (0.2.27-1) unstable; urgency=medium

  * UI cleanup and rearrangement

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Wed, 09 Aug 2017 18:14:15 +0200

octonet (0.2.26-1) unstable; urgency=medium

  * refs #353 - added routine to check CSV files before import

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Mon, 07 Aug 2017 15:35:44 +0200

octonet (0.2.25-1) unstable; urgency=medium

  * version bump for repository misalignment

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Fri, 14 Jul 2017 14:28:20 +0200

octonet (0.2.24-1) unstable; urgency=medium

  * refs #244 - UI for dhcp static assignments

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Fri, 14 Jul 2017 14:13:40 +0200

octonet (0.2.23-1) unstable; urgency=medium

  * refs #286 - default permissions assignments

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Tue, 11 Jul 2017 14:06:44 +0200

octonet (0.2.22-1) unstable; urgency=medium

  * refs #342

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Wed, 05 Jul 2017 15:38:05 +0200

octonet (0.2.21-1) unstable; urgency=medium

  * refs #341

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Wed, 05 Jul 2017 10:57:26 +0200

octonet (0.2.20-1) unstable; urgency=medium

  * fixes on LDIF2CSV converter

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Sun, 02 Jul 2017 12:46:13 +0200

octonet (0.2.19-1) unstable; urgency=medium

  * New tool to convert LDIF files to CSV ready to import

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Fri, 30 Jun 2017 22:32:43 +0200

octonet (0.2.18-1) unstable; urgency=medium

  * Fixed links to CUPS in order to configure server's queue from a client workstation

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Fri, 30 Jun 2017 17:16:02 +0200

octonet (0.2.17-1) unstable; urgency=medium

  * refs #333 - supporto for secondary groups import through CSV.

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Thu, 29 Jun 2017 16:14:20 +0200

octonet (0.2.16-1) unstable; urgency=medium

  * refs #330

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Wed, 28 Jun 2017 18:55:33 +0200

octonet (0.2.15-1) unstable; urgency=medium

  * push nudge action value

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Tue, 27 Jun 2017 15:11:20 +0200

octonet (0.2.14-1) unstable; urgency=medium

  * repository update release

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Tue, 20 Jun 2017 20:46:46 +0200

octonet (0.2.13-1) unstable; urgency=medium

  * refs #213 added gevent to gunicorn deploy

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Tue, 20 Jun 2017 20:46:46 +0200

octonet (0.2.12-1) unstable; urgency=medium

  * refs #313

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Mon, 19 Jun 2017 09:37:03 +0200

octonet (0.2.11-1) unstable; urgency=medium

  * refs #288

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Fri, 16 Jun 2017 15:21:44 +0200

octonet (0.2.10-1) unstable; urgency=medium

  * refs #310

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Tue, 13 Jun 2017 13:53:53 +0200

octonet (0.2.9-1) unstable; urgency=medium

  * refs #307, #308

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Tue, 13 Jun 2017 11:20:44 +0200

octonet (0.2.8-1) unstable; urgency=medium

  * refs #250 - loading fixes

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Mon, 12 Jun 2017 13:08:44 +0200

octonet (0.2.7-1) unstable; urgency=medium

  * Various update fixes, including #287, #231 and other minor UI tweaks

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Fri, 09 Jun 2017 17:42:10 +0200

octonet (0.2.6-1) unstable; urgency=medium

  * Fixes for #249, #255

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Fri, 12 May 2017 16:52:30 +0200

octonet (0.2.5-1) unstable; urgency=medium

  * refs #231 - new csv importer via ajax

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Wed, 10 May 2017 16:41:08 +0200

octonet (0.2.4-1) unstable; urgency=medium

  [Elena Grandi]
  * Fixes #192 #232 #174 #176
  * Added versioned dependency on python3-octofuss

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Fri, 28 Apr 2017 10:24:26 +0200

octonet (0.2.3-1) unstable; urgency=medium

  * Fixes #218 #230 #231

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Wed, 26 Apr 2017 12:21:25 +0200

octonet (0.2.2-1) unstable; urgency=medium

  * Fixes #212, #213

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Tue, 28 Mar 2017 15:02:14 +0200

octonet (0.2.1-1) unstable; urgency=medium

  * More text and fixes
  * i18n support in datatables

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Mon, 30 Dec 2016 11:34:56 +0100

octonet (0.1.4-1) UNRELEASED; urgency=medium

  * Problem with unmerged conflicts

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Mon, 20 Dec 2016 16:20:21 +0100

octonet (0.1.3-1) UNRELEASED; urgency=medium

  * New user, samba and dhcp apps

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Mon, 20 Dec 2016 16:20:21 +0100

octonet (0.1.2-2) UNRELEASED; urgency=medium

  * Version bump for repository

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Mon, 19 Dec 2016 17:20:59 +0100

octonet (0.1.2-1) unstable; urgency=medium

  * New updated version for testing distribution

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Tue, 19 Dec 2016 16:56:12 +0100

octonet (0.1.1-1) unstable; urgency=medium

  * Fixed missing python3-whitenoise deps

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Tue, 13 Dec 2016 10:28:45 +0100

octonet (0.1-1) unstable; urgency=low

  * source package automatically created by stdeb 0.8.5

 -- Enrico Zini <enrico@debian.org>  Tue, 06 Dec 2016 09:12:41 +0100
