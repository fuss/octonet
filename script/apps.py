# coding: utf-8
from django.apps import AppConfig
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from octonet.mixins import OctonetAppMixin

class Script(OctonetAppMixin, AppConfig):
    name = 'script'
    verbose_name = _("Script manager")
    description = _("Manage scripts to run on machines")
    group = _("Network")
    main_url = reverse_lazy("script:list")
    font_awesome_class = "fa-magic"
    octofussd_url = "/scripts"
    
