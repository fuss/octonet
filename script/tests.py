# coding: utf-8
from django.urls import reverse
from django.test import SimpleTestCase
import octofuss
from octonet import backend
from octonet.unittest import TestBase


class LoginMixin():
    def setUp(self):
        # Login
        self.client.post(
            reverse("login"),
            data={
                "username": "root",
                "password": "root",
                "server_url": "http://localhost:13400/conf/",
            }
        )


class TestScript(LoginMixin, TestBase, SimpleTestCase):
    """
    General views tests
    """
    def setUp(self):
        self.tree = octofuss.MockTree()
        # # self.tree.lcreate(
            # # ["scripts", "script0", "script"],
            # # "echo test",
        # # )
        # self.tree.lcreate(
            # ["scripts", "script0", "script"],
            # {
                # 'name': 'script0',
                # 'script': "echo test",
            # },
        # )
        # self.tree.lcreate(
            # ["scripts", "script0", "runs"],
            # [
                # {
                    # 'scheduled': None,
                    # 'completed': None,
                    # 'name': 'run0',
                    # 'script': 'script0',
                    # 'hosts': [],
                    # 'notes': '',
                # },
            # ]
        # )
        # print(self.tree.lget(['scripts', 'script0', 'script']))
        # print(self.tree.lget(['scripts', 'script0', 'runs']))
        self.tree.lcreate(
            ["scripts"],
            [
                {
                    # 'scheduled': '2016-12-05 00:00:00',
                    # 'completed': '2016-12-10 00:00:00',
                    'scheduled': None,
                    'completed': None,
                    'name': 'script0',
                    'script': "echo test",
                    'runs': [],
                    # 'runs': [
                        # {
                            # 'name': 'run0',
                            # 'script': 'script0',
                            # 'hosts': [
                                # {
                                    # 'output': None,
                                    # 'started': None,
                                    # 'ended': None,
                                    # 'result': None,
                                    # 'name': 'host288',
                                # },
                            # ],
                        # },
                    # ],
                },
            ],
        )
        super().setUp()
        session = self.client.session
        session['apps'] = ['script']
        session.save()

    def test_GET_list_gives_200(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("script:list"))
            self.assertEqual(response.status_code, 200)

    def test_POST_list_gives_405(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("script:list"))
            self.assertEqual(response.status_code, 405)

    def test_GET_create_gives_200(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("script:create"))
            self.assertEqual(response.status_code, 200)

    def test_POST_create_gives_200(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("script:create"))
            self.assertEqual(response.status_code, 200)

    def test_GET_edit_gives_200(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("script:edit", kwargs={'name': 'script0'}))
            self.assertEqual(response.status_code, 200)

    def test_POST_edit_gives_200(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("script:edit", kwargs={'name': 'script0'}))
            self.assertEqual(response.status_code, 200)

    def test_GET_list_runs_gives_200(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("script:list_runs", kwargs={'name': 'script0'}))
            self.assertEqual(response.status_code, 200)

    def test_POST_list_runs_gives_200(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("script:list_runs", kwargs={'name': 'script0'}))
            self.assertEqual(response.status_code, 200)

    def test_GET_edit_run_gives_200(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(
                reverse("script:edit_run", kwargs={'name': 'script0', 'run': 'run0'}),
            )
            self.assertEqual(response.status_code, 200)

    def test_POST_edit_run_gives_200(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(
                reverse("script:edit_run", kwargs={'name': 'script0', 'run': 'run0'}),
            )
            self.assertEqual(response.status_code, 200)

    def test_GET_delete_run_gives_405(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(
                reverse("script:delete_run", kwargs={'name': 'script0', 'run': 'run0'}),
            )
            self.assertEqual(response.status_code, 405)

    def test_POST_delete_run_gives_200(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(
                reverse("script:delete_run", kwargs={'name': 'script0', 'run': 'run0'}),
            )
            self.assertEqual(response.status_code, 200)
