from django.urls import re_path
from . import views


app_name = "script"
urlpatterns = [
    re_path(r"^$", views.List.as_view(), name="list"),
    re_path(r"^create$", views.Create.as_view(), name="create"),
    re_path(r"^edit/(?P<name>[^/]+)$", views.Edit.as_view(), name="edit"),
    re_path(r"^runs/(?P<name>[^/]+)$", views.ListRuns.as_view(), name="list_runs"),
    re_path(r"^runs/(?P<name>[^/]+)/edit/(?P<run>[^/]+)$", views.EditRun.as_view(), name="edit_run"),
    re_path(r"^runs/(?P<name>[^/]+)/edit/(?P<run>[^/]+)/delete$", views.DeleteRun.as_view(), name="delete_run"),
    re_path(r"^runs/(?P<name>[^/]+)/edit/(?P<run>[^/]+)/schedule$", views.Schedule.as_view(), name="schedule_run"),
    re_path(r"^runs/(?P<name>[^/]+)/edit/(?P<run>[^/]+)/remove-host$", views.RemoveHost.as_view(), name="edit_run_remove_host"),
]
