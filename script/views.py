from django.utils.translation import gettext_lazy as _
from django import http, forms
from django.utils import html
from django.shortcuts import redirect
from django.urls import reverse
from django.views.generic import View
from django.views.generic import TemplateView
from django.views.generic.edit import FormView
from django.contrib import messages
from octonet.mixins import OctonetMixin
from octonet.forms import FormControlClassMixin
from urllib.parse import quote_plus, unquote_plus


class List(OctonetMixin, TemplateView):
    template_name = "script/list.html"

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        scripts = self.root_tree.lget(["scripts"])
        scripts.sort(key = lambda x:x["name"])
        for s in scripts:
            if s["runs"]:
                s["completed_percent"] = "{}%".format(round(sum(1 for x in s["runs"] if x["completed"] is not None) / len(s["runs"]) * 100))
            else:
                s["completed_percent"] = "--"
        ctx["scripts"] = scripts
        return ctx


class ScriptMixin(OctonetMixin):
    def load_objects(self):
        super().load_objects()
        self.tree = self.octofuss_tree("/scripts/{}".format(self.kwargs["name"]))
        self.script = self.tree.lget([])

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        ctx["script"] = self.script
        return ctx

    def get_initial(self):
        return self.script

    def get_success_url(self):
        return reverse("script:list")
    

class ScriptEditForm(FormControlClassMixin, forms.Form):
    script = forms.CharField(required=False, label=_("Script code"), widget=forms.Textarea(attrs={"rows": 25, "cols": 80, "wrap": "off"}))
    upload = forms.FileField(required=False, label=_("Upload script"))

    def clean(self):
        script = self.cleaned_data["script"]
        upload = self.cleaned_data["upload"]

        if not script and not upload:
            self.add_error(
                    "script",
                    forms.ValidationError(_("Please type the script or upload a file"), code="script_missing")
            )
            self.add_error(
                    "upload",
                    forms.ValidationError(_("Please type the script or upload a file"), code="script_missing")
            )

        return self.cleaned_data

    def clean_upload(self):
        f = self.cleaned_data["upload"]
        if f is not None and f.size > 1000000:
            raise forms.ValidationError(_("Script file is too big"), code="script_too_big")
        return f


class ScriptCreateForm(ScriptEditForm):
    name = forms.CharField(label=_("Script name"))


class Create(OctonetMixin, FormView):
    template_name = "script/edit.html"
    form_class = ScriptCreateForm

    def get_success_url(self):
        return reverse("script:list")

    def form_valid(self, form):
        script_file = form.cleaned_data["upload"]
        if script_file is None:
            script = form.cleaned_data["script"]
        else:
            script = script_file.read().decode('utf-8')
        path = ["scripts", form.cleaned_data["name"]]
        self.root_tree.lcreate(path)
        self.root_tree.lset(path + ["script"], script)
        return super().form_valid(form)


class Edit(ScriptMixin, FormView):
    template_name = "script/edit.html"
    form_class = ScriptEditForm

    def form_valid(self, form):
        script_file = form.cleaned_data["upload"]
        if script_file is None:
            script = form.cleaned_data["script"]
        else:
            script = script_file.read().decode('utf-8')
        self.tree.lset(["script"], script)
        return super().form_valid(form)


class NewRunForm(FormControlClassMixin, forms.Form):
    name = forms.CharField(label=_("Name"))


class ListRuns(ScriptMixin, FormView):
    template_name = "script/list_runs.html"
    form_class = NewRunForm

    def get_initial(self):
        return {}

    def get_context_data(self, **kw):
        ctx = super().get_context_data()
        for run in ctx["script"]["runs"]:
            finished = [k for k in run["hosts"] if k["ended"]]
            run["finished_hosts"] = len(finished)
            run["errored_hosts"] = len([k for k in finished if k["result"] != 0])
        return ctx


    def get_success_url(self):
        return reverse("script:list_runs", args=[self.script["name"]])
    
    def form_valid(self, form):
        name = quote_plus(form.cleaned_data["name"])
        self.root_tree.lcreate(["scripts", self.script["name"], "runs", name])
        return super().form_valid(form)


class RunMixin(ScriptMixin):
    def load_objects(self):
        super().load_objects()
        self.run = self.tree.lget(["runs", self.kwargs["run"]])

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        ctx["run"] = self.run
        return ctx

    def get_success_url(self):
        return reverse("script:edit_run", args=[self.script["name"], self.run["name"]])


class EditRunForm(FormControlClassMixin, forms.Form):
    add_host = forms.CharField(required=False, label=_("Add host"))
    add_group = forms.CharField(required=False, label=_("Add group"))
    del_group = forms.CharField(required=False, label=_("Remove group"))


class EditRun(RunMixin, FormView):
    template_name = "script/edit_run.html"
    form_class = EditRunForm

    def form_valid(self, form):
        add_host = form.cleaned_data["add_host"]
        if add_host:
            self.root_tree.lcreate(["scripts", self.script["name"], "runs", self.run["name"], "hosts", add_host])

        add_group = form.cleaned_data["add_group"]
        if add_group:
            for host in self.root_tree.llist(["cluster", add_group]):
                if host.startswith(":"): continue
                self.root_tree.lcreate(["scripts", self.script["name"], "runs", self.run["name"], "hosts", host])

        del_group = form.cleaned_data["del_group"]
        if del_group:
            for host in self.root_tree.llist(["cluster", del_group]):
                if host.startswith(":"): continue
                path = ["scripts", self.script["name"], "runs", self.run["name"], "hosts", host]
                if self.root_tree.lhas(path):
                    self.root_tree.ldelete(path)

        return super().form_valid(form)


class RemoveHost(RunMixin, View):
    def post(self, request, *args, **kw):
        host = request.POST.get("host", None)
        if host:
            path = ["scripts", self.script["name"], "runs", self.run["name"], "hosts", host]
            if self.root_tree.lhas(path):
                self.root_tree.ldelete(path)
        return redirect("script:edit_run", name=self.script["name"], run=self.run["name"])
        

class DeleteRun(RunMixin, View):
    def post(self, request, *args, **kw):
        if self.run and (not self.run["scheduled"] or self.run["completed"]):
            path = ["scripts", self.script["name"], "runs", self.run["name"]]
            if self.root_tree.lhas(path):
                self.root_tree.ldelete(path)
        return redirect("script:list_runs", name=self.script["name"])


class Schedule(RunMixin, View):
    def post(self, request, *args, **kw):
        if not self.run["hosts"]:
            messages.error(self.request, _("Cannot schedule a script run without hosts"))
            return redirect("script:edit_run", name=self.script["name"], run=self.run["name"])
        if self.run["scheduled"]:
            messages.error(self.request, _("This run has already been scheduled"))
            return redirect("script:edit_run", name=self.script["name"], run=self.run["name"])

        self.root_tree.lset(["scripts", self.script["name"], "runs", self.run["name"], "scheduled"], True)

        return redirect("script:list_runs", name=self.script["name"])
