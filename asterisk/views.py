from django.utils.translation import gettext_lazy as _
from django import forms
from django.views.generic import View
from django.views.generic.edit import FormView
from django.contrib import messages
from django.urls import reverse
from django.shortcuts import redirect
from octonet.mixins import OctonetMixin
from octonet.forms import FormControlClassMixin
from urllib.parse import quote_plus
from . import forms as aforms

class NewForm(FormControlClassMixin, forms.Form):
    name = forms.CharField(label=_("Name"), required=True)


class List(OctonetMixin, FormView):
    template_name = "asterisk/list.html"
    form_class = NewForm

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        tree = self.octofuss_tree("/asterisk/sip")
        phone_names = tree.list("")
        phones = [tree.get(x) for x in phone_names]
        ctx['phones'] = phones
        return ctx

    def form_valid(self, form):
        tree = self.octofuss_tree("/asterisk/sip")
        new_name = quote_plus(form.cleaned_data["name"])
        try:
            tree.create(new_name)
        except Exception as e:
            messages.error(self.request, _("Cannot create {}: {}").format(new_name, str(e)))
            return redirect("asterisk:list")
        return redirect("asterisk:edit", name=new_name)


class AsteriskMixin(OctonetMixin):
    def load_objects(self):
        super().load_objects()
        self.name = self.kwargs["name"]
        self.tree = self.octofuss_tree("/asterisk/sip/" + self.name)
        self.phone = self.tree.get("")

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        ctx["name"] = self.name
        ctx["phone"] = self.phone
        return ctx


class Edit(AsteriskMixin, FormView):
    template_name = "asterisk/edit.html"
    form_class = aforms.AsteriskPhoneForm

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        return ctx

    def form_valid(self, form):
        for k, v in form.cleaned_data.items():
            self.tree.set(k, v)
        messages.info(self.request, _("{} updated".format(self.name)))
        return super().form_valid(form)

    def get_success_url(self):
        return reverse("asterisk:list")

    def get_initial(self):
        return self.phone

class Delete(AsteriskMixin, View):
    def post(self, request, *args, **kw):
        self.tree.delete("")
        return redirect("asterisk:list")
