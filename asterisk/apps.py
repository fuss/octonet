# coding: utf-8
from django.apps import AppConfig
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from octonet.mixins import OctonetAppMixin

class Asterisk(OctonetAppMixin,AppConfig):
    name = 'asterisk'
    verbose_name = _("Switchboard")
    description = _("Asterisk switchboard configuration")
    group = _("Network")
    main_url = reverse_lazy("asterisk:list")
    font_awesome_class = "fa-phone"
    octofussd_url = "/asterisk"
