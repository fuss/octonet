# coding: utf-8
from django import forms
from django.utils.translation import gettext_lazy as _
from octonet.forms import FormControlClassMixin

class AsteriskPhoneForm(FormControlClassMixin, forms.Form):
    secret = forms.CharField(label=_("Secret"), required=True)
    host = forms.CharField(label=_("Host"), required=True)
    context = forms.CharField(label=_("Context"), required=True)
    mailbox = forms.CharField(label=_("Mailbox"), required=True)
