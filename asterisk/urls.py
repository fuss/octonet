from django.urls import re_path
from . import views

app_name = "asterisk"
urlpatterns = [
    re_path(r"^$", views.List.as_view(), name="list"),
    re_path(r"^edit/(?P<name>[^/]+)$", views.Edit.as_view(), name="edit"),
    re_path(r"^delete/(?P<name>[^/]+)$", views.Delete.as_view(), name="delete"),
]
