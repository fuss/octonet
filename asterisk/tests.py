# coding: utf-8
from django.urls import reverse
from django.test import SimpleTestCase
import octofuss
from octonet import backend
from octonet.unittest import TestBase


class LoginMixin():
    def setUp(self):
        # Login
        self.client.post(
            reverse("login"),
            data={
                "username": "root",
                "password": "root",
                "server_url": "http://localhost:13400/conf/",
            }
        )


class TestAsterisk(LoginMixin, TestBase, SimpleTestCase):
    """
    General views tests
    """
    def setUp(self):
        self.tree = octofuss.MockTree()
        self.tree.lcreate(["asterisk", "sip"])
        super().setUp()
        # Make it work after b0f11897be396e03fbf515e787ebdba4cddb9c7e (found thanks to git bisect)
        session = self.client.session
        session['apps'] = ['asterisk']
        session.save()

    def test_GET_list_gives_200(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("asterisk:list"))
            self.assertEqual(response.status_code, 200)

    def test_GET_edit_gives_200(self):
        self.tree.lcreate(["asterisk", "sip", "test_phone"], {
            'secret': "a", 'host': "a", 'context': "a", 'mailbox': "a",
        })
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("asterisk:edit", kwargs={'name': 'test_phone'}))
            self.assertEqual(response.status_code, 200)

    def test_bad_POST_edit_gives_200(self):
        self.tree.lcreate(["asterisk", "sip", "test_phone"], {
            'secret': "a", 'host': "a", 'context': "a", 'mailbox': "a",
        })
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(
                reverse("asterisk:edit", kwargs={'name': 'test_phone'}),
                {
                    'host': "a",
                    'context': "a",
                    'mailbox': "a@b.com",
                },
            )
            self.assertEqual(response.status_code, 200)

    def test_good_POST_edit_gives_302(self):
        self.tree.lcreate(["asterisk", "sip", "test_phone"], {
            'secret': "a", 'host': "a", 'context': "a", 'mailbox': "a",
        })
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(
                reverse("asterisk:edit", kwargs={'name': 'test_phone'}),
                {
                    'secret': "a",
                    'host': "a",
                    'context': "a",
                    'mailbox': "a@b.com",
                },
            )
            self.assertEqual(response.status_code, 302)
            self.assertRedirectMatches(response, reverse("asterisk:list"))

    def test_GET_delete_gives_405(self):
        self.tree.lcreate(["asterisk", "sip", "test_phone"], {
            'secret': "a", 'host': "a", 'context': "a", 'mailbox': "a",
        })
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("asterisk:delete", kwargs={'name': 'test_phone'}))
            self.assertEqual(response.status_code, 405)

    def test_bad_POST_delete_gives_302(self):
        self.tree.lcreate(["asterisk", "sip", "test_phone"], {
            'secret': "a", 'host': "a", 'context': "a", 'mailbox': "a",
        })
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(
                reverse("asterisk:delete", kwargs={'name': 'test_phone'}),
                {
                    'host': "a",
                    'context': "a",
                    'mailbox': "a@b.com",
                },
            )
            self.assertEqual(response.status_code, 302)
            self.assertRedirectMatches(response, reverse("asterisk:list"))

    def test_good_POST_delete_gives_302(self):
        self.tree.lcreate(["asterisk", "sip", "test_phone"], {
            'secret': "a", 'host': "a", 'context': "a", 'mailbox': "a",
        })
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(
                reverse("asterisk:delete", kwargs={'name': 'test_phone'}),
                {
                    'secret': "a",
                    'host': "a",
                    'context': "a",
                    'mailbox': "a@b.com",
                },
            )
            self.assertEqual(response.status_code, 302)
            self.assertRedirectMatches(response, reverse("asterisk:list"))

    def test_all_methods_give_403_if_app_not_active_in_session(self):
        session = self.client.session
        session['apps'] = []
        session.save()
        self.tree.lcreate(["asterisk", "sip", "test_phone"], {
            'secret': "a", 'host': "a", 'context': "a", 'mailbox': "a",
        })
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("asterisk:list"))
            self.assertEqual(response.status_code, 403)
            response = self.client.get(reverse("asterisk:edit", kwargs={'name': 'test_phone'}))
            self.assertEqual(response.status_code, 403)
            response = self.client.post(reverse("asterisk:edit", kwargs={'name': 'test_phone'}))
            self.assertEqual(response.status_code, 403)
            response = self.client.get(reverse("asterisk:delete", kwargs={'name': 'test_phone'}))
            self.assertEqual(response.status_code, 403)
            response = self.client.post(reverse("asterisk:delete", kwargs={'name': 'test_phone'}))
            self.assertEqual(response.status_code, 403)
