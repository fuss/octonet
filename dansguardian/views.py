# coding: utf-8
from urllib.parse import quote_plus, unquote_plus
from django import http
from django.views.generic import View
from django.views.generic.edit import FormView
from octonet.mixins import OctonetMixin
from . import forms as dforms


class Edit(OctonetMixin, FormView):
    form_class = dforms.AllowedSiteFormset
    template_name = "dansguardian/edit.html"

    def load_objects(self):
        super().load_objects()
        self.tree = self.octofuss_tree("/contentfilter")
        self.pages = [x for x in self.tree.list("") if x != "restart"]
        self.pages.sort()
        #self.pages = ['allowed_sites']
        self.page = self.kwargs.get("page", None)
        if self.page not in self.pages:
            self.page = None

    def get_initial(self):
        if not self.page: return []
        return [
            {
                "name": unquote_plus(name),
                "value": self.tree.lget([self.page, name])
            } for name in sorted(self.tree.list(self.page))
        ]

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        ctx["pages"] = self.pages
        ctx["page"] = self.page
        return ctx

    def form_valid(self, form):
        # Get the current values
        cur = {}
        for i in self.tree.list(self.page):
            cur[i] = self.tree.lget([self.page, i])
        print("CURRENT VALUES")
        # Get the new values
        new = {}
        for f in form:
            print("F CLEAN", f.cleaned_data)
            if not f.cleaned_data: continue
            if not f.cleaned_data["name"]: continue
            #if not f.cleaned_data["value"]: continue
            name = quote_plus(f.cleaned_data['name'])
            new[name] = f.cleaned_data["value"]
        # Add the new ones
        for key in new.keys() - cur.keys():
            self.tree.lcreate([self.page, key], new[key])
        # Edit the changed ones
        for key in new.keys() & cur.keys():
            self.tree.lset([self.page, key], new[key])
        # Delete the old ones
        for key in cur.keys() - new.keys():
            self.tree.ldelete([self.page, key])
        return super().form_valid(form)

        # for item in self.tree.list(self.page):
            # self.tree.delete("%s/%s" % (self.page, item))
        # for f in form:
            # if not f.cleaned_data: continue
            # if 'DELETE' in f.cleaned_data and f.cleaned_data['DELETE']: continue
            # self.tree.lcreate([self.page, f.cleaned_data['name']], f.cleaned_data['value'])
        # return super().form_valid(form)

    def get_success_url(self):
        return self.request.get_full_path()


class Restart(OctonetMixin, View):
    def post(self, request, *args, **kw):
        tree = self.octofuss_tree("/contentfilter")
        return http.JsonResponse({
            "success": tree.get("restart"),
        })
