# coding: utf-8
from django import forms
from django.utils.translation import gettext_lazy as _
from octonet.forms import FormControlClassMixin


class AllowedSiteForm(FormControlClassMixin, forms.Form):
    name = forms.CharField(label=_("URL"), required=False)
    value = forms.CharField(label=_("Description"), required=False)


AllowedSiteFormset = forms.formset_factory(AllowedSiteForm, extra=5)
