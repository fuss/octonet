# coding: utf-8
from django.urls import reverse
from django.test import SimpleTestCase
from octonet import backend
from octonet.unittest import TestBase
import octofuss


class TestDansguardian(TestBase, SimpleTestCase):
    def setUp(self):
        # Login
        self.client.post(reverse("login"), data={"username": "root", "password": "root", "server_url": "http://localhost:13400/conf/"})
        # Make it work after b0f11897be396e03fbf515e787ebdba4cddb9c7e (found thanks to git bisect)
        session = self.client.session
        session['apps'] = ['dansguardian']
        session.save()

    def test_POST_login_redirects_to_home(self):
        response = self.client.post(reverse("login"), data={"username": "root", "password": "root", "server_url": "http://localhost:13400/conf/"})
        self.assertRedirectMatches(response, reverse("home"))

    def test_GET_home_redirects_to_edit(self):
        response = self.client.get(reverse("dansguardian:home"))
        self.assertRedirectMatches(response, reverse("dansguardian:edit"))

    def test_GET_edit_gives_200(self):
        response = self.client.get(reverse("dansguardian:edit"))
        self.assertEqual(response.status_code, 200)

    def test_request_current_app(self):
        response = self.client.get(reverse("dansguardian:edit"))
        self.assertEqual(response.wsgi_request.current_app, 'dansguardian')

    def test_POST_with_no_modifications(self):
        tree = octofuss.MockTree()
        tree.create("contentfilter/allowed_sites/www.prova.example.org", 'A test site')
        with self.settings(TEST_USER=backend.TestUser(tree)):
            tree = octofuss.Subtree(tree, '/contentfilter')
            response = self.client.get(
                reverse("dansguardian:edit", kwargs={'page': 'allowed_sites'})
            )
            before = []
            for key in tree.list('/allowed_sites'):
                before.append({
                    'name': key,
                    'value': tree.get('/allowed_sites/%s' % key),
                })
            self.assertEqual(
                tree.list('/allowed_sites'),
                ['www.prova.example.org'],
            )
            post_data = {
                'form-0-name': "www.prova.example.org",
                'form-0-value': "A test site",
                'form-1-name': "",
                'form-1-value': "",
                'form-2-name': "",
                'form-2-value': "",
                'form-3-name': "",
                'form-3-value': "",
                'form-4-name': "",
                'form-4-value': "",
                'form-5-name': "",
                'form-5-value': "",
                'form-INITIAL_FORMS': 1,
                'form-MIN_NUM_FORMS': 0,
                'form-MAX_NUM_FORMS': 1000,
                'form-TOTAL_FORMS': 6,
            }
            response = self.client.post(
                reverse("dansguardian:edit", kwargs={'page': 'allowed_sites'}),
                post_data,
            )
            self.assertRedirectMatches(
                response,
                reverse("dansguardian:edit", kwargs={'page': 'allowed_sites'}),
            )
            response = self.client.get(
                reverse("dansguardian:edit", kwargs={'page': 'allowed_sites'})
            )
            after = []
            for key in tree.list('/allowed_sites'):
                after.append({
                    'name': key,
                    'value': tree.get('/allowed_sites/%s' % key),
                })
            self.assertCountEqual(before, after)

    def test_POST_modifying_the_name_of_the_only_item_present(self):
        tree = octofuss.MockTree()
        tree.create("contentfilter/allowed_sites/www.prova.example.org", 'A test site')
        with self.settings(TEST_USER=backend.TestUser(tree)):
            tree = octofuss.Subtree(tree, '/contentfilter')
            response = self.client.get(
                reverse("dansguardian:edit", kwargs={'page': 'allowed_sites'})
            )
            before = []
            for key in tree.list('/allowed_sites'):
                before.append({
                    'name': key,
                    'value': tree.get('/allowed_sites/%s' % key),
                })
            self.assertEqual(
                tree.list('/allowed_sites'),
                ['www.prova.example.org'],
            )
            new_key = "www.prova2.example.org"
            post_data = {
                'form-0-name': new_key,
                'form-0-value': "A test site",
                'form-1-name': "",
                'form-1-value': "",
                'form-2-name': "",
                'form-2-value': "",
                'form-3-name': "",
                'form-3-value': "",
                'form-4-name': "",
                'form-4-value': "",
                'form-5-name': "",
                'form-5-value': "",
                'form-INITIAL_FORMS': 1,
                'form-MIN_NUM_FORMS': 0,
                'form-MAX_NUM_FORMS': 1000,
                'form-TOTAL_FORMS': 6,
            }
            response = self.client.post(
                reverse("dansguardian:edit", kwargs={'page': 'allowed_sites'}),
                post_data,
            )
            self.assertRedirectMatches(
                response,
                reverse("dansguardian:edit", kwargs={'page': 'allowed_sites'}),
            )
            response = self.client.get(
                reverse("dansguardian:edit", kwargs={'page': 'allowed_sites'})
            )
            after = []
            for key in tree.list('/allowed_sites'):
                after.append({
                    'name': key,
                    'value': tree.get('/allowed_sites/%s' % key),
                })
            self.assertCountEqual(
                after,
                [{'name': new_key, 'value': 'A test site'}],
            )

    def test_POST_modifying_the_value_of_the_only_item_present(self):
        tree = octofuss.MockTree()
        tree.create("contentfilter/allowed_sites/www.prova.example.org", 'A test site')
        with self.settings(TEST_USER=backend.TestUser(tree)):
            tree = octofuss.Subtree(tree, '/contentfilter')
            response = self.client.get(
                reverse("dansguardian:edit", kwargs={'page': 'allowed_sites'})
            )
            before = []
            for key in tree.list('/allowed_sites'):
                before.append({
                    'name': key,
                    'value': tree.get('/allowed_sites/%s' % key),
                })
            self.assertEqual(
                tree.list('/allowed_sites'),
                ['www.prova.example.org'],
            )
            new_value = "Another test site"
            post_data = {
                'form-0-name': "www.prova.example.org",
                'form-0-value': new_value,
                'form-1-name': "",
                'form-1-value': "",
                'form-2-name': "",
                'form-2-value': "",
                'form-3-name': "",
                'form-3-value': "",
                'form-4-name': "",
                'form-4-value': "",
                'form-5-name': "",
                'form-5-value': "",
                'form-INITIAL_FORMS': 1,
                'form-MIN_NUM_FORMS': 0,
                'form-MAX_NUM_FORMS': 1000,
                'form-TOTAL_FORMS': 6,
            }
            response = self.client.post(
                reverse("dansguardian:edit", kwargs={'page': 'allowed_sites'}),
                post_data,
            )
            self.assertRedirectMatches(
                response,
                reverse("dansguardian:edit", kwargs={'page': 'allowed_sites'}),
            )
            response = self.client.get(
                reverse("dansguardian:edit", kwargs={'page': 'allowed_sites'})
            )
            after = []
            for key in tree.list('/allowed_sites'):
                after.append({
                    'name': key,
                    'value': tree.get('/allowed_sites/%s' % key),
                })
            self.assertCountEqual(
                after,
                [{'name': 'www.prova.example.org', 'value': new_value}],
            )

    def test_POST_modifying_name_and_value_of_the_only_item_present(self):
        tree = octofuss.MockTree()
        tree.create("contentfilter/allowed_sites/www.prova.example.org", 'A test site')
        with self.settings(TEST_USER=backend.TestUser(tree)):
            tree = octofuss.Subtree(tree, '/contentfilter')
            response = self.client.get(
                reverse("dansguardian:edit", kwargs={'page': 'allowed_sites'})
            )
            before = []
            for key in tree.list('/allowed_sites'):
                before.append({
                    'name': key,
                    'value': tree.get('/allowed_sites/%s' % key),
                })
            self.assertEqual(
                tree.list('/allowed_sites'),
                ['www.prova.example.org'],
            )
            new_name = "www.prova2.example.org"
            new_value = "Another test site"
            post_data = {
                'form-0-name': new_name,
                'form-0-value': new_value,
                'form-1-name': "",
                'form-1-value': "",
                'form-2-name': "",
                'form-2-value': "",
                'form-3-name': "",
                'form-3-value': "",
                'form-4-name': "",
                'form-4-value': "",
                'form-5-name': "",
                'form-5-value': "",
                'form-INITIAL_FORMS': 1,
                'form-MIN_NUM_FORMS': 0,
                'form-MAX_NUM_FORMS': 1000,
                'form-TOTAL_FORMS': 6,
            }
            response = self.client.post(
                reverse("dansguardian:edit", kwargs={'page': 'allowed_sites'}),
                post_data,
            )
            self.assertRedirectMatches(
                response,
                reverse("dansguardian:edit", kwargs={'page': 'allowed_sites'}),
            )
            response = self.client.get(
                reverse("dansguardian:edit", kwargs={'page': 'allowed_sites'})
            )
            after = []
            for key in tree.list('/allowed_sites'):
                after.append({
                    'name': key,
                    'value': tree.get('/allowed_sites/%s' % key),
                })
            self.assertCountEqual(
                after,
                [{'name': new_name, 'value': new_value}],
            )

    def test_POST_adding_a_new_item(self):
        tree = octofuss.MockTree()
        tree.create("contentfilter/allowed_sites/www.prova.example.org", 'A test site')
        with self.settings(TEST_USER=backend.TestUser(tree)):
            tree = octofuss.Subtree(tree, '/contentfilter')
            response = self.client.get(
                reverse("dansguardian:edit", kwargs={'page': 'allowed_sites'})
            )
            before = []
            for key in tree.list('/allowed_sites'):
                before.append({
                    'name': key,
                    'value': tree.get('/allowed_sites/%s' % key),
                })
            self.assertEqual(
                tree.list('/allowed_sites'),
                ['www.prova.example.org'],
            )
            new_name = "www.prova2.example.org"
            new_value = "Another test site"
            post_data = {
                'form-0-name': "www.prova.example.org",
                'form-0-value': "A test site",
                'form-1-name': new_name,
                'form-1-value': new_value,
                'form-2-name': "",
                'form-2-value': "",
                'form-3-name': "",
                'form-3-value': "",
                'form-4-name': "",
                'form-4-value': "",
                'form-5-name': "",
                'form-5-value': "",
                'form-INITIAL_FORMS': 1,
                'form-MIN_NUM_FORMS': 0,
                'form-MAX_NUM_FORMS': 1000,
                'form-TOTAL_FORMS': 6,
            }
            response = self.client.post(
                reverse("dansguardian:edit", kwargs={'page': 'allowed_sites'}),
                post_data,
            )
            self.assertRedirectMatches(
                response,
                reverse("dansguardian:edit", kwargs={'page': 'allowed_sites'}),
            )
            response = self.client.get(
                reverse("dansguardian:edit", kwargs={'page': 'allowed_sites'})
            )
            after = []
            for key in tree.list('/allowed_sites'):
                after.append({
                    'name': key,
                    'value': tree.get('/allowed_sites/%s' % key),
                })
            self.assertCountEqual(
                after,
                [
                    {'name': "www.prova.example.org", 'value': "A test site"},
                    {'name': new_name, 'value': new_value},
                ],
            )

    def test_POST_deleting_a_item(self):
        tree = octofuss.MockTree()
        tree.create("contentfilter/allowed_sites/www.prova.example.org", 'A test site')
        with self.settings(TEST_USER=backend.TestUser(tree)):
            tree = octofuss.Subtree(tree, '/contentfilter')
            response = self.client.get(
                reverse("dansguardian:edit", kwargs={'page': 'allowed_sites'})
            )
            before = []
            for key in tree.list('/allowed_sites'):
                before.append({
                    'name': key,
                    'value': tree.get('/allowed_sites/%s' % key),
                })
            self.assertEqual(
                tree.list('/allowed_sites'),
                ['www.prova.example.org'],
            )
            post_data = {
                'form-0-name': "",
                'form-0-value': "",
                'form-1-name': "",
                'form-1-value': "",
                'form-2-name': "",
                'form-2-value': "",
                'form-3-name': "",
                'form-3-value': "",
                'form-4-name': "",
                'form-4-value': "",
                'form-5-name': "",
                'form-5-value': "",
                'form-INITIAL_FORMS': 1,
                'form-MIN_NUM_FORMS': 0,
                'form-MAX_NUM_FORMS': 1000,
                'form-TOTAL_FORMS': 6,
            }
            response = self.client.post(
                reverse("dansguardian:edit", kwargs={'page': 'allowed_sites'}),
                post_data,
            )
            self.assertRedirectMatches(
                response,
                reverse("dansguardian:edit", kwargs={'page': 'allowed_sites'}),
            )
            response = self.client.get(
                reverse("dansguardian:edit", kwargs={'page': 'allowed_sites'})
            )
            after = []
            for key in tree.list('/allowed_sites'):
                after.append({
                    'name': key,
                    'value': tree.get('/allowed_sites/%s' % key),
                })
            self.assertCountEqual(after, [])

    def test_POST_an_empty_name_but_non_empty_value_deletes_item(self):
        tree = octofuss.MockTree()
        tree.create("contentfilter/allowed_sites/www.prova.example.org", 'A test site')
        with self.settings(TEST_USER=backend.TestUser(tree)):
            tree = octofuss.Subtree(tree, '/contentfilter')
            response = self.client.get(
                reverse("dansguardian:edit", kwargs={'page': 'allowed_sites'})
            )
            before = []
            for key in tree.list('/allowed_sites'):
                before.append({
                    'name': key,
                    'value': tree.get('/allowed_sites/%s' % key),
                })
            self.assertEqual(
                tree.list('/allowed_sites'),
                ['www.prova.example.org'],
            )
            post_data = {
                'form-0-name': "",
                'form-0-value': "A test site",
                'form-1-name': "",
                'form-1-value': "",
                'form-2-name': "",
                'form-2-value': "",
                'form-3-name': "",
                'form-3-value': "",
                'form-4-name': "",
                'form-4-value': "",
                'form-5-name': "",
                'form-5-value': "",
                'form-INITIAL_FORMS': 1,
                'form-MIN_NUM_FORMS': 0,
                'form-MAX_NUM_FORMS': 1000,
                'form-TOTAL_FORMS': 6,
            }
            response = self.client.post(
                reverse("dansguardian:edit", kwargs={'page': 'allowed_sites'}),
                post_data,
            )
            self.assertRedirectMatches(
                response,
                reverse("dansguardian:edit", kwargs={'page': 'allowed_sites'}),
            )
            response = self.client.get(
                reverse("dansguardian:edit", kwargs={'page': 'allowed_sites'})
            )
            after = []
            for key in tree.list('/allowed_sites'):
                after.append({
                    'name': key,
                    'value': tree.get('/allowed_sites/%s' % key),
                })
            self.assertCountEqual(after, [])

    def test_POST_an_empty_value_but_non_empty_name_deletes_item(self):
        tree = octofuss.MockTree()
        tree.create("contentfilter/allowed_sites/www.prova.example.org", 'A test site')
        with self.settings(TEST_USER=backend.TestUser(tree)):
            tree = octofuss.Subtree(tree, '/contentfilter')
            response = self.client.get(
                reverse("dansguardian:edit", kwargs={'page': 'allowed_sites'})
            )
            before = []
            for key in tree.list('/allowed_sites'):
                before.append({
                    'name': key,
                    'value': tree.get('/allowed_sites/%s' % key),
                })
            self.assertEqual(
                tree.list('/allowed_sites'),
                ['www.prova.example.org'],
            )
            post_data = {
                'form-0-name': "www.prova.example.org",
                'form-0-value': "",
                'form-1-name': "",
                'form-1-value': "",
                'form-2-name': "",
                'form-2-value': "",
                'form-3-name': "",
                'form-3-value': "",
                'form-4-name': "",
                'form-4-value': "",
                'form-5-name': "",
                'form-5-value': "",
                'form-INITIAL_FORMS': 1,
                'form-MIN_NUM_FORMS': 0,
                'form-MAX_NUM_FORMS': 1000,
                'form-TOTAL_FORMS': 6,
            }
            response = self.client.post(
                reverse("dansguardian:edit", kwargs={'page': 'allowed_sites'}),
                post_data,
            )
            self.assertRedirectMatches(
                response,
                reverse("dansguardian:edit", kwargs={'page': 'allowed_sites'}),
            )
            response = self.client.get(
                reverse("dansguardian:edit", kwargs={'page': 'allowed_sites'})
            )
            after = []
            for key in tree.list('/allowed_sites'):
                after.append({
                    'name': key,
                    'value': tree.get('/allowed_sites/%s' % key),
                })
            self.assertCountEqual(after, [])

    def test_all_methods_give_403_if_app_not_active_in_session(self):
        session = self.client.session
        session['apps'] = []
        session.save()
        response = self.client.get(reverse("dansguardian:home"), follow=True)
        self.assertEqual(response.status_code, 403)
        response = self.client.get(reverse("dansguardian:edit"))
        self.assertEqual(response.status_code, 403)
        response = self.client.post(reverse("dansguardian:edit"))
        self.assertEqual(response.status_code, 403)
