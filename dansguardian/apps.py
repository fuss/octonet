# coding: utf-8
from django.apps import AppConfig
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from octonet.mixins import OctonetAppMixin

class DansGuardian(OctonetAppMixin, AppConfig):
    name = 'dansguardian'
    verbose_name = _("Content filter")
    description = _("Configure sites allowed by the content filter")
    group = _("Network")
    main_url = reverse_lazy("dansguardian:edit")
    font_awesome_class = "fa-filter"
    octofussd_url = "/contentfilter"

