# coding: utf-8
from django.apps import AppConfig
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from octonet.mixins import OctonetAppMixin

class Firewall(OctonetAppMixin, AppConfig):
    name = 'firewall'
    verbose_name = _("Firewall")
    description = _("Control access to the network")
    group = _("Network")
    main_url = reverse_lazy("firewall:edit")
    font_awesome_class = "fa-fire"
    octofussd_url = "/firewall"

