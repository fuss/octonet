from django import http, forms
from django.views.generic import View
from django.views.generic.edit import FormView
from django.utils.translation import gettext_lazy as _
from octonet.mixins import OctonetMixin
from octonet.forms import FormControlClassMixin

class EntryForm(FormControlClassMixin, forms.Form):
    value = forms.CharField(label=_("Host"), required=False)
    note = forms.CharField(label=_("Description"), required=False)

pages_desc = {
    "denied_lan_hosts": _("Host without access"),
    "allowed_lan_hosts": _("Hosts full access"),
    "allowed_wan_hosts": _("Allowed destinations"),
    "allowed_wan_host_services": _("Allowed host/services"),
    "allowed_wan_services": _("Allowed services"),
    "external_services": _("Server external services"),
    }



class Edit(OctonetMixin, FormView):
    template_name = "firewall/edit.html"

    def load_objects(self):
        super().load_objects()
        self.tree = self.octofuss_tree("/firewall/conf")
        self.pages = dict()
        for p in self.tree.list(""):
            if p in pages_desc.keys():
                self.pages[p] = dict(name=p, desc=pages_desc[p])
        self.page = self.kwargs.get("page", None)
        if self.page not in self.pages:
            self.page = None
        self.page_title = None
        if self.page:
            self.page_title = pages_desc[self.page]

    def get_form_class(self):
        return forms.formset_factory(EntryForm, extra=2)

    def get_initial(self):
        if not self.page: return []
        return [ { "value": value, "note": self.tree.lget([self.page, value]) } for value in sorted(self.tree.list(self.page)) ]

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        ctx["pages"] = self.pages
        ctx["page"] = self.page
        ctx["page_title"] = self.page_title

        return ctx

    def form_valid(self, form):
        # Get the current values
        cur = {}
        for i in self.tree.list(self.page):
            cur[i] = self.tree.lget([self.page, i])

        # Get the new values
        new = {}
        for f in form:
            if not f.cleaned_data: continue
            if not f.cleaned_data["value"]: continue
            if not f.cleaned_data["note"]: continue
            new[f.cleaned_data["value"]] = f.cleaned_data["note"]

        # Add the new ones
        for key in new.keys() - cur.keys():
            self.tree.lcreate([self.page, key], new[key])

        # Edit the changed ones
        for key in new.keys() & cur.keys():
            self.tree.lset([self.page, key], new[key])

        # Delete the old ones
        for key in cur.keys() - new.keys():
            self.tree.ldelete([self.page, key])

        return super().form_valid(form)

    def get_success_url(self):
        return self.request.get_full_path()


class Restart(OctonetMixin, View):
    def post(self, request, *args, **kw):
        tree = self.octofuss_tree("/firewall")
        return http.JsonResponse({
            "success": tree.get("restart"),
        })
