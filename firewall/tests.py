# coding: utf-8
from django.urls import reverse
from django.test import SimpleTestCase
import octofuss
from octonet import backend
from octonet.unittest import TestBase


class LoginMixin():
    def setUp(self):
        # Login
        self.client.post(
            reverse("login"),
            data={
                "username": "root",
                "password": "root",
                "server_url": "http://localhost:13400/conf/",
            }
        )


class TestFirewall(LoginMixin, TestBase, SimpleTestCase):
    """
    General views tests
    """
    def setUp(self):
        self.tree = octofuss.MockTree()
        # For firewall:edit
        self.tree.lcreate(
            ["firewall", "conf"],
            {
                'allowed_lan_hosts': [],
                'denied_lan_hosts': [],
                'allowed_wan_hosts': [],
            },
        )
        super().setUp()
        # Make it work after b0f11897be396e03fbf515e787ebdba4cddb9c7e (found thanks to git bisect)
        session = self.client.session
        session['apps'] = ['firewall']
        session.save()

    def test_GET_home_redirects_to_edit(self):
        response = self.client.get(reverse("firewall:home"))
        self.assertRedirectMatches(response, reverse("firewall:edit"))

    def test_GET_edit_without_page_gives_200(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("firewall:edit"))
            self.assertEqual(response.status_code, 200)

    def test_GET_edit_with_page_gives_200(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("firewall:edit", kwargs={'page': 'allowed_lan_hosts'}))
            self.assertEqual(response.status_code, 200)

    # This is strange
    def test_GET_edit_with_non_existing_page_gives_200(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("firewall:edit", kwargs={'page': 'antani'}))
            self.assertEqual(response.status_code, 200)

    def test_GET_restart_gives_405(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("firewall:restart"))
            self.assertEqual(response.status_code, 405)

    def test_POST_restart_gives_200(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("firewall:restart"))
            self.assertEqual(response.status_code, 200)

    def test_all_methods_give_403_if_app_not_active_in_session(self):
        session = self.client.session
        session['apps'] = []
        session.save()
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("firewall:home"), follow=True)
            self.assertEqual(response.status_code, 403)
            response = self.client.get(reverse("firewall:edit"))
            self.assertEqual(response.status_code, 403)
            response = self.client.post(reverse("firewall:edit"))
            self.assertEqual(response.status_code, 403)
            response = self.client.get(reverse("firewall:restart"))
            self.assertEqual(response.status_code, 403)
            response = self.client.post(reverse("firewall:restart"))
            self.assertEqual(response.status_code, 403)
