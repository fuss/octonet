from django.urls import re_path
from django.views.generic.base import RedirectView
from . import views

app_name = "firewall"
urlpatterns = [
    re_path(r'^$', RedirectView.as_view(pattern_name='firewall:edit', permanent=True), name='home'),
    re_path(r"^edit/(?P<page>[^/]+)?$", views.Edit.as_view(), name="edit"),
    re_path(r"^restart/$", views.Restart.as_view(), name="restart"),
]
