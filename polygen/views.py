# coding: utf-8
import os
from django import http
from django.contrib import messages
from django.shortcuts import redirect
from django.utils.translation import gettext_lazy as _
from django.views.generic import TemplateView
from octonet.mixins import OctonetMixin

class InvalidPath(Exception):
    pass


class List(OctonetMixin, TemplateView):
    template_name = "polygen/list.html"

    def split_path(self, path):
        lang = None
        grammar = None
        if path:
            if '/' in path:
                splitted = path.split("/")
                if len(splitted) == 1:
                    lang = splitted[0]
                elif len(splitted) >= 2:
                    lang = splitted[0]
                    grammar = splitted[1]
            else:
                # There is path and no '/' in it, so this is the language
                lang = path
        return (lang, grammar)

    def load_objects(self):
        super().load_objects()
        path = self.kwargs.get('path', "")
        if not path: path = ""
        self.lang, self.grammar = self.split_path(path)
        self.tree = self.octofuss_tree("/polygen")
        if not self.tree.has(path):
            raise InvalidPath

    def dispatch(self, request, *args, **kwargs):
        try:
            return super().dispatch(request, *args, **kwargs)
        except InvalidPath:
            messages.error(request, _("Path not found"))
            # This is a little ugly for the UI/UX, even if it's correct from a HTTP PoV.
            # return http.HttpResponseNotFound(_("Path not found"))
            return redirect('polygen:list')

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        if not self.grammar:
            if not self.lang:
                # There is no lang nor grammar, generate the list of languages
                dirs = sorted(self.tree.list("."))
                phrase = ""
            else:
                # There is only lang, generate the list of grammars for this language
                dirs = sorted(self.tree.list(self.lang))
                phrase = ""
        else:
            # There is lang and grammar, generate a phrase
            path = os.path.join(self.lang, self.grammar)
            dirs = ""
            phrase = self.tree.get(path)
        ctx['lang'] = self.lang
        ctx['grammar'] = self.grammar
        ctx['dirs'] = dirs
        ctx['phrase'] = phrase
        return ctx
