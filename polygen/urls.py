# coding: utf-8
from django.urls import re_path
from . import views

app_name = "polygen"
urlpatterns = [
    re_path(r"^(?P<path>[a-zA-Z0-9_\-/]+)?$", views.List.as_view(), name="list"),
    # re_path(r"^(?P<lang>(eng|ita|fra))?/(?P<grammar>[\w\-]+)?$", views.List.as_view(), name="list"),
]
