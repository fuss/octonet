# coding: utf-8
from django.apps import AppConfig
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.conf import settings
from octonet.mixins import OctonetAppMixin

class Polygen(OctonetAppMixin,AppConfig):
    name = 'polygen'
    verbose_name = _("Polygen")
    description = _("Welcome to the Polygen app: select a grammar")
    group = _("Fun")
    main_url = reverse_lazy("polygen:list")
    font_awesome_class = "fa-quote-right"
    octofussd_url = "/polygen"
    @classmethod
    def is_active(cls, tree):
        if settings.DEBUG:
            # refs #249
            return tree.has(cls.octofussd_url)
        return False
