from django import http
from django.views.generic import View, TemplateView
from octonet.mixins import OctonetMixin

class List(OctonetMixin, TemplateView):
    """
    List queue contents
    """
    template_name = "hostqueue/list.html"

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        tree = self.octofuss_tree("/hostqueue")
        ctx["types"] = tree.list("")
        ctx["statuses"] = tree.list("all")
        return ctx


class Query(OctonetMixin, View):
    def get(self, request, *args, **kw):

        # type (all, install, upgrade, script)
        data_type = request.GET.get("type", "all")

        # status (all, done, todo, failed, ok)
        status = request.GET.get("status", "all")

        # host (*, name)
        host = request.GET.get("host", "*")

        tree = self.octofuss_tree("/hostqueue")
        return http.JsonResponse({
            "res": tree.lget([data_type, status, host])
        })
