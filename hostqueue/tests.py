from octonet.unittest import TestBase
from django.urls import reverse
from django.test import SimpleTestCase
from octonet import backend
import octofuss
import json

class TestHostqueue(TestBase, SimpleTestCase):
    def setUp(self):
        self.client.post(reverse("login"), data={"username": "root", "password": "root", "server_url": "http://localhost:13400/conf/"})
        # Make it work after b0f11897be396e03fbf515e787ebdba4cddb9c7e (found thanks to git bisect)
        session = self.client.session
        session['apps'] = ['hostqueue']
        session.save()

    def testList(self):
        response = self.client.get(reverse("hostqueue:list"))
        self.assertEquals(response.status_code, 200)

    def testQuery(self):
        tree = octofuss.MockTree()
        tree.create("hostqueue/all/all/*", [{"test": 1}])
        with self.settings(TEST_USER=backend.TestUser(tree)):
            response = self.client.get(reverse("hostqueue:query"), data={})
            self.assertEquals(response.status_code, 200)
            data = json.loads(response.content.decode("utf-8"))
            self.assertEquals(data, { "res": [{"test": 1}] })

    def test_all_methods_give_403_if_app_not_active_in_session(self):
        session = self.client.session
        session['apps'] = []
        session.save()
        tree = octofuss.MockTree()
        tree.create("hostqueue/all/all/*", [{"test": 1}])
        with self.settings(TEST_USER=backend.TestUser(tree)):
            response = self.client.get(reverse("hostqueue:list"))
            self.assertEqual(response.status_code, 403)
            response = self.client.post(reverse("hostqueue:list"))
            self.assertEqual(response.status_code, 403)
            response = self.client.get(reverse("hostqueue:query"), data={})
            self.assertEqual(response.status_code, 403)
            response = self.client.post(reverse("hostqueue:query"), data={})
            self.assertEqual(response.status_code, 403)
