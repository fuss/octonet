# coding: utf-8
from django.apps import AppConfig
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from octonet.mixins import OctonetAppMixin

class HostQueue(OctonetAppMixin, AppConfig):
    name = 'hostqueue'
    verbose_name = _("Queue of host actions")
    description = _("View the contents of the queue of actions sent to hosts")
    group = _("Hosts")
    main_url = reverse_lazy("hostqueue:list")
    font_awesome_class = "fa-history"
    octofussd_url = "/hostqueue"
