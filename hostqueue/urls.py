from django.urls import re_path
from . import views

app_name = "hostqueue"
urlpatterns = [
    re_path(r"^$", views.List.as_view(), name="list"),
    re_path(r"^query$", views.Query.as_view(), name="query"),
]

