# coding: utf-8
from django.apps import AppConfig
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from octonet.mixins import OctonetAppMixin

class Samba(OctonetAppMixin, AppConfig):
    name = 'samba'
    verbose_name = _("Samba Service")
    description = _("Samba")
    group = _("Shares")
    main_url = reverse_lazy("samba:list")
    font_awesome_class = "fa-share-alt"
    octofussd_url = "/samba"

