from django.utils.translation import gettext_lazy as _
from django import http, forms
from django.views.generic import View, TemplateView
from django.views.generic.edit import FormView
from django.urls import reverse
from django.shortcuts import redirect
from octonet.mixins import OctonetMixin
from octonet.forms import FormControlClassMixin
from urllib.parse import unquote_plus
import re

def _as_bool(s):
    if s is True: return True
    if s is False: return False
    if s.lower() in ["yes", "true", "y", "ok", "on"]: return True
    if s.lower() in ["no", "false", "f", "off"]: return False
    raise ValueError("'{}' cannot be interpreted as true or false".format(s))

class ShareListMixin:
    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        tree = self.octofuss_tree("/samba")

        shares = tree.lget([])
        parms = shares.pop("global", {})
        ctx.update(
            parms=sorted(parms.items()),
            shares=shares,
            )
        return ctx


class List(OctonetMixin, ShareListMixin, TemplateView):
    template_name = "samba/list.html"


class EditForm(FormControlClassMixin, forms.Form):
    path = forms.CharField(label=_("Path"))
    guestok = forms.BooleanField(label=_("Guest ok"), required=False)
    readonly = forms.BooleanField(label=_("Read only"), initial=True, required=False)
    readlist = forms.MultipleChoiceField(label=_("Read list"), required=False)
    writelist = forms.MultipleChoiceField(label=_("Write list"), required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['path'].widget = forms.TextInput(attrs={
            'class': 'form-control',
            'readonly': 'readonly',
        })


class CreateForm(EditForm):
    name = forms.CharField(label=_("Name"))

    def clean_name(self):
        name = self.cleaned_data["name"].strip()

        # refs #13851
        if name != name.lower():
            raise forms.ValidationError(_("A share name cannot contain uppercase characters"), code="no_uppercase")

        if name == "generic":
            raise forms.ValidationError(_("A share cannot be called 'generic'"), code="no_generic")

        if '/' in name:
            raise forms.ValidationError(_("A share name cannot contain a '/'"), code="no_slashes")

        if '..' in name:
            raise forms.ValidationError(_("A share name cannot contain '..'"), code="no_dot_dot")

        if ' ' in name:
            raise forms.ValidationError(_("A share name cannot contain spaces"), code="no_space")

        # refs #13851
        if name.lower() in [x.lower() for x in self.tree.llist(["samba"])]:
            raise forms.ValidationError(_("A share with this name already exists"), code="duplicate")

        return name


class ShareFormMixin:
    re_splitrwlist = re.compile(r"\s*,\s*")

    def _split_rwlist(self, val):
        val = val.strip()
        if not val: return []
        return [x.lstrip("@") for x in self.re_splitrwlist.split(val)]

    def get_form_class(self):
        group_choices = [("@"+x, unquote_plus(x)) for x in self.groups]
        parent = self

        class Form(self.form_class):
            def __init__(self, *args, **kw):
                super().__init__(*args, **kw)
                # Set choices to the list of groups in the system
                self.fields["readlist"].choices = group_choices
                self.fields["writelist"].choices = group_choices
                self.tree = parent.root_tree

        return Form


class Create(ShareListMixin, ShareFormMixin, OctonetMixin, FormView):
    template_name = "samba/edit.html"
    form_class = CreateForm

    def get_form(self, form_class=None):
        # FIXME: just a hack to change the field order,
        # since a FormView doesn't support the 'fields' in Meta
        import collections
        if form_class is None:
            form_class = self.get_form_class()
        the_form = form_class(**self.get_form_kwargs())

        fields = collections.OrderedDict()
        fields['name'] = the_form.fields.pop("name")
        for key in the_form.fields:
            fields[key] = the_form.fields[key]
        the_form.fields = fields

        return the_form

    def get_success_url(self):
        return reverse("samba:list")

    def load_objects(self):
        super().load_objects()
        self.groups = sorted(self.root_tree.llist(["users", "groups"]))

    def form_valid(self, form):
        self.name = form.cleaned_data["name"]
        share = {
            "path": form.cleaned_data["path"],
            "guestok": form.cleaned_data["guestok"] and "yes" or "no",
            "readonly": form.cleaned_data["readonly"] and "yes" or "no",
            "readlist": ", ".join(form.cleaned_data["readlist"]),
            "writelist": ", ".join(form.cleaned_data["writelist"]),
        }
        self.root_tree.lcreate(["samba", self.name], share)
        return super().form_valid(form)


class Edit(ShareListMixin, ShareFormMixin, OctonetMixin, FormView):
    template_name = "samba/edit.html"
    form_class = EditForm

    def get_success_url(self):
        return reverse("samba:list")

    def load_objects(self):
        super().load_objects()
        self.name = self.kwargs["share"]
        # No share can be named 'generic'
        if self.name == "generic": raise http.Http404
        self.share = self.root_tree.lget(["samba", self.name])
        groups = set(self.root_tree.llist(["users", "groups"]))
        groups.update(self._split_rwlist(self.share.get("readlist", "")))
        groups.update(self._split_rwlist(self.share.get("writelist", "")))
        self.groups = sorted(groups)

    def get_initial(self):
        rlist = self.re_splitrwlist.split(self.share.get("readlist", ""))
        wlist = self.re_splitrwlist.split(self.share.get("writelist", ""))
        return {
            "path": self.share.get("path", ""),
            "guestok": _as_bool(self.share.get("guestok", False)),
            "readonly": _as_bool(self.share.get("readonly", True)),
            "readlist": rlist,
            "writelist": wlist,
        }

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        ctx["name"] = self.name
        ctx["share"] = self.share
        return ctx

    def form_valid(self, form):
        self.share["path"] = form.cleaned_data["path"]
        self.share["guestok"] = form.cleaned_data["guestok"] and "yes" or "no"
        self.share["readonly"] = form.cleaned_data["readonly"] and "yes" or "no"
        self.share["readlist"] = ", ".join(form.cleaned_data["readlist"])
        self.share["writelist"] = ", ".join(form.cleaned_data["writelist"])
        self.root_tree.lset(["samba", self.name], self.share)
        return super().form_valid(form)


class Delete(OctonetMixin, View):
    def post(self, request, *args, **kw):
        self.root_tree.ldelete(["samba", self.kwargs["share"]])
        return redirect("samba:list")
