from django.urls import re_path
from . import views


app_name = "samba"
urlpatterns = [
    re_path(r"^$", views.List.as_view(), name="list"),
    re_path(r"^edit/(?P<share>[^/]+)$", views.Edit.as_view(), name="edit"),
    re_path(r"^delete/(?P<share>[^/]+)$", views.Delete.as_view(), name="delete"),
    re_path(r"^create$", views.Create.as_view(), name="create"),
]
