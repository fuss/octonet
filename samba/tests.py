# coding: utf-8
from django.urls import reverse
from django.test import SimpleTestCase
import octofuss
from octonet import backend
from octonet.unittest import TestBase


class LoginMixin():
    def setUp(self):
        # Login
        self.client.post(
            reverse("login"),
            data={
                "username": "root",
                "password": "root",
                "server_url": "http://localhost:13400/conf/",
            }
        )


class TestSamba(LoginMixin, TestBase, SimpleTestCase):
    """
    General views tests
    """
    def setUp(self):
        self.tree = octofuss.MockTree()
        super().setUp()
        self.tree.lcreate(
            ["samba"],
            {'public': {'path': '/public', 'guest ok': 'no', 'read only': 'no'}},
        )
        self.tree.lcreate(["users", "groups", "gruppo"])
        session = self.client.session
        session['apps'] = ['samba']
        session.save()

    def test_GET_list_gives_200(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("samba:list"))
            self.assertEqual(response.status_code, 200)

    def test_POST_list_gives_405(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("samba:list"))
            self.assertEqual(response.status_code, 405)

    def test_GET_edit_gives_200(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("samba:edit", kwargs={'share': 'public'}))
            self.assertEqual(response.status_code, 200)

    def test_POST_edit_gives_200(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("samba:edit", kwargs={'share': 'public'}))
            self.assertEqual(response.status_code, 200)

    def test_GET_delete_gives_405(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("samba:delete", kwargs={'share': 'public'}))
            self.assertEqual(response.status_code, 405)

    def test_POST_delete_gives_302(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("samba:delete", kwargs={'share': 'public'}))
            self.assertEqual(response.status_code, 302)
            self.assertRedirectMatches(response, reverse("samba:list"))

    def test_GET_create_gives_200(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("samba:create"))
            self.assertEqual(response.status_code, 200)

    def test_EMPTY_POST_create_gives_200(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("samba:create"))
            self.assertEqual(response.status_code, 200)

    def test_POST_create_gives_302(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("samba:create"), {'name': "antani", 'path': "/antani"})
            self.assertEqual(response.status_code, 302)
            self.assertRedirectMatches(response, reverse("samba:list"))
