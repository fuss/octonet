# coding: utf-8
from django.apps import AppConfig
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from octonet.mixins import OctonetAppMixin

class Quota(OctonetAppMixin, AppConfig):
    weight = 11
    name = 'quota'
    verbose_name = _("Disk Quotas")
    description = _("Manage user and group quotas")
    group = _("Users")
    main_url = reverse_lazy("quota:list")
    font_awesome_class = "fa-pie-chart"
    octofussd_url = "/quota"
