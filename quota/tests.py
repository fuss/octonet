# coding: utf-8
from django.urls import reverse
from django.test import SimpleTestCase
import octofuss
from octonet import backend
from octonet.unittest import TestBase


class LoginMixin():
    def setUp(self):
        # Login
        self.client.post(
            reverse("login"),
            data={
                "username": "root",
                "password": "root",
                "server_url": "http://localhost:13400/conf/",
            }
        )


class TestQuota(LoginMixin, TestBase, SimpleTestCase):
    """
    General views tests
    """
    def setUp(self):
        self.tree = octofuss.MockTree()
        self.tree.lcreate(["quota", "root"], [])
        self.tree.lcreate(["serverinfo", "filesystems"], {"/": 50})
        super().setUp()
        session = self.client.session
        session['apps'] = ['quota']
        session.save()

    def test_GET_list_gives_200(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("quota:list"))
            self.assertEqual(response.status_code, 200)

    def test_POST_list_gives_405(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("quota:list"))
            self.assertEqual(response.status_code, 405)

    def test_GET_edit_gives_200(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("quota:edit", kwargs={'fs': 'root'}))
            self.assertEqual(response.status_code, 200)

    def test_POST_edit_gives_405(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("quota:edit", kwargs={'fs': 'root'}))
            self.assertEqual(response.status_code, 405)

    def test_GET_save_gives_405(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("quota:save", kwargs={'fs': 'root'}))
            self.assertEqual(response.status_code, 405)

    def test_POST_save_gives_200(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            data = {
                'ugtype': 'user',
                'name': '/',
                'type': 'disk_soft',
            }
            response = self.client.post(
                reverse("quota:save", kwargs={'fs': 'root'}),
                data,
            )
            self.assertEqual(response.status_code, 200)
