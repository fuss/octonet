from django.urls import re_path
from . import views


app_name = "quota"
urlpatterns = [
    re_path(r"^$", views.List.as_view(), name="list"),
    re_path(r"^edit/(?P<fs>[^/]+)$", views.Edit.as_view(), name="edit"),
    re_path(r"^edit/(?P<fs>[^/]+)/save$", views.Save.as_view(), name="save"),
]
