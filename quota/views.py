from django import http
from django.views.generic import View
from django.views.generic import TemplateView
from octonet.mixins import OctonetMixin


class List(OctonetMixin, TemplateView):
    template_name = "quota/list.html"

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        # refs #161
        quota_filesystems = self.root_tree.llist(["quota"])
        serverinfo_filesystems = self.root_tree.lget(["serverinfo", "filesystems"])
        seen = set()
        filesystems = []
        # Get all filesystems, in quota and in serverinfo, and show % if there is a match,
        # otherwise show a "-"
        for fs in list(serverinfo_filesystems.keys()) + quota_filesystems:
            # The code in here is cop/pasted from mangle_fs_names() in the quota octofussd plugin
            # FIXME: Maybe a refactor in octofussd is needed, but it will break the GTK client.
            # FIXME: Maybe use quote_plus/unquote_plus that is reproducible in both ways? The
            # existing algorithm is dependant on the sorting order in /etc/fstab, or on the quirks
            # systemd maybe, and it's not predictable nor reversable (because of the numbers added
            # if a mangled name is already seen)
            # Turn it into a name that fits in the octofuss tree
            name = fs.strip("/").replace("/", "-")
            if name == "": name = "root"
            # Prevent collisions after mangling
            cand = name
            idx = 0
            while cand in seen:
                idx += 1
                cand = name + str(idx)
            if fs in serverinfo_filesystems:
                percentage = "%d%%" % int(serverinfo_filesystems[fs])
            else:
                percentage = "-"
            filesystems.append({
                'fs': fs,
                'mangled_fs': cand,
                'percentage': percentage,
            })
        ctx['filesystems'] = filesystems
        return ctx


class FSMixin(OctonetMixin):
    def load_objects(self):
        super().load_objects()
        self.fs = self.kwargs["fs"]

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        ctx["fs"] = self.fs
        return ctx


class Edit(FSMixin, TemplateView):
    template_name = "quota/edit.html"

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        ctx["quota_user"] = self.root_tree.lget(["quota", self.fs, "user"])
        ctx["quota_group"] = self.root_tree.lget(["quota", self.fs, "group"])
        return ctx


class Save(FSMixin, View):
    def post(self, request, *args, **kw):
        ugtype = request.POST["ugtype"]
        if ugtype not in ("user", "group"):
            return http.JsonResponse({"status": "invalid user/group type"})
        name = request.POST["name"]
        if "/" in name or name.startswith("."):
            return http.JsonResponse({"status": "invalid user/group name"})
        type = request.POST["type"]
        if type not in ("disk_soft", "disk_hard", "file_soft", "file_hard"):
            return http.JsonResponse({"status": "invalid quota type"})
        value = int(request.POST["value"])
        self.root_tree.lset(["quota", self.fs, ugtype, name, type], value)
        return http.JsonResponse({"status": "ok"})
