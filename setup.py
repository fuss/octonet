#!/usr/bin/env python3

from setuptools import setup, find_packages
import os

def list_locale_files():
    res = []
    for root, dirs, files in os.walk("locale"):
        res.append((os.path.join("share/octonet", root), [os.path.join(root, f) for f in files]))
    return res


setup(
    name='octonet',
    description='OctoNet',
    version='13.0.0',
    license='GPL3',
    author='The FUSS Project',
    author_email='packages@fuss.bz.it',
    url='https://gitlab.fuss.bz.it/fuss/octonet/',
    include_package_data=True,
    # package_data={
    #     '': ["templates/*", "static/*"],
    # },
    data_files=list_locale_files(),
    packages=find_packages(),
    scripts=["manage.py"],
    install_requires=[
        "django>=1.8",
        "pyoctofuss>=1.6.6",
        "pytz",
    ],
)
