#!/bin/bash

SETUP_VER="$(grep 'version=' setup.py | cut -d "'" -f 2)"
RELEASE_VER="$(grep octonet debian/changelog | head -1 | cut -d '(' -f 2 | cut -d '-' -f 1)"

if ! [ "$SETUP_VER" = "$RELEASE_VER" ] ; then
  echo "=========== ERROR ==========="
  echo "Version in debian/changelog and setup.py do not match!"
  echo "$RELEASE_VER vs $SETUP_VER"
  exit 1
fi

