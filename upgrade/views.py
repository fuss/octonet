from django.utils.translation import gettext_lazy as _
from django import http, forms
from django.views.generic import View, TemplateView
from django.views.generic.edit import FormView
from django.contrib import messages
from django.shortcuts import redirect
from octonet.mixins import OctonetMixin
from octonet.forms import FormControlClassMixin
from urllib.parse import quote_plus, unquote_plus

#def upgrade_cmp(a, b):
#    res = cmp(a["completed"], b["completed"])
#    if res != 0: return res
#    res = cmp(a["scheduled"], b["scheduled"])
#    if res != 0: return res
#    return cmp(a["name"], b["name"])
#

class NewForm(FormControlClassMixin, forms.Form):
    name = forms.CharField(label=_("Name"), required=True)


class List(OctonetMixin, FormView):
    template_name = "upgrade/list.html"
    form_class = NewForm

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        tree = self.octofuss_tree("/upgrades")
        udata = tree.get("")
        for u in udata:
            u["quoted_name"] = u["name"]
            u["name"] = unquote_plus(u["name"])
        udata.sort(key=lambda x:(x["completed"] or "", x["scheduled"] or "", x["name"]))
        ctx["udata"] = udata
        return ctx

    def form_valid(self, form):
        tree = self.octofuss_tree("/upgrades")
        new_name = quote_plus(form.cleaned_data["name"])
        try:
            tree.create(new_name)
        except Exception as e:
            messages.error(self.request, _("Cannot create {}: {}").format(new_name, str(e)))
            return redirect("upgrade:list")
        return redirect("upgrade:edit", name=new_name)


class UpgradeMixin(OctonetMixin):
    def load_objects(self):
        super().load_objects()
        self.name = self.kwargs["name"]
        self.tree = self.octofuss_tree("/upgrades/" + self.name)
        self.upgrade = self.tree.get("")

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        ctx["name"] = self.name
        ctx["upgrade"] = self.upgrade
        return ctx


class Edit(UpgradeMixin, TemplateView):
    template_name = "upgrade/edit.html"

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        ctx.update(
#                update_form = update_form,
#                new_group_form = new_group_form,
#                del_group_form = del_group_form,
        )
        return ctx


class Schedule(UpgradeMixin, View):
    def post(self, request, *args, **kw):
        if not self.upgrade["hosts"]:
            messages.error(self.request, _("Cannot schedule an upgrade without hosts"))
            return redirect("upgrade:edit", name=self.name)

        if self.upgrade["scheduled"]:
            messages.error(self.request, _("The upgrade has already been scheduled"))
            return redirect("upgrade:edit", name=self.name)

        try:
            self.tree.set("scheduled", True)
        except Exception as e:
            messages.error(self.request, _("Cannot schedule {}: {}").format(self.name, str(e)))

        return redirect("upgrade:edit", name=self.name)


class AddHost(UpgradeMixin, View):
    def post(self, request, *args, **kw):
        host = self.request.POST.get("host", None)
        if not host:
            messages.error(self.request, _("No host specified"))
            return redirect("upgrade:edit", name=self.name)

        host = quote_plus(host)
        try:
            self.tree.lcreate(["hosts", host])
        except Exception as e:
            messages.error(self.request, _("Cannot add host {}: {}").format(host, str(e)))

        return redirect("upgrade:edit", name=self.name)

#class NewHostForm(widgets.SingleFieldForm):
#    class FieldList(WidgetsList):
#        host = AutoCompleteField(
#                label = "Add host",
#                search_controller="../../search/hosts/",
#                search_param="name",
#                result_name="names")
#    name="newhostform"
#    fields = FieldList()
#    submit_text = _("Add")


class RemoveHost(UpgradeMixin, View):
    def post(self, request, *args, **kw):
        host = self.request.POST.get("host", None)
        if not host:
            messages.error(self.request, _("No host specified"))
            return redirect("upgrade:edit", name=self.name)

        host = quote_plus(host)
        try:
            self.tree.ldelete(["hosts", host])
        except Exception as e:
            messages.error(self.request, _("Cannot remove host {}: {}").format(host, str(e)))

        return redirect("upgrade:edit", name=self.name)


class AddGroup(UpgradeMixin, View):
    def post(self, request, *args, **kw):
        group = self.request.POST.get("group", None)
        if not group:
            messages.error(self.request, _("No group specified"))
            return redirect("upgrade:edit", name=self.name)

        try:
            hosts = self.root_tree.llist(["cluster", group])
        except Exception as e:
            messages.error(self.request, _("Cannot list hosts for group {}: {}").format(group, str(e)))
            return redirect("upgrade:edit", name=self.name)

        for host in hosts:
            if host[0] == ":": continue
            try:
                self.tree.lcreate(["hosts", host])
            except Exception as e:
                messages.error(self.request, _("Cannot add host {}: {}").format(host, str(e)))

        return redirect("upgrade:edit", name=self.name)


class RemoveGroup(UpgradeMixin, View):
    def post(self, request, *args, **kw):
        group = self.request.POST.get("group", None)
        if not group:
            messages.error(self.request, _("No group specified"))
            return redirect("upgrade:edit", name=self.name)

        try:
            hosts = self.root_tree.llist(["cluster", group])
        except Exception as e:
            messages.error(self.request, _("Cannot list hosts for group {}: {}").format(group, str(e)))
            return redirect("upgrade:edit", name=self.name)

        for host in hosts:
            if host[0] == ":": continue
            try:
                if not self.tree.lhas(["hosts", host]):
                    continue
            except Exception as e:
                messages.error(self.request, _("Cannot check if host {} is in the group: {}").format(host, str(e)))

            try:
                self.tree.ldelete(["hosts", host])
            except Exception as e:
                messages.error(self.request, _("Cannot remove host {}: {}").format(host, str(e)))

        return redirect("upgrade:edit", name=self.name)


class Delete(UpgradeMixin, View):
    def post(self, request, *args, **kw):
        info = self.tree.get("")

        if info["scheduled"] and not info["completed"]:
            messages.error(self.request, _("Cannot delete {} because it is currently scheduled and not yet completed").format(self.name))
            return redirect("upgrade:edit", name=self.name)
        else:
            self.tree.delete("")
            return redirect("upgrade:list")


class Update(UpgradeMixin, View):
    def post(self, request, *args, **kw):
        upgrade_type = self.request.POST.get("type", None)
        if upgrade_type not in ["upgrade", "dist-upgrade"]:
            messages.error(self.request, _("{} is not a valid type for upgrade {}").format(upgrade_type, self.name))
            return redirect("upgrade:edit", name=self.name)
        self.tree.set("type", upgrade_type)
        return redirect("upgrade:edit", name=self.name)


class Complete(OctonetMixin, View):
    ROOTS = {
        "hosts": "/computers",
        "groups": "/cluster",
    }

    def get(self, request, *args, **kw):
        name = self.request.GET.get("name", "")
        domain = self.kwargs.get("domain", None)
        root = self.ROOTS.get(domain, None)
        if root is None: raise http.Http404()
        tree = self.octofuss_tree(root)
        return http.JsonResponse({
            "names": [n for n in tree.list("") if n.lower().startswith(name.lower())],
        })
