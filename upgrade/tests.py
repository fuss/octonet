from octonet.unittest import TestBase
from django.urls import reverse
from django.test import SimpleTestCase
from octonet import backend
import octofuss
import json

class TestHostqueue(TestBase, SimpleTestCase):
    def setUp(self):
        super().setUp()
        self.tree = octofuss.MockTree()
        # Make it work after b0f11897be396e03fbf515e787ebdba4cddb9c7e (found thanks to git bisect)
        session = self.client.session
        session['apps'] = ['upgrade']
        session.save()

    def test_list(self):
        self.tree.create("upgrades", [{"name": "test1", "type": "upgrade", "scheduled": None, "completed": None, "hosts": []}])
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("upgrade:list"))
            self.assertEquals(response.status_code, 200)
            self.assertContains(response, '<td class="u_type">upgrade</td>')
            self.assertContains(response, '<td class="u_sche">--</td>')
            self.assertContains(response, '<td class="u_comp">--</td>')
            self.assertContains(response, '<td class="u_hosts" align="right">0</td>')

    def test_edit(self):
        self.tree = octofuss.MockTree()
        self.tree.create("upgrades/test1", {"name": "test1", "type": "upgrade", "scheduled": None, "completed": None, "hosts": []})
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("upgrade:edit", args=["test1"]))
            self.assertEquals(response.status_code, 200)
            self.assertContains(response, "Not completed yet")
            self.assertNotContains(response, "Schedule")
            self.assertContains(response, "Add host:")

    def test_schedule_no_hosts(self):
        self.tree = octofuss.MockTree()
        self.tree.create("upgrades/test1", {"name": "test1", "type": "upgrade", "scheduled": None, "completed": None, "hosts": []})
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("upgrade:schedule", args=["test1"]), follow=True)
            self.assertEqual(response.status_code, 200)
            messages = list(response.context['messages'])
            self.assertEqual(len(messages), 1)
            self.assertEqual(messages[0].tags, "error")
            self.assertEqual(messages[0].message, "Cannot schedule an upgrade without hosts")

    def test_schedule(self):
        self.tree = octofuss.MockTree()
        self.tree.create("upgrades/test1", {"name": "test1", "type": "upgrade", "scheduled": None, "completed": None, "hosts": ["foo"]})
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("upgrade:schedule", args=["test1"]), follow=True)
            self.assertEqual(response.status_code, 200)
            messages = list(response.context['messages'])
            self.assertEqual(len(messages), 0)
            self.assertEqual(self.tree.get("upgrades/test1/scheduled"), True)

    def test_add_host(self):
        self.tree = octofuss.MockTree()
        self.tree.create("upgrades/test1", {"name": "test1", "type": "upgrade", "scheduled": None, "completed": None, "hosts": []})
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("upgrade:add_host", args=["test1"]), data={"host": "host1"}, follow=True)
            self.assertEqual(response.status_code, 200)
            messages = list(response.context['messages'])
            self.assertEqual(len(messages), 0)
            self.assertEqual(self.tree.get("upgrades/test1/hosts"), {"host1": None})

    def test_remove_host(self):
        self.tree = octofuss.MockTree()
        self.tree.create("upgrades/test1", {"name": "test1", "type": "upgrade", "scheduled": None, "completed": None, "hosts": {"host1": None, "host2": None}})
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("upgrade:remove_host", args=["test1"]), data={"host": "host1"}, follow=True)
            self.assertEqual(response.status_code, 200)
            messages = list(response.context['messages'])
            self.assertEqual(len(messages), 0)
            self.assertEqual(self.tree.get("upgrades/test1/hosts"), {"host2": None})

    def test_add_group(self):
        self.tree = octofuss.MockTree()
        self.tree.create("cluster/group1", {"host1": None, "host2": None, "host3": None})
        self.tree.create("upgrades/test1", {"name": "test1", "type": "upgrade", "scheduled": None, "completed": None, "hosts": {}})
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("upgrade:add_group", args=["test1"]), data={"group": "group1"}, follow=True)
            self.assertEqual(response.status_code, 200)
            messages = list(response.context['messages'])
            self.assertEqual(len(messages), 0)
            self.assertEqual(self.tree.get("upgrades/test1/hosts"), {"host1": None, "host2": None, "host3": None})

    def test_remove_group(self):
        self.tree = octofuss.MockTree()
        self.tree.create("cluster/group1", {"host1": None, "host2": None, "host3": None})
        self.tree.create("upgrades/test1", {"name": "test1", "type": "upgrade", "scheduled": None, "completed": None, "hosts": {"host1": None, "host4": None}})
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("upgrade:remove_group", args=["test1"]), data={"group": "group1"}, follow=True)
            self.assertEqual(response.status_code, 200)
            messages = list(response.context['messages'])
            self.assertEqual(len(messages), 0)
            self.assertEqual(self.tree.get("upgrades/test1/hosts"), {"host4": None})

    def test_delete(self):
        self.tree = octofuss.MockTree()
        self.tree.create("upgrades/test1", {"name": "test1", "type": "upgrade", "scheduled": None, "completed": None, "hosts": []})
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("upgrade:delete", args=["test1"]))
            self.assertRedirectMatches(response, reverse("upgrade:list"))
            self.assertFalse(self.tree.has("/upgrades/test1"))

        self.tree.create("upgrades/test1", {"name": "test1", "type": "upgrade", "scheduled": None, "completed": "2016-01-01 00:00:00", "hosts": []})
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("upgrade:delete", args=["test1"]))
            self.assertRedirectMatches(response, reverse("upgrade:list"))
            self.assertFalse(self.tree.has("/upgrades/test1"))

        self.tree.create("upgrades/test1", {"name": "test1", "type": "upgrade", "scheduled": "2016-01-01 00:00:00", "completed": "2016-01-01 00:00:00", "hosts": []})
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("upgrade:delete", args=["test1"]))
            self.assertRedirectMatches(response, reverse("upgrade:list"))
            self.assertFalse(self.tree.has("/upgrades/test1"))

        self.tree = octofuss.MockTree()
        self.tree.create("upgrades/test1", {"name": "test1", "type": "upgrade", "scheduled": "2016-01-01 00:00:00", "completed": None, "hosts": []})
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("upgrade:delete", args=["test1"]))
            self.assertRedirectMatches(response, reverse("upgrade:edit", args=["test1"]))
            self.assertTrue(self.tree.has("/upgrades/test1"))

    def test_update(self):
        self.tree = octofuss.MockTree()
        self.tree.create("upgrades/test1", {"name": "test1", "type": "upgrade", "scheduled": None, "completed": None, "hosts": []})
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("upgrade:update", args=["test1"]), data={"type": "dist-upgrade"}, follow=True)
            self.assertEquals(response.status_code, 200)
            self.assertEquals(self.tree.get("/upgrades/test1/type"), "dist-upgrade")

            response = self.client.post(reverse("upgrade:update", args=["test1"]), data={"type": "upgrade"}, follow=True)
            self.assertEquals(response.status_code, 200)
            self.assertEquals(self.tree.get("/upgrades/test1/type"), "upgrade")

            response = self.client.post(reverse("upgrade:update", args=["test1"]), data={"type": "error"}, follow=True)
            self.assertEquals(response.status_code, 200)
            self.assertEquals(self.tree.get("/upgrades/test1/type"), "upgrade")
            messages = list(response.context['messages'])
            self.assertEqual(len(messages), 1)
            self.assertEqual(messages[0].tags, "error")
            self.assertEqual(messages[0].message, "error is not a valid type for upgrade test1")

    def test_complete(self):
        self.tree = octofuss.MockTree()
        self.tree.create("computers", { x: None for x in "foo bar baz".split() })
        self.tree.create("cluster", { x: None for x in "test1 test2 example".split() })
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("upgrade:complete", args=["hosts"]), data={"name": "ba"})
            self.assertEquals(response.status_code, 200)
            data = json.loads(response.content.decode("utf-8"))
            self.assertCountEqual(data["names"], ["bar", "baz"])

            response = self.client.get(reverse("upgrade:complete", args=["groups"]), data={"name": "te"})
            self.assertEquals(response.status_code, 200)
            data = json.loads(response.content.decode("utf-8"))
            self.assertCountEqual(data["names"], ["test1", "test2"])

            response = self.client.get(reverse("upgrade:complete", args=["groups"]), data={"name": "no"})
            self.assertEquals(response.status_code, 200)
            data = json.loads(response.content.decode("utf-8"))
            self.assertCountEqual(data["names"], [])

            response = self.client.get(reverse("upgrade:complete", args=["groups"]), data={"name": ""})
            self.assertEquals(response.status_code, 200)
            data = json.loads(response.content.decode("utf-8"))
            self.assertCountEqual(data["names"], ["test1", "test2", "example"])

            response = self.client.get(reverse("upgrade:complete", args=["people"]), data={"name": ""})
            self.assertEquals(response.status_code, 404)
