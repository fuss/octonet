# coding: utf-8
from django.apps import AppConfig
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from octonet.mixins import OctonetAppMixin

class Upgrade(OctonetAppMixin, AppConfig):
    name = 'upgrade'
    group = _("Hosts")
    verbose_name = _("Upgrade Manager")
    description = _("Manage upgrade of machines")
    main_url = reverse_lazy("upgrade:list")
    font_awesome_class = "fa-download"
    octofussd_url = "/upgrades"

