from django.urls import re_path
from . import views

app_name = "upgrade"
urlpatterns = [
    re_path(r"^$", views.List.as_view(), name="list"),
    re_path(r"^edit/(?P<name>[^/]+)$", views.Edit.as_view(), name="edit"),
    re_path(r"^schedule/(?P<name>[^/]+)$", views.Schedule.as_view(), name="schedule"),
    re_path(r"^delete/(?P<name>[^/]+)$", views.Delete.as_view(), name="delete"),
    re_path(r"^update/(?P<name>[^/]+)$", views.Update.as_view(), name="update"),
    re_path(r"^complete/(?P<domain>[^/]+)$", views.Complete.as_view(), name="complete"),
    re_path(r"^hosts/add/(?P<name>[^/]+)$", views.AddHost.as_view(), name="add_host"),
    re_path(r"^hosts/remove/(?P<name>[^/]+)$", views.RemoveHost.as_view(), name="remove_host"),
    re_path(r"^hosts/addgroup/(?P<name>[^/]+)$", views.AddGroup.as_view(), name="add_group"),
    re_path(r"^hosts/removegroup/(?P<name>[^/]+)$", views.RemoveGroup.as_view(), name="remove_group"),
]
