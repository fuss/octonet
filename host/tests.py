# coding: utf-8
import json
from unittest import skipIf
from django import http
from django.urls import reverse
from django.test import SimpleTestCase
import octofuss
from octonet import backend
from octonet.unittest import TestBase


class LoginMixin():
    def setUp(self):
        # Login
        self.client.post(
            reverse("login"),
            data={
                "username": "root",
                "password": "root",
                "server_url": "http://localhost:13400/conf/",
            }
        )


class TestHost(LoginMixin, TestBase, SimpleTestCase):
    """
    General views tests
    """
    def setUp(self):
        self.tree = octofuss.MockTree()
        self.host_data = {
            'hostname': 'antani', 'logged_user': 'enrico', 'screen_locked': True,
            'net_disabled': False, 'isup': True, 'last_seen': 'None',
        }
        super().setUp()
        # Make it work after b0f11897be396e03fbf515e787ebdba4cddb9c7e (found thanks to git bisect)
        session = self.client.session
        session['apps'] = ['host']
        session.save()

    def test_GET_home_redirects_to_list(self):
        response = self.client.get(reverse("host:home"))
        self.assertRedirectMatches(response, reverse("host:list"))

    def test_GET_list_gives_200(self):
        response = self.client.get(reverse("host:list"))
        self.assertEqual(response.status_code, 200)

    def test_GET_host_edit_gives_200(self):
        tree = octofuss.MockTree()
        tree.create("computers/antani", self.host_data)
        tree.create("cluster/antanigroup/antani", self.host_data)
        with self.settings(TEST_USER=backend.TestUser(tree)):
            response = self.client.get(reverse("host:host_edit", kwargs={'name': 'antani'}))
            self.assertEqual(response.status_code, 200)

    def test_GET_group_detail_gives_200(self):
        self.tree.create("cluster/antanigroup/antani", self.host_data)
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("host:group_detail", kwargs={'group': 'antanigroup'}))
            self.assertEqual(response.status_code, 200)

    def test_GET_group_create_gives_200(self):
        self.tree.create("cluster/antanigroup/antani", self.host_data)
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("host:group_create"))
            self.assertEqual(response.status_code, 200)

    def test_empty_POST_group_create_gives_200(self):
        self.tree.create("cluster/antanigroup/antani", self.host_data)
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("host:group_create"), {})
            self.assertEqual(response.status_code, 200)

    def test_POST_group_create_gives_302(self):
        self.tree.create("cluster/antanigroup/antani", self.host_data)
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("host:group_create"), {'group': 'another_antani'})
            self.assertEqual(response.status_code, 302)

    def test_non_AJAX_GET_actions_for_cluster_gives_400(self):
        self.tree.create("cluster/antanigroup/:actions:/shutdown", {'type': 'nudge', 'label': 'Shutdown now'})
        self.tree.create("cluster/antanigroup/antani", self.host_data)
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("host:actions_ajax", kwargs={'type': 'group', 'name': 'antanigroup'}))
            self.assertEqual(response.status_code, 400)

    def test_AJAX_GET_actions_for_cluster_gives_200(self):
        self.tree.create("cluster/antanigroup/:actions:/shutdown", {'type': 'nudge', 'label': 'Shutdown now'})
        self.tree.create("cluster/antanigroup/antani", self.host_data)
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(
                reverse("host:actions_ajax", kwargs={'type': 'group', 'name': 'antanigroup'}),
                HTTP_X_REQUESTED_WITH='XMLHttpRequest',
            )
            self.assertEqual(response.status_code, 200)

    def test_non_AJAX_GET_actions_for_host_gives_400(self):
        self.tree.create("cluster/antanigroup/antani", self.host_data)
        self.tree.create("computers/antani/:actions:/shutdown", {'type': 'nudge', 'label': 'Shutdown now'})
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("host:actions_ajax", kwargs={'type': 'host', 'name': 'antani'}))
            self.assertEqual(response.status_code, 400)

    def test_AJAX_GET_actions_for_host_gives_200(self):
        self.tree.create("cluster/antanigroup/antani", self.host_data)
        self.tree.create("computers/antani/:actions:/shutdown", {'type': 'nudge', 'label': 'Shutdown now'})
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(
                reverse("host:actions_ajax", kwargs={'type': 'host', 'name': 'antani'}),
                HTTP_X_REQUESTED_WITH='XMLHttpRequest',
            )
            self.assertEqual(response.status_code, 200)

    def test_non_AJAX_GET_action_for_cluster_gives_405(self):
        self.tree.create("cluster/antanigroup/:actions:/shutdown", {'type': 'nudge', 'label': 'Shutdown now'})
        self.tree.create("cluster/antanigroup/antani", self.host_data)
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("host:action_ajax", kwargs={'type': 'group', 'name': 'antanigroup', 'action': 'shutdown'}))
            self.assertEqual(response.status_code, 405)

    def test_AJAX_GET_action_for_cluster_gives_405(self):
        self.tree.create("cluster/antanigroup/:actions:/shutdown", {'type': 'nudge', 'label': 'Shutdown now'})
        self.tree.create("cluster/antanigroup/antani", self.host_data)
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(
                reverse("host:action_ajax", kwargs={'type': 'group', 'name': 'antanigroup', 'action': 'shutdown'}),
                HTTP_X_REQUESTED_WITH='XMLHttpRequest',
            )
            self.assertEqual(response.status_code, 405)

    def test_non_AJAX_POST_action_for_cluster_gives_400(self):
        self.tree.create("cluster/antanigroup/:actions:/shutdown", {'type': 'nudge', 'label': 'Shutdown now'})
        self.tree.create("cluster/antanigroup/antani", self.host_data)
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(
                reverse("host:action_ajax", kwargs={'type': 'group', 'name': 'antanigroup', 'action': 'shutdown'}),
                {'value': ''},
            )
            self.assertEqual(response.status_code, 400)

    def test_AJAX_POST_action_for_cluster_gives_500_is_action_does_not_exist(self):
        self.tree.create("cluster/antanigroup/:actions:/shutdown", {'type': 'nudge', 'label': 'Shutdown now'})
        self.tree.create("cluster/antanigroup/antani", self.host_data)
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(
                reverse("host:action_ajax", kwargs={'type': 'group', 'name': 'antanigroup', 'action': 'idontexist'}),
                HTTP_X_REQUESTED_WITH='XMLHttpRequest',
            )
            self.assertEqual(response.status_code, 500)

    def test_AJAX_POST_action_for_cluster_gives_200_if_the_action_is_good(self):
        self.tree.create("cluster/antanigroup/:actions:/shutdown", {'type': 'nudge', 'label': 'Shutdown now'})
        self.tree.create("cluster/antanigroup/antani", self.host_data)
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(
                reverse("host:action_ajax", kwargs={'type': 'group', 'name': 'antanigroup', 'action': 'shutdown'}),
                HTTP_X_REQUESTED_WITH='XMLHttpRequest',
            )
            self.assertEqual(response.status_code, 200)

    def test_non_AJAX_GET_action_for_host_gives_405(self):
        self.tree.create("cluster/antanigroup/antani", self.host_data)
        self.tree.create("computers/antani/:actions:/shutdown", {'type': 'nudge', 'label': 'Shutdown now'})
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("host:action_ajax", kwargs={'type': 'host', 'name': 'antani', 'action': 'shutdown'}))
            self.assertEqual(response.status_code, 405)

    def test_AJAX_GET_action_for_host_gives_405(self):
        self.tree.create("cluster/antanigroup/antani", self.host_data)
        self.tree.create("computers/antani/:actions:/shutdown", {'type': 'nudge', 'label': 'Shutdown now'})
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(
                reverse("host:action_ajax", kwargs={'type': 'host', 'name': 'antani', 'action': 'shutdown'}),
                HTTP_X_REQUESTED_WITH='XMLHttpRequest',
            )
            self.assertEqual(response.status_code, 405)

    def test_non_AJAX_POST_action_for_host_gives_400(self):
        self.tree.create("cluster/antanigroup/antani", self.host_data)
        self.tree.create("computers/antani/:actions:/shutdown", {'type': 'nudge', 'label': 'Shutdown now'})
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(
                reverse("host:action_ajax", kwargs={'type': 'host', 'name': 'antani', 'action': 'shutdown'}),
                {'value': ''},
            )
            self.assertEqual(response.status_code, 400)

    def test_AJAX_POST_action_for_host_gives_500_is_action_does_not_exist(self):
        self.tree.create("cluster/antanigroup/antani", self.host_data)
        self.tree.create("computers/antani/:actions:/shutdown", {'type': 'nudge', 'label': 'Shutdown now'})
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(
                reverse("host:action_ajax", kwargs={'type': 'host', 'name': 'antani', 'action': 'idontexist'}),
                HTTP_X_REQUESTED_WITH='XMLHttpRequest',
            )
            self.assertEqual(response.status_code, 500)

    def test_AJAX_POST_action_for_host_gives_200_if_the_action_is_good(self):
        self.tree.create("cluster/antanigroup/antani", self.host_data)
        self.tree.create("computers/antani/:actions:/shutdown", {'type': 'nudge', 'label': 'Shutdown now'})
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(
                reverse("host:action_ajax", kwargs={'type': 'host', 'name': 'antani', 'action': 'shutdown'}),
                HTTP_X_REQUESTED_WITH='XMLHttpRequest',
            )
            self.assertEqual(response.status_code, 200)

    def test_non_AJAX_GET_action_completion_for_host_gives_400(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(reverse("host:action_completion_ajax"))
            self.assertEqual(response.status_code, 400)

    def test_AJAX_GET_action_completion_for_host_gives_200(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.get(
                reverse("host:action_completion_ajax"),
                HTTP_X_REQUESTED_WITH='XMLHttpRequest',
            )
            self.assertEqual(response.status_code, 200)

    def test_non_AJAX_POST_action_completion_for_host_gives_405(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(reverse("host:action_completion_ajax"))
            self.assertEqual(response.status_code, 405)

    def test_AJAX_POST_action_completion_for_host_gives_405(self):
        with self.settings(TEST_USER=backend.TestUser(self.tree)):
            response = self.client.post(
                reverse("host:action_completion_ajax"),
                HTTP_X_REQUESTED_WITH='XMLHttpRequest',
            )
            self.assertEqual(response.status_code, 405)


class TestHostActionsAJAXMixin():
    """
    Tests for actions_ajax (please note that's action*s* with a 's'), that gives the action list.
    """
    def test_GET_actions_ajax_gives_400(self):
        tree = octofuss.MockTree()
        tree.create("%s/antani/:actions:/shutdown" % self.root_string, {'type': 'nudge', 'label': 'Shutdown now'})
        with self.settings(TEST_USER=backend.TestUser(tree)):
            response = self.client.get(
                reverse(
                    "host:actions_ajax",
                    kwargs={'type': self.type_string, 'name': 'antani'},
                )
            )
            self.assertEqual(response.status_code, 400)

    def test_AJAX_GET_actions_ajax_gives_200(self):
        tree = octofuss.MockTree()
        tree.create("%s/antani/:actions:/shutdown" % self.root_string, {'type': 'nudge', 'label': 'Shutdown now'})
        with self.settings(TEST_USER=backend.TestUser(tree)):
            response = self.client.get(
                reverse(
                    "host:actions_ajax",
                    kwargs={'type': self.type_string, 'name': 'antani'},
                ),
                HTTP_X_REQUESTED_WITH='XMLHttpRequest',
            )
            self.assertEqual(response.status_code, 200)

    def test_AJAX_GET_actions_ajax_gives_JSON_data_with_one_action(self):
        tree = octofuss.MockTree()
        tree.create("%s/antani/:actions:/shutdown" % self.root_string, {'type': 'nudge', 'label': 'Shutdown now'})
        with self.settings(TEST_USER=backend.TestUser(tree)):
            response = self.client.get(
                reverse(
                    "host:actions_ajax",
                    kwargs={'type': self.type_string, 'name': 'antani'},
                ),
                HTTP_X_REQUESTED_WITH='XMLHttpRequest',
            )
            # The check is *a bit* "laborioso"
            # The response is a JsonResponse...
            self.assertIsInstance(response, http.JsonResponse)
            # ...which content is a bytestring...
            content = json.loads(response.content.decode("utf-8"))
            # Check that there is a 'actions' key in the content dict
            self.assertIn('actions', content)
            # ...and that it's length is 1
            self.assertEqual(len(content['actions']), 1)
            # Check the first (and only, in this case) item, but as it is a dict, do it regardless
            # of the key order, using assertCountEqual
            self.assertCountEqual(
                content['actions'][0],
                {'action': 'shutdown', 'type': 'nudge', 'label': 'Shutdown now'},
            )
            # The check is *a bit* "laborioso"
            # The response is a JsonResponse...
            self.assertIsInstance(response, http.JsonResponse)
            # ...which content is a bytestring...
            content = json.loads(response.content.decode("utf-8"))
            # Check that there is a 'actions' key in the content dict
            self.assertIn('actions', content)
            # ...and that it's length is 1
            self.assertEqual(len(content['actions']), 1)
            # Check the first (and only, in this case) item, but as it is a dict, do it regardless
            # of the key order, using assertCountEqual
            self.assertCountEqual(
                content['actions'],
                [{'action': 'shutdown', 'type': 'nudge', 'label': 'Shutdown now'}],
            )

    def test_AJAX_GET_actions_ajax_gives_JSON_data_with_two_actions(self):
        tree = octofuss.MockTree()
        tree.create(
            "%s/antani/:actions:/shutdown" % self.root_string,
            {'type': 'nudge', 'label': 'Shutdown now'},
        )
        tree.create(
            "%s/antani/:actions:/installpkg" % self.root_string,
            {'type': 'freeform', 'text': 'Package to install', 'label': 'Install software'},
        )
        with self.settings(TEST_USER=backend.TestUser(tree)):
            response = self.client.get(
                reverse(
                    "host:actions_ajax",
                    kwargs={'type': self.type_string, 'name': 'antani'},
                ),
                HTTP_X_REQUESTED_WITH='XMLHttpRequest',
            )
            # The check is *a bit* "laborioso"
            # The response is a JsonResponse...
            self.assertIsInstance(response, http.JsonResponse)
            # ...which content is a bytestring...
            content = json.loads(response.content.decode("utf-8"))
            # Check that there is a 'actions' key in the content dict
            self.assertIn('actions', content)
            # ...and that it's length is 2
            self.assertEqual(len(content['actions']), 2)
            # Check the first (and only, in this case) item, but as it is a dict, do it regardless
            # of the key order, using assertCountEqual
            self.assertCountEqual(
                content['actions'],
                [
                    {'action': 'shutdown', 'type': 'nudge', 'label': 'Shutdown now'},
                    {'action': 'installpkg', 'type': 'freeform', 'text': 'Package to install', 'label': 'Install software'},
                ],
            )


class TestHostActionAJAXMixin():
    """
    Tests for action_ajax, that *does* an action.
    """
    def test_GET_action_ajax_gives_405(self):
        tree = octofuss.MockTree()
        tree.create("%s/antani/:actions:/shutdown" % self.root_string, {'type': 'nudge', 'label': 'Shutdown now'})
        with self.settings(TEST_USER=backend.TestUser(tree)):
            response = self.client.get(
                reverse(
                    "host:action_ajax",
                    kwargs={'action': 'shutdown', 'type': self.type_string, 'name': 'antani'},
                )
            )
            self.assertEqual(response.status_code, 405)

    def test_AJAX_GET_action_ajax_gives_405(self):
        tree = octofuss.MockTree()
        tree.create("%s/antani/:actions:/shutdown" % self.root_string, {'type': 'nudge', 'label': 'Shutdown now'})
        with self.settings(TEST_USER=backend.TestUser(tree)):
            response = self.client.get(
                reverse(
                    "host:action_ajax",
                    kwargs={'action': 'shutdown', 'type': self.type_string, 'name': 'antani'},
                ),
                HTTP_X_REQUESTED_WITH='XMLHttpRequest',
            )
            self.assertEqual(response.status_code, 405)

    def test_POST_action_ajax_gives_400(self):
        tree = octofuss.MockTree()
        tree.create("%s/antani/:actions:/shutdown" % self.root_string, {'type': 'nudge', 'label': 'Shutdown now'})
        with self.settings(TEST_USER=backend.TestUser(tree)):
            response = self.client.post(
                reverse(
                    "host:action_ajax",
                    kwargs={'action': 'shutdown', 'type': self.type_string, 'name': 'antani'},
                )
            )
            self.assertEqual(response.status_code, 400)

    def test_POST_action_ajax_gives_400_if_action_does_not_exist(self):
        tree = octofuss.MockTree()
        tree.create("%s/antani/:actions:/shutdown" % self.root_string, {'type': 'nudge', 'label': 'Shutdown now'})
        with self.settings(TEST_USER=backend.TestUser(tree)):
            response = self.client.post(
                reverse(
                    "host:action_ajax",
                    kwargs={'action': 'shutdow', 'type': self.type_string, 'name': 'antani'},
                ),
            )
            self.assertEqual(response.status_code, 400)

    def test_AJAX_POST_action_ajax_gives_200(self):
        tree = octofuss.MockTree()
        tree.create("%s/antani/:actions:/shutdown" % self.root_string, {'type': 'nudge', 'label': 'Shutdown now'})
        with self.settings(TEST_USER=backend.TestUser(tree)):
            response = self.client.post(
                reverse(
                    "host:action_ajax",
                    kwargs={'action': 'shutdown', 'type': self.type_string, 'name': 'antani'},
                ),
                HTTP_X_REQUESTED_WITH='XMLHttpRequest',
            )
            self.assertEqual(response.status_code, 200)

    def test_AJAX_POST_action_ajax_gives_500_if_action_does_not_exist(self):
        tree = octofuss.MockTree()
        tree.create("%s/antani/:actions:/shutdown" % self.root_string, {'type': 'nudge', 'label': 'Shutdown now'})
        with self.settings(TEST_USER=backend.TestUser(tree)):
            response = self.client.post(
                reverse(
                    "host:action_ajax",
                    kwargs={'action': 'shutdow', 'type': self.type_string, 'name': 'antani'},
                ),
                HTTP_X_REQUESTED_WITH='XMLHttpRequest',
            )
            self.assertEqual(response.status_code, 500)


###################
# TESTS FOR HOSTS #
###################
class HostViewsMixin():
    def setUp(self):
        self.root_string = 'computers'
        self.type_string = 'host'


class TestHostActionsAJAXForHosts(HostViewsMixin, TestHostActionsAJAXMixin, LoginMixin, TestBase, SimpleTestCase):
    def setUp(self):
        super().setUp()
        # Make it work after b0f11897be396e03fbf515e787ebdba4cddb9c7e (found thanks to git bisect)
        session = self.client.session
        session['apps'] = ['host']
        session.save()


class TestHostActionAJAXForHosts(HostViewsMixin, TestHostActionAJAXMixin, LoginMixin, TestBase, SimpleTestCase):
    def setUp(self):
        super().setUp()
        # Make it work after b0f11897be396e03fbf515e787ebdba4cddb9c7e (found thanks to git bisect)
        session = self.client.session
        session['apps'] = ['host']
        session.save()


######################
# TESTS FOR CLUSTERS #
######################
class ClusterViewsMixin():
    def setUp(self):
        self.root_string = 'cluster'
        self.type_string = 'group'


class TestHostActionsAJAXForClusters(ClusterViewsMixin, TestHostActionsAJAXMixin, LoginMixin, TestBase, SimpleTestCase):
    def setUp(self):
        super().setUp()
        # Make it work after b0f11897be396e03fbf515e787ebdba4cddb9c7e (found thanks to git bisect)
        session = self.client.session
        session['apps'] = ['host']
        session.save()


class TestHostActionAJAXForClusters(ClusterViewsMixin, TestHostActionAJAXMixin, LoginMixin, TestBase, SimpleTestCase):
    def setUp(self):
        super().setUp()
        # Make it work after b0f11897be396e03fbf515e787ebdba4cddb9c7e (found thanks to git bisect)
        session = self.client.session
        session['apps'] = ['host']
        session.save()

    def test_AJAX_POST_create_new_computer_gives_200(self):
        tree = octofuss.MockTree()
        tree.create(
            "%s/antani/:actions:/create" % self.root_string,
            {'type': 'freeform', 'label': 'Create new computer', 'text': 'Name of the new computer', 'order': 9},
        )
        with self.settings(TEST_USER=backend.TestUser(tree)):
            response = self.client.post(
                reverse(
                    "host:action_ajax",
                    kwargs={'action': 'create', 'type': 'group', 'name': 'antani'},
                ),
                {'value': 'hostname'},
                HTTP_X_REQUESTED_WITH='XMLHttpRequest',
            )
            self.assertEqual(response.status_code, 200)

    @skipIf(True, "Finish and implement this test when the situation is clearer")
    def test_AJAX_POST_create_new_computer_creates_item(self):
        self.fail("Finish and implement this test when the situation is clearer")
        tree = octofuss.MockTree()
        tree.create(
            "%s/antani/:actions:/create" % self.root_string,
            {'type': 'freeform', 'label': 'Create new computer', 'text': 'Name of the new computer', 'order': 9},
        )
        with self.settings(TEST_USER=backend.TestUser(tree)):
            response = self.client.post(
                reverse(
                    "host:action_ajax",
                    kwargs={'action': 'create', 'type': 'group', 'name': 'antani'},
                ),
                {'value': 'hostname'},
                HTTP_X_REQUESTED_WITH='XMLHttpRequest',
            )
