# coding: utf-8
from django.apps import AppConfig
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from octonet.mixins import OctonetAppMixin

class Host(OctonetAppMixin, AppConfig):
    name = 'host'
    verbose_name = _("Managed Hosts")
    description = _("Hosts")
    group = _("Hosts")
    main_url = reverse_lazy("host:list")
    font_awesome_class = "fa-desktop"
    octofussd_url = "/computers"
