# coding: utf-8
from django import forms
from django.utils.translation import gettext_lazy as _
from octonet.forms import FormControlClassMixin


# refs #150
class GroupCreateForm(FormControlClassMixin, forms.Form):
    group = forms.CharField(max_length=32, label=_("Cluster name"))
