# coding: utf-8
import os
from urllib.parse import quote_plus, unquote_plus
from django import http
from django.contrib import messages
from django.urls import reverse
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.utils.translation import gettext_lazy as _
from django.views.decorators.http import require_http_methods
from django.views.generic import TemplateView
from django.views.generic.base import View
from django.views.generic.edit import FormView
import octofuss
from octonet.mixins import OctonetMixin
import host.forms as hforms


class HostMixin():
    def dispatch(self, *args, **kwargs):
        # refs #149 Use self.root_tree
        self.clusters = self.root_tree.list("cluster")
        groups = [
            ('', '', _("All computers")),
            (':avahi:', 'avahi', _("New computers")),
        ]
        # refs #12379 #455
        for cluster in sorted([x for x in self.clusters], key=str.lower):
            groups.append((cluster, cluster, cluster))
        self.groups = groups
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['groups'] = self.groups
        return context


class List(OctonetMixin, HostMixin, TemplateView):
    template_name = "host/list.html"

    def load_objects(self):
        super().load_objects()
        self.tree = self.octofuss_tree("/")
        # FIXME: Deduplicate association between slug and strings
        group = self.kwargs.get("group", None)
        if group in [None, ""]:
            self.group_type = "all"
            self.group = (':all:', 'all', _("All computers"))
            self.hosts = octofuss.Subtree(self.tree, 'computers')
        elif group == ":avahi:":
            self.group_type = "avahigroup"
            self.group = (':avahi:', 'avahi', _("New computers"))
            self.hosts = octofuss.Subtree(self.tree, "avahicomputers")
        else:
            self.group_type = "group"
            self.group = (group, group, group)
            self.hosts = octofuss.Subtree(self.tree, os.path.join('cluster', group))
        # refs #154 #153 Workaround because it is possible to create a host with an empty name
        # that gives a "/" name here, that makes the url regex fail. Once fixed #153 this bug shouldn't
        # happen anymore, hopefully
        self.all_info = [(x, self.hosts.get(x)) for x in self.hosts.list("") if x not in [":actions:", "/"]]
        self.hosts = [x[0] for x in self.all_info]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['group_type'] = self.group_type
        context['group'] = self.group
        context['hosts'] = self.hosts
        context['all_info'] = self.all_info
        return context


class HostEdit(OctonetMixin, HostMixin, TemplateView):
    template_name = "host/host_edit.html"

    def load_objects(self):
        super().load_objects()
        name = self.kwargs.get("name")
        self.tree = self.octofuss_tree("/")
        # Just to avoid exceptions (View has no host or host_type)
        self.host = None
        self.host_type = None
        if self.tree.lhas(["computers", name]):
            self.host = self.tree.lget(["computers", name])
            self.host_type = "host"
        else:
            # If it is not a normal host, it could be an autodetected host
            self.host = self.tree.lget(["avahicomputers", name])
            self.host_type = "avahihost"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['host_type'] = self.host_type
        context['host'] = self.host
        return context


# refs #150
class GroupCreate(OctonetMixin, HostMixin, FormView):
    template_name = "host/group_create.html"
    form_class = hforms.GroupCreateForm

    def form_valid(self, form):
        self.group = quote_plus(form.cleaned_data['group'])
        if self.root_tree.lhas(["cluster", self.group]):
            messages.error(self.request, _("There is already a cluster named %s") % self.group)
            return redirect("host:group_create")
        self.root_tree.lcreate(["cluster", self.group])
        messages.success(self.request, _("Cluster %s created") % unquote_plus(self.group))
        return super().form_valid(form)

    def form_invalid(self, form):
        self.group = None
        messages.error(self.request, _("Cluster not created"))
        return super().form_invalid(form)

    def get_success_url(self):
        group = getattr(self, 'group', None)
        if group:
            return reverse("host:group_detail", kwargs={'group': group})
        else:
            return reverse("host:list")


class GroupDetail(OctonetMixin, HostMixin, TemplateView):
    template_name = "host/group_detail.html"

    def dispatch(self, *args, **kwargs):
        if self.kwargs.get("group", "") == "all":
            messages.error(self.request, _("This is not an editable group"))
            return redirect('host:list')
        return super().dispatch(*args, **kwargs)

    def load_objects(self):
        super().load_objects()
        group = self.kwargs.get("group")
        tree = self.octofuss_tree("/")
        if group == "all":
            self.group_type = "all"
            # FIXME: Deduplicate association between slug and strings
            self.group = ('', 'all', _("All computers"))
            self.tree = octofuss.Subtree(tree, "computers")
        elif group == "avahi":
            self.group_type = "avahigroup"
            # FIXME: Deduplicate association between slug and strings
            self.group = (':avahi:', 'avahi', _("New computers"))
            self.tree = octofuss.Subtree(tree, "avahicomputers")
        else:
            self.group_type = "group"
            # Old code: group = quote_plus(unquote_plus(group))
            self.group = (group, group, group)
            self.tree = octofuss.Subtree(tree, "cluster/%s" % group)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['group_type'] = self.group_type
        context['group'] = self.group
        hosts = [self.tree.get(x) for x in self.tree.list("") if x != ":actions:"]
        context['hosts_count'] = len(hosts)
        context['hosts_with_logged_user'] = len([x for x in hosts if x['logged_user']])
        context['hosts_up'] = len([x for x in hosts if x['isup']])
        context['hosts_screen_locked'] = len([x for x in hosts if x['screen_locked']])
        return context


class EmptyGroup(OctonetMixin, HostMixin, View):
    def get(self, request, group, **kwargs):
        if group == "all":
            messages.error(self.request, _("This is not an editable group"))
            return redirect('host:list')
        super().load_objects()
        group = self.kwargs.get("group")
        tree = self.octofuss_tree("/")
        self.group_type = "group"
        # Old code: group = quote_plus(unquote_plus(group))
        self.group = (group, group, group)
        clusterTree = octofuss.Subtree(tree, "cluster/%s" % group)
        hosts = [clusterTree.get(x).get("hostname") for x in clusterTree.list("") if x != ":actions:"]
        for i in hosts:
            octofuss.Subtree(tree, "computers/%s/:actions:/delcluster" % i).set("", group)
        messages.success(request, _("Cluster emptied."))
        return redirect('host:list')


class ActionsMixin():
    def get_path(self, target_type, target_name):
        if target_type == "host":
            return "/computers/" + target_name + "/:actions:"
        elif target_type == "group":
            return "/cluster/" + target_name + "/:actions:"
        elif target_type == "all":
            return "/computers/:actions:"
        elif target_type == "avahihost":
            return "/avahicomputers/" + target_name + "/:actions:"
        elif target_type == "avahigroup":
            return "/avahicomputers/:actions:"
        else:
            raise ValueError("Unknown action type: " + target_type)


class ActionAJAX(OctonetMixin, ActionsMixin, View):
    """
    This is the AJAX view that *does* an action.
    """
    @method_decorator(require_http_methods(["POST"]))
    def dispatch(self, *args, **kwargs):
        if not self.request.is_ajax():
            return http.HttpResponseBadRequest(_("Bad request"))
        return super().dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        target_type = kwargs.get("type")
        target_name = kwargs.get("name")
        action = kwargs.get("action")
        value = request.POST.get("value", "")
        path = self.get_path(target_type, target_name) + "/" + action
        tree = self.octofuss_tree(path)
        # Do it
        response_data = {}
        try:
            tree.set("", value)
            response_data['message'] = str(_("Action: OK"))
            response_data['message_class'] = "success"
            # HTTP status OK
            response_data['status_code'] = 200
        except Exception as e:
            print(repr(e))
            response_data['message'] = str(_("Action: not OK "))
            response_data['message_class'] = "danger"
            # Generic server error
            response_data['status_code'] = 500
        response_data['data'] = tree.get("")
        # refs 174
        # If removing a host from its last cluster, redirect to host list to avoid an empty page
        # with an ajax call with error for a non existing host.
        if target_type == "host" and action == "delcluster":
            _tree = self.octofuss_tree("/")
            if _tree.lget(["computers", target_name]) is None:
                response_data['redirect_url'] = reverse("host:list")
        response = http.JsonResponse(response_data, status=response_data['status_code'])
        return response


class ActionsAJAX(OctonetMixin, ActionsMixin, View):
    """
    This is the AJAX view that gives the actions available on an object.
    """
    @method_decorator(require_http_methods(["GET"]))
    def dispatch(self, *args, **kwargs):
        if not self.request.is_ajax():
            return http.HttpResponseBadRequest(_("Bad request"))
        return super().dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        target_type = kwargs.get("type")
        target_name = kwargs.get("name")
        self.path = self.get_path(target_type, target_name)
        tree = self.octofuss_tree(self.path)
        data = tree.lget([])
        data_list = self.get_sorted_actions(data)
        response = http.JsonResponse({'actions': data_list})
        return response

    def get_sorted_actions(self, actions_from_octofussd):
        """
        Octofussd returns the actions (using `tree.lget([])`) in this format (as a dict of dicts):
        {
            'lockdesktop': {'type': 'nudge', 'order': 6, 'label': 'Lock screen'},
            'create': {'text': 'Name of the new computer', 'type': 'freeform',
                       'order': 9, 'label': 'Create new computer'},
            'shutdown': {'type': 'nudge', 'order': 1, 'label': 'Shutdown now'},
            'installpkg': {'text': 'Package to install', 'type': 'freeform', 'order': 3, 'label': 'Install software'},
            'disablenet': {'type': 'nudge', 'order': 5, 'label': 'Disable internet'},
            'unlockdesktop': {'type': 'nudge', 'order': 7, 'label': 'Unlock screen'},
            'message': {'text': 'Message to send', 'type': 'freeform', 'order': 2, 'label': 'Send a message'},
            'wakeup': {'type': 'nudge', 'order': 8, 'label': 'Power up'},
        }
        These actions are to be sorted by the value of order. This is easier if it's done in the view
        rather than in the template, or via javascript. So I transform the dict of dicts in a sorted
        list of dicts (obviously I have to add another key to the single dict, which is the action
        itself (before it was the 'first level' key of the dict)):
        [
            {'order': 1, 'action': 'shutdown', 'label': 'Shutdown now', 'type': 'nudge'},
            {'order': 2, 'text': 'Message to send', 'action': 'message', 'label': 'Send a message', 'type': 'freeform'},
            {'order': 3, 'text': 'Package to install', 'action': 'installpkg',
             'label': 'Install software', 'type': 'freeform'},
            {'order': 5, 'action': 'disablenet', 'label': 'Disable internet', 'type': 'nudge'},
            {'order': 6, 'action': 'lockdesktop', 'label': 'Lock screen', 'type': 'nudge'},
            {'order': 7, 'action': 'unlockdesktop', 'label': 'Unlock screen', 'type': 'nudge'},
            {'order': 8, 'action': 'wakeup', 'label': 'Power up', 'type': 'nudge'},
            {'order': 9, 'text': 'Name of the new computer', 'action':
             'create', 'label': 'Create new computer', 'type': 'freeform'},
        ]
        """
        data_list = []
        for action in actions_from_octofussd:
            item = {'action': action}
            item.update(actions_from_octofussd[action])
            if item['type'] == "completion":
                item['base_path'] = self.path
            data_list.append(item)
        # Cluster actions have 'order', Host actions don't
        if 'order' in data_list[0]:
            return sorted(data_list, key=lambda y: y['order'])
        else:
            # refs #157
            # TODO: Maybe it's worth to add an 'order' key for host actions also, in the octofussd backend
            return sorted(data_list, key=lambda x: x.get('action') in ['addcluster', 'delcluster'])


class ActionCompletionAJAX(OctonetMixin, ActionsMixin, View):
    """
    This is the AJAX view that gives the completion options for a given action.
    """
    @method_decorator(require_http_methods(["GET"]))
    def dispatch(self, *args, **kwargs):
        if not self.request.is_ajax():
            return http.HttpResponseBadRequest(_("Bad request"))
        return super().dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        base_path = request.GET.get("base_path", "")
        action = request.GET.get("action", "")
        path = request.GET.get("path", "")
        if not (base_path and action and path):
            return http.JsonResponse({})
        complete_path = "/".join([base_path, action, path])
        tree = self.octofuss_tree(complete_path)
        options = tree.get("")
        response = http.JsonResponse({"status": "ok", "options": options})
        return response
