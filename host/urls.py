# coding: utf-8
from django.urls import re_path
from django.views.generic.base import RedirectView
from . import views


app_name = "host"
urlpatterns = [
    re_path(r'^$', RedirectView.as_view(pattern_name='host:list', permanent=True), name='home'),
    re_path(r"^list/(?P<group>[^/]+)?$", views.List.as_view(), name="list"),
    re_path(r"^edit/(?P<name>[^/]+)$", views.HostEdit.as_view(), name="host_edit"),
    re_path(r"^group/detail/(?P<group>[^/]+)$", views.GroupDetail.as_view(), name="group_detail"),
    re_path(r"^group/empty/(?P<group>[^/]+)", views.EmptyGroup.as_view(), name="group_empty"),
    re_path(r"^group/create/$", views.GroupCreate.as_view(), name="group_create"),
    # Can name be empty? For example when type == 'all'?
    re_path(
        r"^actions/(?P<type>(host|group|all|avahihost|avahigroup))/(?P<name>[\w_\-\+\%\.]+)?$",
        views.ActionsAJAX.as_view(),
        name="actions_ajax",
    ),
    # refs #71. Add _type to type kwarg to allow creating a "dummy" url in the template and subsequent
    # fix in actions.js (which has no access to django template language/filters/tags)
    re_path(
        r"^action/(?P<action>[\w_]+)/(?P<type>(host|group|all|avahihost|avahigroup|_type))/(?P<name>.+)$",
        views.ActionAJAX.as_view(),
        name="action_ajax",
    ),
    re_path(r"^action/completion/$", views.ActionCompletionAJAX.as_view(), name="action_completion_ajax"),
]
