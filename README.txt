To build a new package:

* Bump the version *both* in debian/changelog *and* setup.py
* Follow the instructions in the documentation here:
  https://fuss-dev-guide.readthedocs.io/it/latest/pacchetti-e-repository.html
  using ``debian/rules debsrc`` to create the source tarball.
