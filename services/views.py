# -*- coding: utf-8 -*-
# Copyright (c) 2018 Marco Marinello <mmarinello@fuss.bz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import subprocess
from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.views.generic import View
from octonet.mixins import OctonetMixin
from django.contrib import messages
from django.utils.translation import gettext_lazy as _
import octofuss


class List(OctonetMixin, TemplateView):
    template_name = "services/list.html"

    def load_objects(self):
        super().load_objects()
        self.tree = self.octofuss_tree("/")
        services = self.tree.get("/services")
        self.services = []
        for x in sorted(services.keys()):
            this = [x, services[x][0], services[x][1]]
            self.services.append(this)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["services"] = self.services
        return context


class PerformAction(OctonetMixin, View):
    def get_success_url(self):
        referrer = self.request.META.get("HTTP_REFERER", "")
        if referrer and "users/user/list" in referrer:
            return "users:list"
        if not referrer:
            from_variable = self.request.GET.get("from", "")
            if from_variable and from_variable == "users_menu":
                return "users:list"
        return "services:list"

    def load_objects(self):
        super().load_objects()
        self.tree = self.octofuss_tree("/")

    def get(self, request, service, action):
        services = octofuss.Subtree(self.tree, "services")
        if not services.has(service):
            messages.error(request, _("Service %s not found") % service)
            return redirect(self.get_success_url())
        if services.get(service)[1] == action:
            services.set(service, 1)
            messages.success(request, _("%s restarted/reloaded correctly") % service)
        else:
            messages.error(request, _("Action %(action)s not found for service %(service)s") % {
                "service": service,
                "action": action
            })
        return redirect(self.get_success_url())


# refs #13223
class ManageAccessPoints(OctonetMixin, View):
    http_method_names = ['post']
    conf_filename = "/etc/fuss-server/ap-config.sh"
    script_filename = "/usr/share/fuss-server/scripts/ap-control.sh"

    def post(self, request, *args, **kwargs):
        # Avoid race conditions
        # https://stackoverflow.com/questions/14574518/how-does-using-the-try-statement-avoid-a-race-condition
        child = None
        timeout = 60
        try:
            with open(self.script_filename) as self.script_file, open(self.conf_filename) as self.conf_file:
                if 'start_access_points' in request.POST:
                    # https://stackoverflow.com/questions/48169667/how-to-handle-subprocess-popen-output-in-both-python-2-and-python-3
                    child = subprocess.Popen(
                        [self.script_filename, 'enable'],
                        stdout=subprocess.PIPE,
                    )
                    output = child.communicate(timeout=timeout)[0]
                    # https://stackoverflow.com/a/5631819
                    return_code = child.returncode
                    print("manage_access_points, start:", output, "return code:", return_code)
                    if return_code == 0:
                        messages.info(request, _("Access points started"))
                    else:
                        messages.error(request, _("Error while starting access points. Return code: {}").format(return_code))
                elif 'stop_access_points' in request.POST:
                    # https://stackoverflow.com/questions/48169667/how-to-handle-subprocess-popen-output-in-both-python-2-and-python-3
                    child = subprocess.Popen(
                        [self.script_filename, 'disable'],
                        stdout=subprocess.PIPE,
                    )
                    output = child.communicate(timeout=timeout)[0]
                    # https://stackoverflow.com/a/5631819
                    return_code = child.returncode
                    print("manage_access_points, stop:", output, "return code:", return_code)
                    if return_code == 0:
                        messages.info(request, _("Access points stopped"))
                    else:
                        messages.error(request, _("Error while stopping access points. Return code: {}").format(return_code))
        except IOError as e:
            messages.error(request, _("Script and/or configuration file not found"))
        except subprocess.TimeoutExpired as e:
            # https://docs.python.org/3/library/subprocess.html#subprocess.Popen.communicate
            if child:
                child.kill()
            messages.error(request, _("Timeout during the operation. Maybe the action is not completed."))
        return redirect("services:list")
