from django.urls import re_path
from django.views.generic import TemplateView
from . import views


app_name = "services"
urlpatterns = [
    re_path(r"^list$", views.List.as_view(), name="list"),
    re_path(r"^do/(?P<service>[-\w]+)/(?P<action>[a-z]+)$", views.PerformAction.as_view(), name="do"),
    # refs #13223
    re_path(r"^access_points/manage$", views.ManageAccessPoints.as_view(), name="manage_access_points"),
]
