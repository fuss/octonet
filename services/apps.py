# coding: utf-8
from django.apps import AppConfig
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from octonet.mixins import OctonetAppMixin

class Services(OctonetAppMixin, AppConfig):
    name = 'services'
    verbose_name = _("Services")
    description = _("Restart services")
    group = _("Network")
    main_url = reverse_lazy("services:list")
    font_awesome_class = "fa-cog"
    octofussd_url = "/services"
